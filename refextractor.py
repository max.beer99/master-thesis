import sys

path_to_tex = sys.argv[1]

lines = open(path_to_tex).readlines()

reflines = []
for line in lines:
    if r"\ref{" in line:
        reflines.append(line)

for refline in reflines:
    refs = refline.split(r"\ref{p")
    for i in range(1,len(refs)):
        print("p"+refs[i].split(r"}")[0])