import numpy as np
import qcodes as qc
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import scipy
import scipy.interpolate
import scipy.misc
import scipy.optimize
import json
import os
import traceback
import typing

backslash = '\\'

def get_dac_channel_from_name(channel_name):
    """Convert dac channel name string to channel number.

        Assume channel_name to be of form: dac_SlotN_ChanM_volt and return
        N*4 + M."""

    N = int(channel_name.split('Slot')[1].split('_')[0])
    M = int(channel_name.split('Chan')[1].split('_')[0])

    return (N*4 + M)

def save_metadata(sample_config, experiment_metadata, base_path):
    """Try to save sample_config and experiment_metadata together with plot."""
    with open(f'{base_path}sample_config.json', 'w+') as file:
        json.dump(sample_config, file)

    if not experiment_metadata == {}:
        with open(f'{base_path}experiment_metadata.json', 'w+') as file:
            json.dump(experiment_metadata, file)

def si_prefix(number: float) -> typing.Tuple[float, str]:
    """Returns the next appropriate scaling factor and prefix"""
    prefix_list = ['a', 'f', 'p', 'n', r'$\mu$', 'm', '', 'k', 'M', 'G', 'T']
    order = int(np.floor(np.min([np.max([-18, np.log10(number)]), 12])))
    return (10.**(-3 * (order //3)), prefix_list[order //3 + 6])

def transform_string_finder(array_1, array_2, array_1_unit: str, array_2_unit: str) -> str:
    """Takes two 1D-arrays and attempts to find a nice tranform from array_1 to array_2 and returns a string for annotation.
    
        Note that array_1 and array_2 should already be scaled.
    """
    # If array_2 is constant:
    if np.unique(array_2).shape[0] == 1:
        return f"{np.round(np.unique(array_2)[0])} {array_2_unit}"

    # Else: assume linear transfer for now:
    m = np.round((array_2[-1] - array_2[0]) /(array_1[-1] - array_1[0]), 2)
    b = np.round(array_2[0] - m * array_1[0])
    b_sign = '-' if b < 0 else '+'

    if array_1_unit == array_2_unit:
        return f"{m}" + r"$\cdot x $" + f" {b_sign} {np.abs(b)} {array_2_unit}"
    
    return f"{m}" + r"$\frac{"+ array_2_unit +"}{"+ array_1_unit +"}\cdot x $" + f" {b_sign} {np.abs(b)} {array_2_unit}"

def generate_latex_code(plot_path: str, sample_name: str, experiment_name: str, figsize: list) -> str:
    """Generate and return generic figure latex code."""
    figure_label = '_'.join(plot_path.split('.png')[0].split('.pdf')[0].replace(backslash, '/').split('/')[-3:])
    caption_command = f"\\getCaption{{{ figure_label }}}{{\\textbf{{Sample: { latexify(sample_name) }, experiment: { latexify(experiment_name) }. }}}}"
    latex_string = f"""\\begin{{figure}}[H]
	\\centering
	\\includegraphics[width = { figsize[0] }in]{{./Graphics/Data/{ latexify('/'.join(plot_path.replace(backslash, '/').split('/')[-3:])) }}}
	\\caption{{{ caption_command }}}
	\\label{{{ figure_label }}}
\\end{{figure}}
% Define the following key for getCaption command to change the caption:
% { figure_label }
    """

    return latex_string

def save_latex_code(plot_path: str, sample_name: str, experiment_name: str, figsize: typing.Tuple[int, int]):
    """Try to generate and save generic figure latex code."""
    with open(plot_path.replace('.png', '.tex').replace('.pdf', '.tex'), 'w+') as file:
        file.write(generate_latex_code(plot_path, sample_name, experiment_name, figsize))

def latexify(string: str) -> str:
    """Make string latex compatible."""
    # replace _ by \_
    string = string.replace('_', '\_')

    return string

def pathname_to_save(plot_base_path: str, sample_name: str, timestamp: str, experiment_name: str, measurement_name: str, filetype: str, flat_file_hirarchy=False) -> typing.Tuple[str, str]:
    """Creates the folder structure and returns the base_path and plot_path for saving plots."""
    
    timestamp = timestamp.replace('-', '_').replace(' ', '-').replace(':', '_')
    base_path = ""
    plot_path = ""
    if flat_file_hirarchy:
        base_path = f"./{plot_base_path}plots_flat/"  
        plot_path = f"./{base_path}{sample_name}_{timestamp}-{experiment_name}_{measurement_name}{filetype}"
        if len(plot_path) >= 145:
            plot_path = f"./{base_path}{sample_name}_{timestamp}-{''.join([word[0] if not word == '' else '' for word in experiment_name.split('_')])}_{measurement_name}{filetype}"
        if len(plot_path) >= 145:
            plot_path = f"./{base_path}{sample_name}_{timestamp}-{''.join([word[0] if not word == '' else '' for word in experiment_name.split('_')])}_{''.join([word[0] if not word == '' else '' for word in measurement_name.split('_')])}{filetype}"

    else:
        base_path = f"./{plot_base_path}{sample_name}/{timestamp}-{experiment_name}/"        
        plot_path = f"{base_path}{measurement_name}{filetype}"
    
    assert not base_path == ""
    assert not plot_path == ""
    try:
        os.makedirs(base_path)
    except:
        pass
    return base_path, plot_path
