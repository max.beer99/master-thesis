import numpy as np
import qcodes as qc
import matplotlib.pyplot as plt
import scipy
import scipy.interpolate
import scipy.misc
import scipy.optimize
import scipy.signal
from pprint import pprint

%matplotlib inline

qc.initialise_or_create_database_at("../../20210310_QT477.db")

dataset = qc.dataset.data_set.load_by_run_spec(captured_run_id=172)

#pprint(dataset.get_metadata('sample_config'))

data = dataset.get_parameter_data()

smoothing_n = 5
R_threshold = 20e-12
R_operating_point = 200e-12

_R = data['lockin_up_R']['lockin_up_R']
_V = data['lockin_up_R']['keithley_left_volt']

# smoothen and interpolate
_interp_first_pinchoff = scipy.interpolate.interp1d(_V, scipy.signal.savgol_filter(_R, window_length = smoothing_n, polyorder = 3), kind = 'cubic')

assert np.max(_interp_first_pinchoff(_V)) > R_threshold, "Threshold current not reached."
assert np.max(_interp_first_pinchoff(_V)) > R_operating_point, "Operating current not reached."

_V_pinchoff = _V[np.where(np.min(np.abs(_interp_first_pinchoff(_V) - R_threshold)) == np.abs(_interp_first_pinchoff(_V) - R_threshold))][0]
_V_th = _V[np.where(np.abs(_interp_first_pinchoff(_V) - R_operating_point) == np.min(np.abs(_interp_first_pinchoff(_V) - R_operating_point)))][0]
_R_th = float(_interp_first_pinchoff(_V_th))

print(f'{_V_pinchoff=}\n{_V_th=}\n{_R_th=}')

plt.scatter(_V, _R, marker = '.')
plt.plot(_V, _interp_first_pinchoff(_V), color = 'r')
plt.vlines([_V_pinchoff, _V_th], np.min(_interp_first_pinchoff(_V)), np.max(_interp_first_pinchoff(_V)), color='r')
plt.show()
