import numpy as np
import scipy.optimize as sco
import scipy.signal as scs
import matplotlib.pyplot as plt
import matplotlib.font_manager as font_manager
import matplotlib
import os
import sys

dektak_csv_path = '../../DEKTAK 150/20210310_QT477_Chip_2_mesa_after_RIE.csv'

if len(sys.argv) == 1:
    pass
else:
    dektak_csv_path = sys.argv[1]

save_base_path = f"../../plots/DEKTAK 150/"
show_title = False

# Load fonts
font_paths = ["../../fonts/"]
for font in font_manager.findSystemFonts(font_paths):
    font_manager.fontManager.addfont(font)

# Matplotlib rcParams
figsize_imshow = (5, 4)
myRcParams = {
    'savefig.transparent': False,
    'savefig.facecolor': "white",
    'savefig.dpi': 600,
    'figure.figsize': (5.5, 3.5),
    'font.family': 'Latin Modern Roman',
    'mathtext.fontset': 'custom',
    'mathtext.bf': 'Latin Modern Roman',
    'mathtext.cal': 'Latin Modern Roman',
    'mathtext.it': 'Latin Modern Roman',
    'mathtext.rm': 'Latin Modern Roman',
    'mathtext.sf': 'Latin Modern Roman',
    'mathtext.tt': 'monospace',
    'font.size': 11,
    'axes.titlesize': 'medium',
    'axes.labelsize': 'medium',
    'figure.autolayout': False,
    'figure.constrained_layout.use': True,
    'figure.constrained_layout.h_pad': 0.2,
    'image.aspect': 'auto'
}
# Update rcParams
matplotlib.rcParams.update(matplotlib.rcParamsDefault)
for key in myRcParams.keys():
    matplotlib.rcParams[key] = myRcParams[key]

try:
    os.makedirs(save_base_path)
except BaseException:
    pass


lines = []
with open(dektak_csv_path, 'r') as file:
    lines = file.readlines()

x, y = [], []
for line in lines:
    x.append(line.split(',')[0])
    y.append(line.split(',')[1])
x, y = np.array(x, dtype=np.float64), np.array(y, dtype=np.float64)

y = y - np.min(y)

def fitting_function(x, m, b, plateau_start, plateau_stop, plateau_hight):
    return m * x + b + ((x > plateau_start) * (x < plateau_stop)) * plateau_hight

x_fit = x - np.mean(x)
popt, pcov = sco.curve_fit(fitting_function, x_fit, y, p0=[0, np.mean(y), x_fit[int(0.3 * len(x_fit))], x_fit[int(0.7 * len(x_fit))], np.mean(y) + 90.], sigma=np.ones_like(y)*5.)

#y = y - fitting_function(x, *popt[:4], 0)

fig = plt.figure()
ax = fig.add_subplot()

ax.plot(x, y)
#ax.plot(x, fitting_function(x_fit, *popt))

if show_title:
    ax.set_title(dektak_csv_path.split('/')[-1].split('.')[0])
ax.set_xlabel("Position (µm)")
ax.set_ylabel("Height (nm)")

ax.grid(which = 'major')
ax.grid(which = 'minor', alpha = 0.3)
ax.minorticks_on()

plt.savefig(f"{save_base_path}{dektak_csv_path.split('/')[-1].split('.')[0]}_dektak_analysis.png")
plt.close()
