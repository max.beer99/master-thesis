# %% Setup

#  -------------------------------------------------
#  --------- START OF USER DEFINED CONFIG ----------
#  -------------------------------------------------

plot_base_path = "../../plots/custom_plots/"

filetype = '.png'
plot_title = False
enable_latex_code = True
flat_file_hirarchy = True # Save plots in a flat file hirarchy for easier overleaf upload
font_paths = ["../../fonts/"]
plot_dpi = 600

#  -----------------------------------------------
#  --------- END OF USER DEFINED CONFIG ----------
#  -----------------------------------------------

data_folder = "../../Law_Batch_3_Chip_14_C3"

from logging import Filter
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.font_manager as font_manager
import scipy
import scipy.interpolate
import scipy.misc
import scipy.optimize
import scipy.io
import scipy.ndimage
import scipy.fftpack
import json
import os
import traceback
from plotting_helpers import *

# Load fonts
for font in font_manager.findSystemFonts(font_paths):
    font_manager.fontManager.addfont(font)

# Matplotlib rcParams
figsize_imshow = (5, 4)
myRcParams = {
    'savefig.transparent': False,
    'savefig.facecolor': "white",
    'savefig.dpi': plot_dpi,
    'figure.figsize': (5.5, 3.5),
    'font.family': 'Latin Modern Roman',
    'mathtext.fontset': 'custom',
    'mathtext.bf': 'Latin Modern Roman',
    'mathtext.cal': 'Latin Modern Roman',
    'mathtext.it': 'Latin Modern Roman',
    'mathtext.rm': 'Latin Modern Roman',
    'mathtext.sf': 'Latin Modern Roman',
    'mathtext.tt': 'monospace',
    'font.size': 11,
    'axes.titlesize': 'medium',
    'axes.labelsize': 'medium',
    'figure.autolayout': False,
    'figure.constrained_layout.use': True,
    'figure.constrained_layout.h_pad': 0.2,
    'image.aspect': 'auto'
}
# Update rcParams
matplotlib.rcParams.update(matplotlib.rcParamsDefault)
for key in myRcParams.keys():
    matplotlib.rcParams[key] = myRcParams[key]

###############################################################################

# Right SET Barrier vs Barrier
## Params
path_to_data = f"{data_folder}/01_SET_right/2022_09_21_1943_AWG_RB2_DAC_RB1_retune_SET.mat"
plot_awg_values_with_indices = [10, -10]

## Load Data
data_matlab = scipy.io.loadmat(path_to_data)
data = scipy.io.loadmat(path_to_data)['data'][0]
scan = scipy.io.loadmat(path_to_data)['scan'][0][0]

dac_channel = scan['loops']['setchan'][0][0][0][0][0]
dac_range = scan['loops']['rng'][0][0][0]
dac_npoints = scan['loops']['npoints'][0][0][0][0]
dac_values = np.linspace(*dac_range, dac_npoints)

awg_channel = scan['disp']['xlabel'][0][1][0] # Label on second position for this scan
awg_values = scan['disp']['x'][0][1][0]
awg_range = [min(awg_values), max(awg_values)]
awg_npoints = len(awg_values)

alazar_current = data[-1] * 1e-08 # Compensate for TIA

## Modify Data
###  Truncate via skip_over_first_n_awg_values
awg_values = awg_values[plot_awg_values_with_indices[0]:plot_awg_values_with_indices[1]]
alazar_current = alazar_current[:, plot_awg_values_with_indices[0]:plot_awg_values_with_indices[1]]
awg_range = [min(awg_values), max(awg_values)]
awg_npoints = len(awg_values)

## Setup Plotting
z_axis_values = alazar_current
z_axis_scaling, z_axis_prefix = si_prefix(np.max(np.abs(z_axis_values)))
z_axis_label = f"Ohmic Current I ({z_axis_prefix}A)"

x_axis_values = awg_values
x_axis_scaling, x_axis_prefix = si_prefix(np.max(np.abs(x_axis_values)))
x_axis_label = f"{awg_channel} U ({x_axis_prefix}V)"

y_axis_values = dac_values
y_axis_scaling, y_axis_prefix = si_prefix(np.max(np.abs(y_axis_values)))
y_axis_label = f"{dac_channel} U ({y_axis_prefix}V)"

experiment_name = f"{'_'.join(path_to_data.split('/')[-1].split('.')[0].split('_')[4:])}"
sample_name = f"{data_folder.split('/')[-1]}"
timestamp = f"{'_'.join(path_to_data.split('/')[-1].split('.')[0].split('_')[:5])}"

## Plot
fig = plt.figure(figsize = figsize_imshow)
ax = fig.add_subplot()

image = plt.imshow(z_axis_values * z_axis_scaling, extent=(np.min(x_axis_values) * x_axis_scaling, np.max(x_axis_values) * x_axis_scaling, np.min(y_axis_values) * y_axis_scaling, np.max(y_axis_values) * y_axis_scaling), origin="lower", interpolation='none')

if plot_title: ax.set_title(f"{sample_name}\n{experiment_name}", wrap = True)
ax.set_xlabel(x_axis_label)
ax.set_ylabel(y_axis_label)

fig.colorbar(image, label = z_axis_label, ax = ax)

base_path, plot_path = pathname_to_save(plot_base_path,
                                        sample_name,
                                        timestamp,
                                        experiment_name,
                                        "current",
                                        filetype,
                                        flat_file_hirarchy=flat_file_hirarchy)
if enable_latex_code: save_latex_code(plot_path, sample_name, f"{experiment_name}_current", figsize_imshow)
fig.savefig(plot_path)
plt.close(fig)

###############################################################################

# Right SET Coulomb Diamonds First Two Diamonds
## Params
path_to_data = f"{data_folder}/01_SET_right/2022_09_18_1807_AWG_RB1_DAC_Bias_highRes_CoulombDiamond.mat"
plot_awg_values_with_indices = [0, -1]

## Load Data
data_matlab = scipy.io.loadmat(path_to_data)
data = scipy.io.loadmat(path_to_data)['data'][0]
scan = scipy.io.loadmat(path_to_data)['scan'][0][0]

dac_channel = scan['loops']['setchan'][0][0][0][0][0]
dac_range = scan['loops']['rng'][0][0][0]
dac_npoints = scan['loops']['npoints'][0][0][0][0]
dac_values = np.linspace(*dac_range, dac_npoints)

awg_channel = scan['disp']['xlabel'][0][0][0][0][0] # Label on first position for this scan
awg_values = scan['disp']['x'][0][0][0]
awg_range = [min(awg_values), max(awg_values)]
awg_npoints = len(awg_values)

alazar_current = data[-1] * 1e-08 # Compensate for TIA

## Modify Data
###  Truncate via skip_over_first_n_awg_values
awg_values = awg_values[plot_awg_values_with_indices[0]:plot_awg_values_with_indices[1]]
alazar_current = alazar_current[:, plot_awg_values_with_indices[0]:plot_awg_values_with_indices[1]]
awg_range = [min(awg_values), max(awg_values)]
awg_npoints = len(awg_values)

### Truncate alazar_current between -1nA and 1nA
alazar_current[np.where(alazar_current > 1e-09)] = 1e-09
alazar_current[np.where(alazar_current < -1e-09)] = -1e-09

## Setup Plotting
z_axis_values = alazar_current
z_axis_scaling, z_axis_prefix = si_prefix(np.max(np.abs(z_axis_values)))
z_axis_label = f"Ohmic Current I ({z_axis_prefix}A)"

x_axis_values = awg_values
x_axis_scaling, x_axis_prefix = si_prefix(np.max(np.abs(x_axis_values)))
x_axis_label = f"{awg_channel} U ({x_axis_prefix}V)"

y_axis_values = dac_values
y_axis_scaling, y_axis_prefix = si_prefix(np.max(np.abs(y_axis_values)))
y_axis_label = f"{dac_channel} U ({y_axis_prefix}V)"

experiment_name = f"{'_'.join(path_to_data.split('/')[-1].split('.')[0].split('_')[4:])}"
sample_name = f"{data_folder.split('/')[-1]}"
timestamp = f"{'_'.join(path_to_data.split('/')[-1].split('.')[0].split('_')[:5])}"

## Plot
fig = plt.figure(figsize = figsize_imshow)
ax = fig.add_subplot()

image = plt.imshow(z_axis_values * z_axis_scaling, extent=(np.min(x_axis_values) * x_axis_scaling, np.max(x_axis_values) * x_axis_scaling, np.min(y_axis_values) * y_axis_scaling, np.max(y_axis_values) * y_axis_scaling), origin="lower", interpolation='none')

if plot_title: ax.set_title(f"{sample_name}\n{experiment_name}", wrap = True)
ax.set_xlabel(x_axis_label)
ax.set_ylabel(y_axis_label)

fig.colorbar(image, label = z_axis_label, ax = ax)

base_path, plot_path = pathname_to_save(plot_base_path,
                                        sample_name,
                                        timestamp,
                                        experiment_name,
                                        "current",
                                        filetype,
                                        flat_file_hirarchy=flat_file_hirarchy)
if enable_latex_code: save_latex_code(plot_path, sample_name, f"{experiment_name}_current", figsize_imshow)
fig.savefig(plot_path)
plt.close(fig)

###############################################################################

# Right SET Coulomb Diamonds Diamond 3 and onwards
## Params
path_to_data = f"{data_folder}/01_SET_right/2022_09_18_1817_AWG_RB1_DAC_Bias_highRes_CoulombDiamond.mat"
plot_awg_values_with_indices = [0, -1]

## Load Data
data_matlab = scipy.io.loadmat(path_to_data)
data = scipy.io.loadmat(path_to_data)['data'][0]
scan = scipy.io.loadmat(path_to_data)['scan'][0][0]

dac_channel = scan['loops']['setchan'][0][0][0][0][0]
dac_range = scan['loops']['rng'][0][0][0]
dac_npoints = scan['loops']['npoints'][0][0][0][0]
dac_values = np.linspace(*dac_range, dac_npoints)

awg_channel = scan['disp']['xlabel'][0][0][0][0][0] # Label on first position for this scan
awg_values = scan['disp']['x'][0][0][0]
awg_range = [min(awg_values), max(awg_values)]
awg_npoints = len(awg_values)

alazar_current = data[-1] * 1e-08 # Compensate for TIA

## Modify Data
###  Truncate via skip_over_first_n_awg_values
awg_values = awg_values[plot_awg_values_with_indices[0]:plot_awg_values_with_indices[1]]
alazar_current = alazar_current[:, plot_awg_values_with_indices[0]:plot_awg_values_with_indices[1]]
awg_range = [min(awg_values), max(awg_values)]
awg_npoints = len(awg_values)

### Truncate alazar_current between -1nA and 1nA
alazar_current[np.where(alazar_current > 1e-09)] = 1e-09
alazar_current[np.where(alazar_current < -1e-09)] = -1e-09

## Setup Plotting
z_axis_values = alazar_current
z_axis_scaling, z_axis_prefix = si_prefix(np.max(np.abs(z_axis_values)))
z_axis_label = f"Ohmic Current I ({z_axis_prefix}A)"

x_axis_values = awg_values
x_axis_scaling, x_axis_prefix = si_prefix(np.max(np.abs(x_axis_values)))
x_axis_label = f"{awg_channel} U ({x_axis_prefix}V)"

y_axis_values = dac_values
y_axis_scaling, y_axis_prefix = si_prefix(np.max(np.abs(y_axis_values)))
y_axis_label = f"{dac_channel} U ({y_axis_prefix}V)"

experiment_name = f"{'_'.join(path_to_data.split('/')[-1].split('.')[0].split('_')[4:])}"
sample_name = f"{data_folder.split('/')[-1]}"
timestamp = f"{'_'.join(path_to_data.split('/')[-1].split('.')[0].split('_')[:5])}"

## Plot
fig = plt.figure(figsize = figsize_imshow)
ax = fig.add_subplot()

image = plt.imshow(z_axis_values * z_axis_scaling, extent=(np.min(x_axis_values) * x_axis_scaling, np.max(x_axis_values) * x_axis_scaling, np.min(y_axis_values) * y_axis_scaling, np.max(y_axis_values) * y_axis_scaling), origin="lower", interpolation='none')

if plot_title: ax.set_title(f"{sample_name}\n{experiment_name}", wrap = True)
ax.set_xlabel(x_axis_label)
ax.set_ylabel(y_axis_label)

fig.colorbar(image, label = z_axis_label, ax = ax)

base_path, plot_path = pathname_to_save(plot_base_path,
                                        sample_name,
                                        timestamp,
                                        experiment_name,
                                        "current",
                                        filetype,
                                        flat_file_hirarchy=flat_file_hirarchy)
if enable_latex_code: save_latex_code(plot_path, sample_name, f"{experiment_name}_current", figsize_imshow)
fig.savefig(plot_path)
plt.close(fig)

###############################################################################

# Right SET DQD
## Params (The label "left" SET is actually wrong)
path_to_data = f"{data_folder}/02_SET_left/2022_09_21_1749_AWG_S3_DAC_TRP_2chan_measure.mat"
plot_awg_values_with_indices = [1, -1]
diff_ax = 1 # 0: dac; 1: awg
debug_filtering = False
filtering_x_indeces = [40, 41, 42, 43, 44, 103, 104, 105, 106, 107, 69, 70, 71, 72, 75, 76, 77, 78, 14, 133]

## Load Data
data_matlab = scipy.io.loadmat(path_to_data)
data = scipy.io.loadmat(path_to_data)['data'][0]
scan = scipy.io.loadmat(path_to_data)['scan'][0][0]

dac_channel = scan['loops']['setchan'][0][0][0][0][0]
dac_range = scan['loops']['rng'][0][0][0]
dac_npoints = scan['loops']['npoints'][0][0][0][0]
dac_values = np.linspace(*dac_range, dac_npoints)

awg_channel = scan['disp']['xlabel'][0][0][0][0][0] # Label on first position for this scan
awg_values = scan['disp']['x'][0][0][0]
awg_range = [min(awg_values), max(awg_values)]
awg_npoints = len(awg_values)

alazar_current = data[-1] * 1e-08 # Compensate for TIA

## Modify Data
###  Truncate via skip_over_first_n_awg_values
awg_values = awg_values[plot_awg_values_with_indices[0]:plot_awg_values_with_indices[1]]
alazar_current = alazar_current[:, plot_awg_values_with_indices[0]:plot_awg_values_with_indices[1]]
awg_range = [min(awg_values), max(awg_values)]
awg_npoints = len(awg_values)

### Calculate derivative of alazar_current after diff_ax
x_mesh, y_mesh = np.meshgrid(awg_values, dac_values)
dalazar_dx = (alazar_current[:, 1:] - alazar_current[:, :-1]) /(x_mesh[:, 1:] - x_mesh[:, :-1])
dalazar_dy = (alazar_current[1:, :] - alazar_current[:-1, :]) /(y_mesh[1:, :] - y_mesh[:-1, :])
if diff_ax == 0:
    dalazar_d = dalazar_dy
else:
    dalazar_d = dalazar_dx

### Filter out noise of derivative
dalazar_d_fft2 = scipy.fftpack.fft2(dalazar_d)
for index_x in filtering_x_indeces:
    dalazar_d_fft2[:, index_x] = 0

dalazar_d_filtered = np.real(scipy.fftpack.fft2(dalazar_d_fft2))[::-1, ::-1]

if debug_filtering:
    plt.imshow(np.abs(scipy.fftpack.fft2(dalazar_d)), interpolation='none')
    plt.show()
    plt.imshow(np.abs(scipy.fftpack.fft2(dalazar_d_filtered)), interpolation='none')
    plt.show()

## Setup Plotting
z_axis_values = alazar_current
z_axis_scaling, z_axis_prefix = si_prefix(np.max(np.abs(z_axis_values)))
z_axis_label = f"Ohmic Current I ({z_axis_prefix}A)"

dz_axis_values = dalazar_d
dz_axis_scaling, dz_axis_prefix = si_prefix(np.max(np.abs(dz_axis_values)))
dz_axis_label = f"Differential Ohmic Conductivity " + r"$\frac{dI}{dV}$" + f" ({dz_axis_prefix}S)"

dzf_axis_values = dalazar_d_filtered
dzf_axis_scaling, dzf_axis_prefix = si_prefix(np.max(np.abs(dzf_axis_values)))
dzf_axis_label = f"Differential Ohmic Conductivity " + r"$\frac{dI}{dV}$" + f" (a.U.)"

x_axis_values = awg_values
x_axis_scaling, x_axis_prefix = si_prefix(np.max(np.abs(x_axis_values)))
x_axis_label = f"{awg_channel} U ({x_axis_prefix}V)"

y_axis_values = dac_values
y_axis_scaling, y_axis_prefix = si_prefix(np.max(np.abs(y_axis_values)))
y_axis_label = f"{dac_channel} U ({y_axis_prefix}V)"

experiment_name = f"{'_'.join(path_to_data.split('/')[-1].split('.')[0].split('_')[4:])}"
sample_name = f"{data_folder.split('/')[-1]}"
timestamp = f"{'_'.join(path_to_data.split('/')[-1].split('.')[0].split('_')[:5])}"

## Plot
### Plot z
fig = plt.figure(figsize = figsize_imshow)
ax = fig.add_subplot()

image = plt.imshow(z_axis_values * z_axis_scaling, extent=(np.min(x_axis_values) * x_axis_scaling, np.max(x_axis_values) * x_axis_scaling, np.min(y_axis_values) * y_axis_scaling, np.max(y_axis_values) * y_axis_scaling), origin="lower", interpolation='none')

if plot_title: ax.set_title(f"{sample_name}\n{experiment_name}", wrap = True)
ax.set_xlabel(x_axis_label)
ax.set_ylabel(y_axis_label)

fig.colorbar(image, label = z_axis_label, ax = ax)

base_path, plot_path = pathname_to_save(plot_base_path,
                                        sample_name,
                                        timestamp,
                                        experiment_name,
                                        "current",
                                        filetype,
                                        flat_file_hirarchy=flat_file_hirarchy)
if enable_latex_code: save_latex_code(plot_path, sample_name, f"{experiment_name}_current", figsize_imshow)
fig.savefig(plot_path)
plt.close(fig)

### Plot dz
fig = plt.figure(figsize = figsize_imshow)
ax = fig.add_subplot()

image = plt.imshow(dz_axis_values * dz_axis_scaling, extent=(np.min(x_axis_values) * x_axis_scaling, np.max(x_axis_values) * x_axis_scaling, np.min(y_axis_values) * y_axis_scaling, np.max(y_axis_values) * y_axis_scaling), origin="lower", interpolation='none')

if plot_title: ax.set_title(f"{sample_name}\n{experiment_name}", wrap = True)
ax.set_xlabel(x_axis_label)
ax.set_ylabel(y_axis_label)

fig.colorbar(image, label = dz_axis_label, ax = ax)

base_path, plot_path = pathname_to_save(plot_base_path,
                                        sample_name,
                                        timestamp,
                                        experiment_name,
                                        "differential_current",
                                        filetype,
                                        flat_file_hirarchy=flat_file_hirarchy)
if enable_latex_code: save_latex_code(plot_path, sample_name, f"{experiment_name}_differential_current", figsize_imshow)
fig.savefig(plot_path)
plt.close(fig)

### Plot dzf
fig = plt.figure(figsize = figsize_imshow)
ax = fig.add_subplot()

image = plt.imshow(dzf_axis_values * dzf_axis_scaling, extent=(np.min(x_axis_values) * x_axis_scaling, np.max(x_axis_values) * x_axis_scaling, np.min(y_axis_values) * y_axis_scaling, np.max(y_axis_values) * y_axis_scaling), origin="lower", interpolation='none')

if plot_title: ax.set_title(f"{sample_name}\n{experiment_name}", wrap = True)
ax.set_xlabel(x_axis_label)
ax.set_ylabel(y_axis_label)

fig.colorbar(image, label = dzf_axis_label, ax = ax)

base_path, plot_path = pathname_to_save(plot_base_path,
                                        sample_name,
                                        timestamp,
                                        experiment_name,
                                        "differential_current_filtered",
                                        filetype,
                                        flat_file_hirarchy=flat_file_hirarchy)
if enable_latex_code: save_latex_code(plot_path, sample_name, f"{experiment_name}_differential_current_filtered", figsize_imshow)
fig.savefig(plot_path)
plt.close(fig)

###############################################################################

# Right SET DQD Higher voltages (x-derivative)
## Params (The label "left" SET is actually wrong)
path_to_data = f"{data_folder}/02_SET_left/2022_09_21_1743_AWG_S3_DAC_TRP_2chan_measure.mat"
plot_awg_values_with_indices = [1, -1]
diff_ax = 1 # 0: dac; 1: awg
debug_filtering = False
filtering_x_indeces = [36, 40, 41, 42, 43, 44, 103, 104, 105, 106, 107, 69, 70, 71, 72, 75, 76, 77, 78, 14, 133]

## Load Data
data_matlab = scipy.io.loadmat(path_to_data)
data = scipy.io.loadmat(path_to_data)['data'][0]
scan = scipy.io.loadmat(path_to_data)['scan'][0][0]

dac_channel = scan['loops']['setchan'][0][0][0][0][0]
dac_range = scan['loops']['rng'][0][0][0]
dac_npoints = scan['loops']['npoints'][0][0][0][0]
dac_values = np.linspace(*dac_range, dac_npoints)

awg_channel = scan['disp']['xlabel'][0][0][0][0][0] # Label on first position for this scan
awg_values = scan['disp']['x'][0][0][0]
awg_range = [min(awg_values), max(awg_values)]
awg_npoints = len(awg_values)

alazar_current = data[-1] * 1e-08 # Compensate for TIA

## Modify Data
###  Truncate via skip_over_first_n_awg_values
awg_values = awg_values[plot_awg_values_with_indices[0]:plot_awg_values_with_indices[1]]
alazar_current = alazar_current[:, plot_awg_values_with_indices[0]:plot_awg_values_with_indices[1]]
awg_range = [min(awg_values), max(awg_values)]
awg_npoints = len(awg_values)

### Calculate derivative of alazar_current after diff_ax
x_mesh, y_mesh = np.meshgrid(awg_values, dac_values)
dalazar_dx = (alazar_current[:, 1:] - alazar_current[:, :-1]) /(x_mesh[:, 1:] - x_mesh[:, :-1])
dalazar_dy = (alazar_current[1:, :] - alazar_current[:-1, :]) /(y_mesh[1:, :] - y_mesh[:-1, :])
if diff_ax == 0:
    dalazar_d = dalazar_dy
else:
    dalazar_d = dalazar_dx

### Filter out noise of derivative
dalazar_d_fft2 = scipy.fftpack.fft2(dalazar_d)
for index_x in filtering_x_indeces:
    dalazar_d_fft2[:, index_x] = 0

dalazar_d_filtered = np.real(scipy.fftpack.fft2(dalazar_d_fft2))[::-1, ::-1]

if debug_filtering:
    plt.imshow(np.abs(scipy.fftpack.fft2(dalazar_d)), interpolation='none')
    plt.show()
    plt.imshow(np.abs(scipy.fftpack.fft2(dalazar_d_filtered)), interpolation='none')
    plt.show()

## Setup Plotting
z_axis_values = alazar_current
z_axis_scaling, z_axis_prefix = si_prefix(np.max(np.abs(z_axis_values)))
z_axis_label = f"Ohmic Current I ({z_axis_prefix}A)"

dz_axis_values = dalazar_d
dz_axis_scaling, dz_axis_prefix = si_prefix(np.max(np.abs(dz_axis_values)))
dz_axis_label = f"Differential Ohmic Conductivity " + r"$\frac{dI}{dV}$" + f" ({dz_axis_prefix}S)"

dzf_axis_values = dalazar_d_filtered
dzf_axis_scaling, dzf_axis_prefix = si_prefix(np.max(np.abs(dzf_axis_values)))
dzf_axis_label = f"Differential Ohmic Conductivity " + r"$\frac{dI}{dV}$" + f" (a.U.)"

x_axis_values = awg_values
x_axis_scaling, x_axis_prefix = si_prefix(np.max(np.abs(x_axis_values)))
x_axis_label = f"{awg_channel} U ({x_axis_prefix}V)"

y_axis_values = dac_values
y_axis_scaling, y_axis_prefix = si_prefix(np.max(np.abs(y_axis_values)))
y_axis_label = f"{dac_channel} U ({y_axis_prefix}V)"

experiment_name = f"{'_'.join(path_to_data.split('/')[-1].split('.')[0].split('_')[4:])}"
sample_name = f"{data_folder.split('/')[-1]}"
timestamp = f"{'_'.join(path_to_data.split('/')[-1].split('.')[0].split('_')[:5])}"

## Plot
### Plot z
fig = plt.figure(figsize = figsize_imshow)
ax = fig.add_subplot()

image = plt.imshow(z_axis_values * z_axis_scaling, extent=(np.min(x_axis_values) * x_axis_scaling, np.max(x_axis_values) * x_axis_scaling, np.min(y_axis_values) * y_axis_scaling, np.max(y_axis_values) * y_axis_scaling), origin="lower", interpolation='none')

if plot_title: ax.set_title(f"{sample_name}\n{experiment_name}", wrap = True)
ax.set_xlabel(x_axis_label)
ax.set_ylabel(y_axis_label)

fig.colorbar(image, label = z_axis_label, ax = ax)

base_path, plot_path = pathname_to_save(plot_base_path,
                                        sample_name,
                                        timestamp,
                                        experiment_name,
                                        "current",
                                        filetype,
                                        flat_file_hirarchy=flat_file_hirarchy)
if enable_latex_code: save_latex_code(plot_path, sample_name, f"{experiment_name}_current", figsize_imshow)
fig.savefig(plot_path)
plt.close(fig)

### Plot dz
fig = plt.figure(figsize = figsize_imshow)
ax = fig.add_subplot()

image = plt.imshow(dz_axis_values * dz_axis_scaling, extent=(np.min(x_axis_values) * x_axis_scaling, np.max(x_axis_values) * x_axis_scaling, np.min(y_axis_values) * y_axis_scaling, np.max(y_axis_values) * y_axis_scaling), origin="lower", interpolation='none')

if plot_title: ax.set_title(f"{sample_name}\n{experiment_name}", wrap = True)
ax.set_xlabel(x_axis_label)
ax.set_ylabel(y_axis_label)

fig.colorbar(image, label = dz_axis_label, ax = ax)

base_path, plot_path = pathname_to_save(plot_base_path,
                                        sample_name,
                                        timestamp,
                                        experiment_name,
                                        "x_differential_current",
                                        filetype,
                                        flat_file_hirarchy=flat_file_hirarchy)
if enable_latex_code: save_latex_code(plot_path, sample_name, f"{experiment_name}_differential_current", figsize_imshow)
fig.savefig(plot_path)
plt.close(fig)

### Plot dzf
fig = plt.figure(figsize = figsize_imshow)
ax = fig.add_subplot()

image = plt.imshow(dzf_axis_values * dzf_axis_scaling, extent=(np.min(x_axis_values) * x_axis_scaling, np.max(x_axis_values) * x_axis_scaling, np.min(y_axis_values) * y_axis_scaling, np.max(y_axis_values) * y_axis_scaling), origin="lower", interpolation='none')

if plot_title: ax.set_title(f"{sample_name}\n{experiment_name}", wrap = True)
ax.set_xlabel(x_axis_label)
ax.set_ylabel(y_axis_label)

fig.colorbar(image, label = dzf_axis_label, ax = ax)

base_path, plot_path = pathname_to_save(plot_base_path,
                                        sample_name,
                                        timestamp,
                                        experiment_name,
                                        "x_differential_current_filtered",
                                        filetype,
                                        flat_file_hirarchy=flat_file_hirarchy)
if enable_latex_code: save_latex_code(plot_path, sample_name, f"{experiment_name}_differential_current_filtered", figsize_imshow)
fig.savefig(plot_path)
plt.close(fig)

###############################################################################

# Right SET DQD Higher voltages (y-derivative)
## Params (The label "left" SET is actually wrong)
path_to_data = f"{data_folder}/02_SET_left/2022_09_21_1743_AWG_S3_DAC_TRP_2chan_measure.mat"
plot_awg_values_with_indices = [1, -1]
diff_ax = 0 # 0: dac; 1: awg
debug_filtering = False
filtering_x_indeces = [14, 41, 42, 43, 44, 45, 71, 77, 103, 104, 105, 106, 107, 134]

## Load Data
data_matlab = scipy.io.loadmat(path_to_data)
data = scipy.io.loadmat(path_to_data)['data'][0]
scan = scipy.io.loadmat(path_to_data)['scan'][0][0]

dac_channel = scan['loops']['setchan'][0][0][0][0][0]
dac_range = scan['loops']['rng'][0][0][0]
dac_npoints = scan['loops']['npoints'][0][0][0][0]
dac_values = np.linspace(*dac_range, dac_npoints)

awg_channel = scan['disp']['xlabel'][0][0][0][0][0] # Label on first position for this scan
awg_values = scan['disp']['x'][0][0][0]
awg_range = [min(awg_values), max(awg_values)]
awg_npoints = len(awg_values)

alazar_current = data[-1] * 1e-08 # Compensate for TIA

## Modify Data
###  Truncate via skip_over_first_n_awg_values
awg_values = awg_values[plot_awg_values_with_indices[0]:plot_awg_values_with_indices[1]]
alazar_current = alazar_current[:, plot_awg_values_with_indices[0]:plot_awg_values_with_indices[1]]
awg_range = [min(awg_values), max(awg_values)]
awg_npoints = len(awg_values)

### Calculate derivative of alazar_current after diff_ax
x_mesh, y_mesh = np.meshgrid(awg_values, dac_values)
dalazar_dx = (alazar_current[:, 1:] - alazar_current[:, :-1]) /(x_mesh[:, 1:] - x_mesh[:, :-1])
dalazar_dy = (alazar_current[1:, :] - alazar_current[:-1, :]) /(y_mesh[1:, :] - y_mesh[:-1, :])
if diff_ax == 0:
    dalazar_d = dalazar_dy
else:
    dalazar_d = dalazar_dx

### Filter out noise of derivative
dalazar_d_fft2 = scipy.fftpack.fft2(dalazar_d)
for index_x in filtering_x_indeces:
    dalazar_d_fft2[:, index_x] = 0

dalazar_d_filtered = np.real(scipy.fftpack.fft2(dalazar_d_fft2))[::-1, ::-1]

if debug_filtering:
    plt.imshow(np.abs(scipy.fftpack.fft2(dalazar_d)), interpolation='none')
    plt.show()
    plt.imshow(np.abs(scipy.fftpack.fft2(dalazar_d_filtered)), interpolation='none')
    plt.show()

## Setup Plotting
z_axis_values = alazar_current
z_axis_scaling, z_axis_prefix = si_prefix(np.max(np.abs(z_axis_values)))
z_axis_label = f"Ohmic Current I ({z_axis_prefix}A)"

dz_axis_values = dalazar_d
dz_axis_scaling, dz_axis_prefix = si_prefix(np.max(np.abs(dz_axis_values)))
dz_axis_label = f"Differential Ohmic Conductivity " + r"$\frac{dI}{dV}$" + f" ({dz_axis_prefix}S)"

dzf_axis_values = dalazar_d_filtered
dzf_axis_scaling, dzf_axis_prefix = si_prefix(np.max(np.abs(dzf_axis_values)))
dzf_axis_label = f"Differential Ohmic Conductivity " + r"$\frac{dI}{dV}$" + f" (a.U.)"

x_axis_values = awg_values
x_axis_scaling, x_axis_prefix = si_prefix(np.max(np.abs(x_axis_values)))
x_axis_label = f"{awg_channel} U ({x_axis_prefix}V)"

y_axis_values = dac_values
y_axis_scaling, y_axis_prefix = si_prefix(np.max(np.abs(y_axis_values)))
y_axis_label = f"{dac_channel} U ({y_axis_prefix}V)"

experiment_name = f"{'_'.join(path_to_data.split('/')[-1].split('.')[0].split('_')[4:])}"
sample_name = f"{data_folder.split('/')[-1]}"
timestamp = f"{'_'.join(path_to_data.split('/')[-1].split('.')[0].split('_')[:5])}"

## Plot
### Plot z
fig = plt.figure(figsize = figsize_imshow)
ax = fig.add_subplot()

image = plt.imshow(z_axis_values * z_axis_scaling, extent=(np.min(x_axis_values) * x_axis_scaling, np.max(x_axis_values) * x_axis_scaling, np.min(y_axis_values) * y_axis_scaling, np.max(y_axis_values) * y_axis_scaling), origin="lower", interpolation='none')

if plot_title: ax.set_title(f"{sample_name}\n{experiment_name}", wrap = True)
ax.set_xlabel(x_axis_label)
ax.set_ylabel(y_axis_label)

fig.colorbar(image, label = z_axis_label, ax = ax)

base_path, plot_path = pathname_to_save(plot_base_path,
                                        sample_name,
                                        timestamp,
                                        experiment_name,
                                        "current",
                                        filetype,
                                        flat_file_hirarchy=flat_file_hirarchy)
if enable_latex_code: save_latex_code(plot_path, sample_name, f"{experiment_name}_current", figsize_imshow)
fig.savefig(plot_path)
plt.close(fig)

### Plot dz
fig = plt.figure(figsize = figsize_imshow)
ax = fig.add_subplot()

image = plt.imshow(dz_axis_values * dz_axis_scaling, extent=(np.min(x_axis_values) * x_axis_scaling, np.max(x_axis_values) * x_axis_scaling, np.min(y_axis_values) * y_axis_scaling, np.max(y_axis_values) * y_axis_scaling), origin="lower", interpolation='none')

if plot_title: ax.set_title(f"{sample_name}\n{experiment_name}", wrap = True)
ax.set_xlabel(x_axis_label)
ax.set_ylabel(y_axis_label)

fig.colorbar(image, label = dz_axis_label, ax = ax)

base_path, plot_path = pathname_to_save(plot_base_path,
                                        sample_name,
                                        timestamp,
                                        experiment_name,
                                        "y_differential_current",
                                        filetype,
                                        flat_file_hirarchy=flat_file_hirarchy)
if enable_latex_code: save_latex_code(plot_path, sample_name, f"{experiment_name}_differential_current", figsize_imshow)
fig.savefig(plot_path)
plt.close(fig)

### Plot dzf
fig = plt.figure(figsize = figsize_imshow)
ax = fig.add_subplot()

image = plt.imshow(dzf_axis_values * dzf_axis_scaling, extent=(np.min(x_axis_values) * x_axis_scaling, np.max(x_axis_values) * x_axis_scaling, np.min(y_axis_values) * y_axis_scaling, np.max(y_axis_values) * y_axis_scaling), origin="lower", interpolation='none')

if plot_title: ax.set_title(f"{sample_name}\n{experiment_name}", wrap = True)
ax.set_xlabel(x_axis_label)
ax.set_ylabel(y_axis_label)

fig.colorbar(image, label = dzf_axis_label, ax = ax)

base_path, plot_path = pathname_to_save(plot_base_path,
                                        sample_name,
                                        timestamp,
                                        experiment_name,
                                        "y_differential_current_filtered",
                                        filetype,
                                        flat_file_hirarchy=flat_file_hirarchy)
if enable_latex_code: save_latex_code(plot_path, sample_name, f"{experiment_name}_differential_current_filtered", figsize_imshow)
fig.savefig(plot_path)
plt.close(fig)