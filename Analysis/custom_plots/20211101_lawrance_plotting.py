# %% Setup

#  -------------------------------------------------
#  --------- START OF USER DEFINED CONFIG ----------
#  -------------------------------------------------

plot_base_path = "../../plots/custom_plots/"

filetype = '.png'
plot_title = False
enable_latex_code = True
flat_file_hirarchy = True # Save plots in a flat file hirarchy for easier overleaf upload
font_paths = ["../../fonts/"]
plot_dpi = 600

path_to_sample_configs_to_overload_json = "../../sample_configs_to_overload.json"

#  -----------------------------------------------
#  --------- END OF USER DEFINED CONFIG ----------
#  -----------------------------------------------

database_path = "../../20211101_Lawrance_plotting.db"

from attr import asdict
from isort import file
from random import sample
from importlib_metadata import metadata
import numpy as np
import qcodes as qc
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.font_manager as font_manager
import scipy
import scipy.interpolate
import scipy.misc
import scipy.optimize
import json
import os
import traceback
from plotting_helpers import *
from plotting_functions.analyze_find_SET_operating_points import *
from plotting_functions.analyze_tlm_measurement import *
from plotting_functions.analyze_generic_constant import *
from plotting_functions.analyze_channel_sequence_1D_sweep import *
from plotting_functions.analyze_generic_sequence_1D_sweep import *
from plotting_functions.analyze_generic_1D_sweep import *
from plotting_functions.analyze_generic_2D_sweep import *

# Load fonts
for font in font_manager.findSystemFonts(font_paths):
    font_manager.fontManager.addfont(font)

# Matplotlib rcParams
figsize_imshow = (5, 4)
myRcParams = {
    'savefig.transparent': False,
    'savefig.facecolor': "white",
    'savefig.dpi': plot_dpi,
    'figure.figsize': (5.5, 3.5),
    'font.family': 'Latin Modern Roman',
    'mathtext.fontset': 'custom',
    'mathtext.bf': 'Latin Modern Roman',
    'mathtext.cal': 'Latin Modern Roman',
    'mathtext.it': 'Latin Modern Roman',
    'mathtext.rm': 'Latin Modern Roman',
    'mathtext.sf': 'Latin Modern Roman',
    'mathtext.tt': 'monospace',
    'font.size': 11,
    'axes.titlesize': 'medium',
    'axes.labelsize': 'medium',
    'figure.autolayout': False,
    'figure.constrained_layout.use': True,
    'figure.constrained_layout.h_pad': 0.2,
    'image.aspect': 'auto'
}
# Update rcParams
matplotlib.rcParams.update(matplotlib.rcParamsDefault)
for key in myRcParams.keys():
    matplotlib.rcParams[key] = myRcParams[key]

# Load path_to_sample_configs_to_overload.json to potentially overload
# sample_config
sample_configs_to_overload = {database_path: {}}
try:
    with open(path_to_sample_configs_to_overload_json, 'r') as file:
        sample_configs_to_overload = json.load(file)
    print('Sample config successfully overloaded!')
except BaseException:
    print('No sample config to overload!')
    print('continuing...')
# If sample_configs_to_overload exists but no entries for this specific db
# exist, add dummy key
if database_path not in sample_configs_to_overload.keys():
    sample_configs_to_overload[database_path] = {}

# Import dataset
qc.initialise_or_create_database_at(database_path)

def load_datasets(dataset_ids):
    datasets = []
    for run_id in dataset_ids:
        curr_dataset = qc.load_by_id(run_id)
        if str(curr_dataset.run_id) in sample_configs_to_overload[database_path].keys():
            curr_dataset.add_metadata(tag = 'sample_config', metadata = json.dumps(sample_configs_to_overload[database_path][str(curr_dataset.run_id)]))
        datasets.append(curr_dataset)
    
    return datasets

# Show Charging
# Charging LP first
ids =  [[list(range(745, 757+1)), list(range(799, 808+1))]]
ids += [[list(range(758, 767+1)), list(range(809, 818+1))]]
ids += [[list(range(758, 767+1)), list(range(809, 818+1))]]
# Charging LBX first
ids += [[list(range(487, 496+1)), list(range(514, 523+1))]]
ids += [[list(range(498, 508+1)), list(range(525, 534+1)), list(range(540, 549+1))]]

for current_set in range(len(ids)):
    ids_in_set = []
    for ids_in_set_single in ids[current_set]:
        ids_in_set += ids_in_set_single
    dataset_list = load_datasets(ids_in_set)

    analyze_generic_sequence_1D_sweep(dataset_list, plot_base_path, filetype, plot_title, enable_latex_code, flat_file_hirarchy)


