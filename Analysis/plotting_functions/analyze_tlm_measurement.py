import numpy as np
import qcodes as qc
import matplotlib
import matplotlib.offsetbox
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import scipy
import scipy.interpolate
import scipy.misc
import scipy.optimize
import json
import os
import traceback
from plotting_helpers import *

def analyze_tlm_measurement(dataset_list, plot_base_path, filetype = '.png', TLM_distances = [], TLM_W = [], plot_title=True, enable_latex_code=False, flat_file_hirarchy=False):
    """
    Analyze the results of a set of TLM measurements.
    """
    # Input validation
    assert not dataset_list == [], 'Invalid input!'

    if TLM_distances == []:
        TLM_distances = list(np.array([50, (50 + 50 + 100), (50 + 50 + 100 + 50 + 200), (50 + 50 + 100 + 50 + 200 + 50 + 400), 100, 100 + 50 + 200, 100 + 50 + 200 + 50 + 400, 200, 200 + 50 + 400, 400]) *1e-06) # m
    if TLM_W == []:
        TLM_W = 50e-06  # m

    values_volt = []
    values_curr = []
    list_polyvars = []
    TLM_resistances = []
    _experiment_labels = []
    for dataset in dataset_list:
        sample_config = json.loads(dataset.get_metadata('sample_config'))
        experiment_metadata = json.loads(dataset.get_metadata('experiment_metadata'))
        data = dataset.get_parameter_data()

        # Extract metadata
        sample_name = sample_config['sample_name']
        gate_mapping = sample_config['gate_mapping']
        decadac_mapping = sample_config['DECADAC_mapping']
        lockin_mapping = sample_config['Lockin_mapping']
        keithley_mapping = sample_config['Keithley_mapping']

        experiment_name = dataset.exp_name.replace('_', ' ')
        _experiment_labels.append(' '.join(experiment_name.split(' ')[:4]))

        keithleys = []
        for key in data.keys():
            if 'keithley' in key:
                keithleys.append(key)

        for keithley in keithleys:
            data_keithley = data[keithley]
            keithleys_volt = []
            for key in data_keithley.keys():
                if 'keithley' in key and '_volt' in key:
                    keithleys_volt.append(key)

            values_volt.append(data_keithley[keithleys_volt[0]])  # mV
            values_curr.append(data_keithley[keithley]) # µA

            x_axis_scaling, x_axis_prefix = si_prefix(np.max(np.abs(values_volt[-1])))
            y_axis_scaling, y_axis_prefix = si_prefix(np.max(np.abs(values_curr[-1])))

            # Perform linear fit
            polyvars = np.polyfit(values_volt[-1], values_curr[-1], deg = 1) # I = U/R + const.
            list_polyvars.append(polyvars)
            R = 1/polyvars[0] # Ohm
            TLM_resistances.append(R)

            R_scaling, R_prefix = si_prefix(R)

            fig = plt.figure()
            ax = fig.add_subplot()

            ax.scatter(values_volt[-1] * x_axis_scaling, values_curr[-1] * y_axis_scaling, marker = '.')
            ax.plot(values_volt[-1] * x_axis_scaling, np.polyval(polyvars, values_volt[-1]) * y_axis_scaling)

            if plot_title: ax.set_title(f'{sample_name}\n{experiment_name}', wrap = True)
            ax.set_xlabel(f"{gate_mapping[keithley_mapping[keithleys_volt[0].split('_volt')[0]]]} U ({x_axis_prefix}V)")
            ax.set_ylabel(f"{gate_mapping[keithley_mapping[keithley.split('_curr')[0]]]} I ({y_axis_prefix}A)")

            text = r"$y = a\cdot x+b$" + "\n" + r"$a^{-1}$" + f" = {np.round(R * R_scaling, 2)} {R_prefix}Ohm\nb = {np.round(polyvars[1] * y_axis_scaling, 2)} {y_axis_prefix}A"
            text_prop = dict()
            annotation = matplotlib.offsetbox.AnchoredText(text, loc='upper left', prop=text_prop)
            annotation.patch.set_facecolor('w')
            ax.add_artist(annotation)

            ax.grid(which = 'major', zorder = -1)
            ax.grid(which = 'minor', zorder = -1, alpha = 0.3)
            ax.minorticks_on()

            base_path, plot_path = pathname_to_save(plot_base_path,
                                                    sample_name,
                                                    dataset.run_timestamp(),
                                                    dataset.exp_name,
                                                    f"TLM_measurement",
                                                    filetype,
                                                    flat_file_hirarchy=flat_file_hirarchy)
            if enable_latex_code: save_latex_code(plot_path, sample_name, experiment_name, matplotlib.rcParams.get('figure.figsize'))
            save_metadata(sample_config, experiment_metadata, base_path)
            fig.savefig(plot_path)
            plt.close(fig)


    # Delete base measurement
    values_volt = values_volt[1:]
    values_curr = values_curr[1:]
    list_polyvars = list_polyvars[1:]
    TLM_resistances = TLM_resistances[1:]
    TLM_distances = TLM_distances[0:len(TLM_resistances)]
    _experiment_labels = _experiment_labels[1:]

    x_axis_scaling, x_axis_prefix = si_prefix(np.max(np.abs([np.max(np.abs(volt_array)) for volt_array in values_volt])))
    y_axis_scaling, y_axis_prefix = si_prefix(np.max(np.abs([np.max(np.abs(curr_array)) for curr_array in values_curr])))

    fig = plt.figure()
    ax = fig.add_subplot()

    for i in range(len(list_polyvars)):
        ax.scatter(values_volt[i] * x_axis_scaling, values_curr[i] * y_axis_scaling, marker = '.', label = _experiment_labels[i])
        ax.plot(values_volt[i] * x_axis_scaling, np.polyval(list_polyvars[i], values_volt[i]) *  y_axis_scaling)

    if plot_title: ax.set_title(f'{sample_name}\nTLM measurements', wrap = True)
    ax.set_xlabel(f"{gate_mapping[keithley_mapping[keithleys_volt[0].split('_volt')[0]]]} U ({x_axis_prefix}V)")
    ax.set_ylabel(f"{gate_mapping[keithley_mapping[keithley.split('_curr')[0]]]} I ({y_axis_prefix}A)")
    ax.legend(ncol = 3, loc = 'upper center', bbox_to_anchor = (.5, -.2), bbox_transform = plt.gca().transAxes, facecolor = 'white', fancybox = False, edgecolor = 'black')

    ax.grid(which = 'major', zorder = -1)
    ax.grid(which = 'minor', zorder = -1, alpha = 0.3)
    ax.minorticks_on()

    base_path, plot_path = pathname_to_save(plot_base_path,
                                            sample_name,
                                            dataset_list[-1].run_timestamp(),
                                            dataset_list[-1].exp_name,
                                            f"TLM_measurement_overview",
                                            filetype,
                                            flat_file_hirarchy=flat_file_hirarchy)
    if enable_latex_code: save_latex_code(plot_path, sample_name, experiment_name, matplotlib.rcParams.get('figure.figsize'))
    save_metadata(sample_config, experiment_metadata, base_path)
    fig.savefig(plot_path)
    plt.close(fig)

    # Perform linear fit
    polyvars = np.polyfit(TLM_distances, TLM_resistances, deg = 1) # R = R_s/W * L + 2 * R_c
    R_s = polyvars[0] * TLM_W # Ohm
    R_c = polyvars[1] / 2 # Ohm

    x_axis_scaling, x_axis_prefix = si_prefix(np.max(np.abs(TLM_distances)))
    y_axis_scaling, y_axis_prefix = si_prefix(np.max(np.abs(TLM_resistances)))

    R_s_scaling, R_s_prefix = si_prefix(R_s)
    R_c_scaling, R_c_prefix = si_prefix(R_c)

    fig = plt.figure()
    ax = fig.add_subplot()

    ax.scatter(np.array(TLM_distances) * x_axis_scaling, np.array(TLM_resistances) * y_axis_scaling)
    ax.plot(np.array(TLM_distances) * x_axis_scaling, np.polyval(polyvars, TLM_distances) * y_axis_scaling)

    if plot_title: ax.set_title(f'{sample_name}\nR_s, R_c calculation', wrap = True)
    ax.set_xlabel(f"TLM distance ({x_axis_prefix}m)")
    ax.set_ylabel(f"Resistance ({y_axis_prefix}Ohm)")

    text = f"$R = R_s\cdot L / W + 2\cdot R_c$\n $R_s$ = {np.round(R_s * R_s_scaling, 2)} {R_s_prefix}Ohm/sq \n $R_c$ = {np.round(R_c * R_c_scaling, 2)} {R_c_prefix}Ohm"
    text_prop = dict()
    annotation = matplotlib.offsetbox.AnchoredText(text, loc='upper left', prop=text_prop)
    annotation.patch.set_facecolor('w')
    ax.add_artist(annotation)


    ax.grid(which = 'major', zorder = -1)
    ax.grid(which = 'minor', zorder = -1, alpha = 0.3)
    ax.minorticks_on()

    base_path, plot_path = pathname_to_save(plot_base_path,
                                            sample_name,
                                            dataset_list[-1].run_timestamp(),
                                            dataset_list[-1].exp_name,
                                            f"TLM_Rs_Rc_calculation",
                                            filetype,
                                            flat_file_hirarchy=flat_file_hirarchy)
    if enable_latex_code: save_latex_code(plot_path, sample_name, experiment_name, matplotlib.rcParams.get('figure.figsize'))
    save_metadata(sample_config, experiment_metadata, base_path)
    fig.savefig(plot_path)
    plt.close(fig)