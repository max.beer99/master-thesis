from attr import asdict
import numpy as np
import qcodes as qc
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import scipy
import scipy.interpolate
import scipy.misc
import scipy.optimize
import json
import os
import traceback
from plotting_helpers import *

def analyze_generic_sequence_1D_sweep(dataset_list, plot_base_path, filetype = '.png', plot_title=True, enable_latex_code=False, flat_file_hirarchy=False):
    """
    Plot all datasets in dataset_list into a single plot.

    Plot lockin_R vs keithley_volt (if available) or lockin_R vs dac_channel.volt alternatively.
    This plotting only analyzes the set voltages of the first dataset (Except the plotted range,
    as it will scale to accomodate all datasets) in the sequence. Therefore, the swept gates
    should not be changed the swept voltage ranges should only be changed such that
    the calculated linear relationship between voltages still hold true.
    """
    # Input validation:
    assert not dataset_list == [], "Invalid input!"

    sample_config = json.loads(dataset_list[-1].get_metadata('sample_config'))
    experiment_metadata = json.loads(dataset_list[-1].get_metadata('experiment_metadata'))
    data = dataset_list[-1].get_parameter_data()

    # Extract metadata
    sample_name = sample_config['sample_name']
    gate_mapping = sample_config['gate_mapping']
    decadac_mapping = sample_config['DECADAC_mapping']
    lockin_mapping = sample_config['Lockin_mapping']
    keithley_mapping = sample_config['Keithley_mapping']

    experiment_name = dataset_list[-1].exp_name.replace('_', ' ')

    # Analyze the first dataset to get information about the shape of the data
    names_current_parameters = []
    for key in data.keys():
        if 'lockin' in key and '_R' in key:
            names_current_parameters.append(key)
        elif '_curr' in key:
            names_current_parameters.append(key)

    assert names_current_parameters != [], "Nothing to plot!"

    for name_current_parameter in names_current_parameters:
        current_parameter = data[name_current_parameter]
        names_voltage_setpoint = []
        values_voltage_setpoint = []
        for key in current_parameter.keys():
            if 'volt' in key or 'amplitude' in key:
                names_voltage_setpoint.append(key)
                values_voltage_setpoint.append(current_parameter[key])

        assert names_voltage_setpoint != [], "Nothing to plot!"

        flag_multiple_x_axes_required = False
        flag_exactly_2_axes_required = False
        if bool(np.sum(np.abs(np.array([voltages[0]-values_voltage_setpoint[0][0] for voltages in values_voltage_setpoint])) > np.abs(values_voltage_setpoint[0][1] - values_voltage_setpoint[0][0]))):
            flag_multiple_x_axes_required = True
            if len(names_voltage_setpoint) == 2:
                flag_exactly_2_axes_required = True

        # Collect the data from all the datasets
        x_axis_values = []
        y_axis_values = []
        for dataset_index, dataset in enumerate(dataset_list):
            data = dataset.get_parameter_data()

            x_axis_values.append([data[name_current_parameter][name_voltage_setpoint] for name_voltage_setpoint in names_voltage_setpoint])
            y_axis_values.append(data[name_current_parameter][name_current_parameter])

        # Determine scaling
        x_axis_scaling, x_axis_prefix = si_prefix(np.max([np.max([np.max(values) for values in value_list]) for value_list in x_axis_values]))
        y_axis_scaling, y_axis_prefix = si_prefix(np.max([np.max([np.max(values) for values in value_list]) for value_list in y_axis_values]))

        if flag_multiple_x_axes_required:
            raise NotImplementedError
        else:
            for i in range(len(x_axis_values)):
                x_axis_values[i] = x_axis_values[i][0]

            x_axis_labels = []
            for name_voltage_parameter in names_voltage_setpoint:
                if 'keithley' in name_voltage_parameter:
                    x_axis_labels.append(gate_mapping[keithley_mapping[name_voltage_parameter.split('_volt')[0]]])
                elif 'dac' in name_voltage_parameter:
                    x_axis_labels.append(gate_mapping[decadac_mapping[str(get_dac_channel_from_name(name_voltage_parameter))]])
                elif 'lockin' in name_voltage_parameter:
                    x_axis_labels.append(gate_mapping[lockin_mapping[name_voltage_parameter.split('_amplitude')[0]]['in']])

            x_axis_label = ', '.join(x_axis_labels) + f' U ({x_axis_prefix}V)'

            y_axis_label = ''
            if 'lockin' in name_current_parameter:
                y_axis_label = gate_mapping[lockin_mapping[name_current_parameter.split('_R')[0]]['in']] + f' R ({y_axis_prefix}A)'
            elif 'keithley' in name_current_parameter:
                y_axis_label = gate_mapping[keithley_mapping[name_current_parameter.split('_curr')[0]]] + f' I ({y_axis_prefix}A)'

            fig = plt.figure()
            ax = fig.add_subplot()

            for values_i in range(len(x_axis_values)):
                ax.plot(x_axis_values[values_i] * x_axis_scaling, y_axis_values[values_i] * y_axis_scaling)

            if plot_title: ax.set_title(f'{sample_name}\n{experiment_name}', wrap = True)
            ax.set_xlabel(x_axis_label)
            ax.set_ylabel(y_axis_label)

            ax.grid(which = 'major', zorder = -1)
            ax.grid(which = 'minor', zorder = -1, alpha = 0.3)
            ax.minorticks_on()

            base_path, plot_path = pathname_to_save(plot_base_path,
                                                    sample_name,
                                                    dataset_list[-1].run_timestamp(),
                                                    dataset_list[-1].exp_name,
                                                    f"{'_'.join(name_current_parameter.split('_')[:-1:])}_sequence_1D_sweep",
                                                    filetype,
                                                    flat_file_hirarchy=flat_file_hirarchy)
            if enable_latex_code: save_latex_code(plot_path, sample_name, experiment_name, matplotlib.rcParams.get('figure.figsize'))
            save_metadata(sample_config, experiment_metadata, base_path)
            fig.savefig(plot_path)
            plt.close(fig)
