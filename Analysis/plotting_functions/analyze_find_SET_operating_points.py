import numpy as np
import qcodes as qc
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import scipy
import scipy.interpolate
import scipy.misc
import scipy.optimize
import json
import os
import traceback
from plotting_helpers import *

def analyze_find_SET_operating_points(dataset, plot_base_path, filetype, plot_title=True, enable_latex_code=False, flat_file_hirarchy=False):
    """
    Plot and Analyze set operating point measurement at 4K.

    Plot Lockin_R vs keithley_volt (linear and log).
    Plot keithley_curr vs keithley_volt.
    """
    sample_config = json.loads(dataset.get_metadata('sample_config'))
    experiment_metadata = json.loads(dataset.get_metadata('experiment_metadata'))
    data = dataset.get_parameter_data()

    # Extract metadata
    sample_name = sample_config['sample_name']
    gate_mapping = sample_config['gate_mapping']
    decadac_mapping = sample_config['DECADAC_mapping']
    lockin_mapping = sample_config['Lockin_mapping']
    keithley_mapping = sample_config['Keithley_mapping']

    experiment_name = dataset.exp_name.replace('_', ' ')

    keithleys = []
    lockins_R = []
    lockins_P = []
    for key in data.keys():
        if 'keithley' in key:
            keithleys.append(key)
        if 'lockin' in key:
            if '_R' in key:
                lockins_R.append(key)
            elif '_P' in key:
                lockins_P.append(key)

    for lockin_R in lockins_R:
        data_lockin_R = data[lockin_R]
        keithleys_volt = []
        dac_channels = []
        labels = []
        for key in data_lockin_R.keys():
            if 'dac' in key:
                dac_channels.append(key)
                labels.append(gate_mapping[decadac_mapping[str(get_dac_channel_from_name(key))]])
            if 'keithley' in key and '_volt' in key:
                keithleys_volt.append(key)
                labels.append(gate_mapping[keithley_mapping[key.split('_volt')[0]]])

        channels_string = ', '.join(labels)

        values_keithley_volt = data_lockin_R[keithleys_volt[0]]
        values_lockin_R = data_lockin_R[lockin_R] * 1e12 # pA

        fig = plt.figure()
        ax = fig.add_subplot()

        ax.plot(values_keithley_volt, values_lockin_R)

        if plot_title: ax.set_title(f'{sample_name}\n{experiment_name}', wrap = True)
        ax.set_xlabel(f"{channels_string} U (V)")
        ax.set_ylabel(f"{gate_mapping[lockin_mapping[lockin_R.split('_R')[0]]['out']]} - {gate_mapping[lockin_mapping[lockin_R.split('_R')[0]]['in']]} R (pA)")

        ax.grid(which = 'major', zorder = -1)
        ax.grid(which = 'minor', zorder = -1, alpha = 0.3)
        ax.minorticks_on()

        base_path, plot_path = pathname_to_save(plot_base_path,
                                                sample_name,
                                                dataset.run_timestamp(),
                                                dataset.exp_name,
                                                f"Find_SET_operating_point",
                                                filetype,
                                                flat_file_hirarchy=flat_file_hirarchy)
        if enable_latex_code: save_latex_code(plot_path, sample_name, experiment_name, matplotlib.rcParams.get('figure.figsize'))
        save_metadata(sample_config, experiment_metadata, base_path)
        fig.savefig(plot_path)
        plt.close(fig)

        fig = plt.figure()
        ax = fig.add_subplot()

        ax.plot(values_keithley_volt, values_lockin_R)

        if plot_title: ax.set_title(f'{sample_name}\n{experiment_name}', wrap = True)
        ax.set_xlabel(f"{channels_string} U (V)")
        ax.set_ylabel(f"{gate_mapping[lockin_mapping[lockin_R.split('_R')[0]]['out']]} - {gate_mapping[lockin_mapping[lockin_R.split('_R')[0]]['in']]} R (pA)")

        ax.set_yscale('log')
        ax.grid(which = 'major', zorder = -1)
        ax.grid(which = 'minor', zorder = -1, alpha = 0.3)
        ax.minorticks_on()

        base_path, plot_path = pathname_to_save(plot_base_path,
                                                sample_name,
                                                dataset.run_timestamp(),
                                                dataset.exp_name,
                                                f"Find_SET_operating_point_log",
                                                filetype,
                                                flat_file_hirarchy=flat_file_hirarchy)
        if enable_latex_code: save_latex_code(plot_path, sample_name, experiment_name, matplotlib.rcParams.get('figure.figsize'))
        save_metadata(sample_config, experiment_metadata, base_path)
        fig.savefig(plot_path)
        plt.close(fig)

    for keithley in keithleys:
        data_keithley = data[keithley]
        keithleys_volt = []
        dac_channels = []
        labels = []
        for key in data_lockin_R.keys():
            if 'dac' in key:
                dac_channels.append(key)
                labels.append(gate_mapping[decadac_mapping[str(get_dac_channel_from_name(key))]])
            if 'keithley' in key and '_volt' in key:
                keithleys_volt.append(key)
                labels.append(gate_mapping[keithley_mapping[key.split('_volt')[0]]])

        channels_string = ', '.join(labels)
                
        values_keithley_volt = data_keithley[keithleys_volt[0]]
        values_keithley_curr = data_keithley[keithley] * 1e12 # pA

        fig = plt.figure()
        ax = fig.add_subplot()

        ax.plot(values_keithley_volt, values_keithley_curr)

        if plot_title: ax.set_title(f'{sample_name}\n{experiment_name}', wrap = True)
        ax.set_xlabel(f"{channels_string} U (V)")
        ax.set_ylabel(f"{gate_mapping[keithley_mapping[keithley.split('_curr')[0]]]} I (pA)")

        ax.grid(which = 'major', zorder = -1)
        ax.grid(which = 'minor', zorder = -1, alpha = 0.3)
        ax.minorticks_on()

        base_path, plot_path = pathname_to_save(plot_base_path,
                                                sample_name,
                                                dataset.run_timestamp(),
                                                dataset.exp_name,
                                                f"Find_SET_operating_point_leakage",
                                                filetype,
                                                flat_file_hirarchy=flat_file_hirarchy)
        if enable_latex_code: save_latex_code(plot_path, sample_name, experiment_name, matplotlib.rcParams.get('figure.figsize'))
        save_metadata(sample_config, experiment_metadata, base_path)
        fig.savefig(plot_path)
        plt.close(fig)

