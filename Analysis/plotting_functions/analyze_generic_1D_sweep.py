from tkinter.font import names
import numpy as np
import qcodes as qc
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import scipy
import scipy.interpolate
import scipy.misc
import scipy.optimize
import json
import os
import traceback
from plotting_helpers import *
import warnings

def analyze_generic_1D_sweep(dataset, plot_base_path, filetype = '.png', plot_title=True, enable_latex_code=False, flat_file_hirarchy=False):
    """
    Plot lockin_R and keithley_curr vs keithley_volt and dac_channels.volt
    
    If the difference between dac channel voltages at the same checkpoint is larger than the seperation
    between set points of the same channel, then give multiple x-axes for two channels or assume a
    linear transformation between voltages and give that one.
    """
    sample_config = json.loads(dataset.get_metadata('sample_config'))
    experiment_metadata = json.loads(dataset.get_metadata('experiment_metadata'))
    data = dataset.get_parameter_data()

    # Extract metadata
    sample_name = sample_config['sample_name']
    gate_mapping = sample_config['gate_mapping']
    decadac_mapping = sample_config['DECADAC_mapping']
    lockin_mapping = sample_config['Lockin_mapping']
    keithley_mapping = sample_config['Keithley_mapping']

    experiment_name = dataset.exp_name.replace('_', ' ')

    names_current_parameters = []
    for key in data.keys():
        if 'lockin' in key and '_R' in key:
            names_current_parameters.append(key)
        elif '_curr' in key:
            names_current_parameters.append(key)

    assert names_current_parameters != [], "Nothing to plot!"

    for name_current_parameter in names_current_parameters:
        current_parameter = data[name_current_parameter]
        names_voltage_setpoint = []
        values_voltage_setpoint = []
        for key in current_parameter.keys():
            if 'volt' in key or 'amplitude' in key:
                names_voltage_setpoint.append(key)
                values_voltage_setpoint.append(current_parameter[key])

        assert names_voltage_setpoint != [], "Nothing to plot!"

        flag_multiple_x_axes_required = False
        flag_exactly_2_axes_required = False
        if bool(np.sum(np.abs(np.array([voltages[0]-values_voltage_setpoint[0][0] for voltages in values_voltage_setpoint])) > np.abs(values_voltage_setpoint[0][1] - values_voltage_setpoint[0][0]))):
            flag_multiple_x_axes_required = True
            if len(names_voltage_setpoint) == 2:
                flag_exactly_2_axes_required = True
                
        if flag_multiple_x_axes_required:
            # Check for swept axes:
            swept_flags = []
            for setpoint_i in range(len(names_voltage_setpoint)):
                swept_flags.append(np.unique(values_voltage_setpoint[setpoint_i]).shape[0] > 1)

            warnings.warn('Axis with same ranges are not grouped and may display trivial transfer functions!', RuntimeWarning)

            # Set first swept axis as primary:
            x_axis_primary_values = values_voltage_setpoint[np.where(swept_flags)[0][0]]
            x_axis_primary_name = names_voltage_setpoint[np.where(swept_flags)[0][0]]
            x_axes_secondary_values = [values_voltage_setpoint[i] for i in list(np.where(swept_flags)[0][1:]) + list(np.where([not flag for flag in swept_flags])[0][1:])]
            x_axes_secondary_names = [names_voltage_setpoint[i] for i in list(np.where(swept_flags)[0][1:]) + list(np.where([not flag for flag in swept_flags])[0][1:])]
            y_axis_values = current_parameter[name_current_parameter]

            x_axis_scaling, x_axis_prefix = si_prefix(np.max(np.abs(x_axis_primary_values)))
            y_axis_scaling, y_axis_prefix = si_prefix(np.max(np.abs(y_axis_values)))

            x_axes_secondary_scaling_and_prefix = []
            for secondary_values in x_axes_secondary_values:
                x_axes_secondary_scaling_and_prefix.append(si_prefix(np.max(np.abs(secondary_values))))

            if flag_exactly_2_axes_required:
                warnings.warn('Actually using a secondary axis not yet implemented!', RuntimeWarning)

            # Get Transform functions
            transform_strings = []
            for secondary_i, secondary_values in enumerate(x_axes_secondary_values):
                transform_strings.append(transform_string_finder(x_axis_scaling * x_axis_primary_values, x_axes_secondary_scaling_and_prefix[secondary_i][0] * secondary_values, f"{x_axis_prefix}V", f"{x_axes_secondary_scaling_and_prefix[secondary_i][1]}V"))

            x_axis_labels = []
            if 'keithley' in x_axis_primary_name:
                    x_axis_labels.append(gate_mapping[keithley_mapping[x_axis_primary_name.split('_volt')[0]]])
            elif 'dac' in x_axis_primary_name:
                x_axis_labels.append(gate_mapping[decadac_mapping[str(get_dac_channel_from_name(x_axis_primary_name))]])
            elif 'lockin' in x_axis_primary_name:
                x_axis_labels.append(gate_mapping[lockin_mapping[x_axis_primary_name.split('_amplitude')[0]]['in']])
            else:
                raise RuntimeError('Channel could not be identified!')
            x_axis_labels[-1] += f" U ({x_axis_prefix}V)"

            for index_secondary, name_secondary in enumerate(x_axes_secondary_names):
                if 'keithley' in name_secondary:
                    x_axis_labels.append(gate_mapping[keithley_mapping[name_secondary.split('_volt')[0]]])
                elif 'dac' in name_secondary:
                    x_axis_labels.append(gate_mapping[decadac_mapping[str(get_dac_channel_from_name(name_secondary))]])
                elif 'lockin' in name_secondary:
                    x_axis_labels.append(gate_mapping[lockin_mapping[name_secondary.split('_amplitude')[0]]['in']])
                else:
                    raise RuntimeError('Channel could not be identified!')
                x_axis_labels[-1] += f" U ({transform_strings[index_secondary]})"

                x_axis_label = '\n'.join(x_axis_labels)

                y_axis_label = ''
                if 'lockin' in name_current_parameter:
                    y_axis_label = gate_mapping[lockin_mapping[name_current_parameter.split('_R')[0]]['in']] + f' R ({y_axis_prefix}A)'
                elif 'keithley' in name_current_parameter:
                    y_axis_label = gate_mapping[keithley_mapping[name_current_parameter.split('_curr')[0]]] + f' I ({y_axis_prefix}A)'

                fig = plt.figure()
                ax = fig.add_subplot()

                ax.plot(x_axis_primary_values * x_axis_scaling, y_axis_values * y_axis_scaling)

                if plot_title: ax.set_title(f'{sample_name}\n{experiment_name}', wrap = True)
                ax.set_xlabel(x_axis_label)
                ax.set_ylabel(y_axis_label)

                ax.grid(which = 'major', zorder = -1)
                ax.grid(which = 'minor', zorder = -1, alpha = 0.3)
                ax.minorticks_on()

                base_path, plot_path = pathname_to_save(plot_base_path,
                                                        sample_name,
                                                        dataset.run_timestamp(),
                                                        dataset.exp_name,
                                                        f"{'_'.join(name_current_parameter.split('_')[:-1:])}_1D_sweep",
                                                        filetype,
                                                        flat_file_hirarchy=flat_file_hirarchy)
                if enable_latex_code: save_latex_code(plot_path, sample_name, experiment_name, matplotlib.rcParams.get('figure.figsize'))
                save_metadata(sample_config, experiment_metadata, base_path)
                fig.savefig(plot_path)
                plt.close(fig)

        else:
            x_axis_values = values_voltage_setpoint[0]
            y_axis_values = current_parameter[name_current_parameter]

            x_axis_scaling, x_axis_prefix = si_prefix(np.max(np.abs(x_axis_values)))
            y_axis_scaling, y_axis_prefix = si_prefix(np.max(np.abs(y_axis_values)))

            x_axis_labels = []
            for name_voltage_parameter in names_voltage_setpoint:
                if 'keithley' in name_voltage_parameter:
                    x_axis_labels.append(gate_mapping[keithley_mapping[name_voltage_parameter.split('_volt')[0]]])
                elif 'dac' in name_voltage_parameter:
                    x_axis_labels.append(gate_mapping[decadac_mapping[str(get_dac_channel_from_name(name_voltage_parameter))]])
                elif 'lockin' in name_voltage_parameter:
                    x_axis_labels.append(gate_mapping[lockin_mapping[name_voltage_parameter.split('_amplitude')[0]]['in']])

            x_axis_label = ', '.join(x_axis_labels) + f' U ({x_axis_prefix}V)'

            y_axis_label = ''
            if 'lockin' in name_current_parameter:
                y_axis_label = gate_mapping[lockin_mapping[name_current_parameter.split('_R')[0]]['in']] + f' R ({y_axis_prefix}A)'
            elif 'keithley' in name_current_parameter:
                y_axis_label = gate_mapping[keithley_mapping[name_current_parameter.split('_curr')[0]]] + f' I ({y_axis_prefix}A)'

            fig = plt.figure()
            ax = fig.add_subplot()

            ax.plot(x_axis_values * x_axis_scaling, y_axis_values * y_axis_scaling)

            if plot_title: ax.set_title(f'{sample_name}\n{experiment_name}', wrap = True)
            ax.set_xlabel(x_axis_label)
            ax.set_ylabel(y_axis_label)

            ax.grid(which = 'major', zorder = -1)
            ax.grid(which = 'minor', zorder = -1, alpha = 0.3)
            ax.minorticks_on()

            base_path, plot_path = pathname_to_save(plot_base_path,
                                                    sample_name,
                                                    dataset.run_timestamp(),
                                                    dataset.exp_name,
                                                    f"{'_'.join(name_current_parameter.split('_')[:-1:])}_1D_sweep",
                                                    filetype,
                                                    flat_file_hirarchy=flat_file_hirarchy)
            if enable_latex_code: save_latex_code(plot_path, sample_name, experiment_name, matplotlib.rcParams.get('figure.figsize'))
            save_metadata(sample_config, experiment_metadata, base_path)
            fig.savefig(plot_path)
            plt.close(fig)