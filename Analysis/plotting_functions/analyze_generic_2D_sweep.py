from distutils.log import debug
import numpy as np
import qcodes as qc
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import scipy
import scipy.interpolate
import scipy.misc
import scipy.optimize
import json
import os
import traceback
import warnings
from plotting_helpers import *
from plotting_functions.analyze_generic_1D_sweep import *

def analyze_generic_2D_sweep(dataset, plot_base_path, filetype = '.png', plot_title=True, enable_latex_code=False, flat_file_hirarchy=False, figsize_imshow=(5, 4)):
    """
    Plot lockin_R and keithley_curr vs 2 axes of keithley_volt or dac_channels.volt or lockin.amplitude
    
    If the difference between dac channel voltages at the same checkpoint is larger than the seperation
    between set points of the same channel, then give multiple x-axes for two channels or assume a
    linear transformation between voltages and give that one.
    """
    # Load data
    sample_config = json.loads(dataset.get_metadata('sample_config'))
    experiment_metadata = json.loads(dataset.get_metadata('experiment_metadata'))
    data = dataset.get_parameter_data()

    # Extract metadata
    sample_name = sample_config['sample_name']
    gate_mapping = sample_config['gate_mapping']
    decadac_mapping = sample_config['DECADAC_mapping']
    lockin_mapping = sample_config['Lockin_mapping']
    keithley_mapping = sample_config['Keithley_mapping']

    # shorten experiment_name
    experiment_name = dataset.exp_name.split('_linear')[0].replace('_', ' ')

    names_current_parameters = []
    for key in data.keys():
        if 'lockin' in key and '_R' in key:
            names_current_parameters.append(key)
        elif '_curr' in key:
            names_current_parameters.append(key)

    assert names_current_parameters != [], "Nothing to plot!"

    for name_current_parameter in names_current_parameters:
        current_parameter = data[name_current_parameter]
        names_voltage_setpoint = []
        values_voltage_setpoint = []
        for key in current_parameter.keys():
            if 'volt' in key or 'amplitude' in key:
                names_voltage_setpoint.append(key)
                values_voltage_setpoint.append(current_parameter[key])

        assert names_voltage_setpoint != [], "Nothing to plot!"
            
        # Identify to which set of voltages each voltage sweep belongs:
        names_voltage_setpoint_x = []
        values_voltage_setpoint_x = []
        names_voltage_setpoint_y = []
        values_voltage_setpoint_y = []
        for setpoint_i in range(len(names_voltage_setpoint)):
            if values_voltage_setpoint[setpoint_i][0] == values_voltage_setpoint[setpoint_i][1]:
                names_voltage_setpoint_y.append(names_voltage_setpoint[setpoint_i])
                values_voltage_setpoint_y.append(values_voltage_setpoint[setpoint_i])
            else:
                names_voltage_setpoint_x.append(names_voltage_setpoint[setpoint_i])
                values_voltage_setpoint_x.append(values_voltage_setpoint[setpoint_i])

        # Transform data to be usable
        for setpoint_i in range(len(names_voltage_setpoint_x)):
            values_voltage_setpoint_x[setpoint_i] = np.unique(values_voltage_setpoint_x[setpoint_i])
        for setpoint_i in range(len(names_voltage_setpoint_y)):
            values_voltage_setpoint_y[setpoint_i] = np.unique(values_voltage_setpoint_y[setpoint_i])

        # Check wether the data is actaully 1D:
        number_of_swept_axes_x = 0
        for values_voltage_setpoint in values_voltage_setpoint_x:
            if values_voltage_setpoint.shape[0] > 1:
                number_of_swept_axes_x += 1
        number_of_swept_axes_y = 0
        for values_voltage_setpoint in values_voltage_setpoint_y:
            if values_voltage_setpoint.shape[0] > 1:
                number_of_swept_axes_y += 1 

        # If data is 1D, pass data to analyze_generic_1D_sweep
        if number_of_swept_axes_x == 0 or number_of_swept_axes_y == 0:
            analyze_generic_1D_sweep(dataset, plot_base_path, filetype, plot_title, enable_latex_code, flat_file_hirarchy)
            continue

        z_axis_values = np.reshape(current_parameter[name_current_parameter], (len(values_voltage_setpoint_y[0]), len(values_voltage_setpoint_x[0])))
        z_axis_scaling, z_axis_prefix = si_prefix(np.max(np.abs(z_axis_values)))

        z_axis_label = ''
        if 'lockin' in name_current_parameter:
            z_axis_label = gate_mapping[lockin_mapping[name_current_parameter.split('_R')[0]]['in']] + f' R ({z_axis_prefix}A)'
        elif 'keithley' in name_current_parameter:
            z_axis_label = gate_mapping[keithley_mapping[name_current_parameter.split('_curr')[0]]] + f' I ({z_axis_prefix}A)'

        # Check limits of sweep voltages
        voltage_setpoint_x_min_values = []
        voltage_setpoint_y_min_values = []
        voltage_setpoint_x_max_values = []
        voltage_setpoint_y_max_values = []
        for values in values_voltage_setpoint_x:
            voltage_setpoint_x_min_values.append(np.min(values))
            voltage_setpoint_x_max_values.append(np.max(values))
        for values in values_voltage_setpoint_y:
            voltage_setpoint_y_min_values.append(np.min(values))
            voltage_setpoint_y_max_values.append(np.max(values))

        x_axis_scaling, x_axis_prefix = si_prefix(np.max(np.abs(values_voltage_setpoint_x)))
        y_axis_scaling, y_axis_prefix = si_prefix(np.max(np.abs(values_voltage_setpoint_y)))

        # Assemble axis labels
        x_axis_labels = []
        for name_voltage_parameter in names_voltage_setpoint_x:
            if 'keithley' in name_voltage_parameter:
                x_axis_labels.append(gate_mapping[keithley_mapping[name_voltage_parameter.split('_volt')[0]]])
            elif 'dac' in name_voltage_parameter:
                x_axis_labels.append(gate_mapping[decadac_mapping[str(get_dac_channel_from_name(name_voltage_parameter))]])
            elif 'lockin' in name_voltage_parameter:
                x_axis_labels.append(gate_mapping[lockin_mapping[name_voltage_parameter.split('_amplitude')[0]]['in']])

        x_axis_label = ', '.join(x_axis_labels) + f' U ({x_axis_prefix}V)'

        y_axis_labels = []
        for name_voltage_parameter in names_voltage_setpoint_y:
            if 'keithley' in name_voltage_parameter:
                y_axis_labels.append(gate_mapping[keithley_mapping[name_voltage_parameter.split('_volt')[0]]])
            elif 'dac' in name_voltage_parameter:
                y_axis_labels.append(gate_mapping[decadac_mapping[str(get_dac_channel_from_name(name_voltage_parameter))]])
            elif 'lockin' in name_voltage_parameter:
                y_axis_labels.append(gate_mapping[lockin_mapping[name_voltage_parameter.split('_amplitude')[0]]['in']])

        y_axis_label = ', '.join(y_axis_labels) + f' U ({y_axis_prefix}V)'

        fig = plt.figure(figsize = figsize_imshow)
        ax = fig.add_subplot()

        image = ax.imshow(z_axis_values * z_axis_scaling, origin = 'lower', extent = (np.min(values_voltage_setpoint_x), np.max(values_voltage_setpoint_x), np.min(values_voltage_setpoint_y), np.max(values_voltage_setpoint_y)))

        if plot_title: ax.set_title(f'{sample_name}\n{experiment_name}', wrap = True)
        ax.set_xlabel(x_axis_label)

        warnings.warn("More than 2 ranges on any axis not yet supported!", RuntimeWarning)

        # If exactly 2 different ranges on y axis, show additional axis
        if len(names_voltage_setpoint_y) == 2 and not (voltage_setpoint_y_min_values[0] == voltage_setpoint_y_min_values[1] and voltage_setpoint_y_max_values[0] == voltage_setpoint_y_max_values[1]):
            y_axis_1_labels = []
            for name_voltage_parameter in [names_voltage_setpoint_y[0]]:
                if 'keithley' in name_voltage_parameter:
                    y_axis_1_labels.append(gate_mapping[keithley_mapping[name_voltage_parameter.split('_volt')[0]]])
                elif 'dac' in name_voltage_parameter:
                    y_axis_1_labels.append(gate_mapping[decadac_mapping[str(get_dac_channel_from_name(name_voltage_parameter))]])
                elif 'lockin' in name_voltage_parameter:
                    y_axis_1_labels.append(gate_mapping[lockin_mapping[name_voltage_parameter.split('_amplitude')[0]]['in']])
            
            y_axis_1_label = ', '.join(y_axis_1_labels) + f' U ({y_axis_prefix}V)'

            y_axis_2_labels = []
            for name_voltage_parameter in [names_voltage_setpoint_y[1]]:
                if 'keithley' in name_voltage_parameter:
                    y_axis_2_labels.append(gate_mapping[keithley_mapping[name_voltage_parameter.split('_volt')[0]]])
                elif 'dac' in name_voltage_parameter:
                    y_axis_2_labels.append(gate_mapping[decadac_mapping[str(get_dac_channel_from_name(name_voltage_parameter))]])
                elif 'lockin' in name_voltage_parameter:
                    y_axis_2_labels.append(gate_mapping[lockin_mapping[name_voltage_parameter.split('_amplitude')[0]]['in']])
            
            y_axis_2_label = ', '.join(y_axis_2_labels) + f' U ({y_axis_prefix}V)'

            values_y_axis_1 = np.unique(values_voltage_setpoint_y[0])
            values_y_axis_2 = np.unique(values_voltage_setpoint_y[1])

            values_y_axis_1_min = np.min(values_y_axis_1)
            values_y_axis_1_max = np.max(values_y_axis_1)
            values_y_axis_2_min = np.min(values_y_axis_2)
            values_y_axis_2_max = np.max(values_y_axis_2)

            ax.set_ylabel(y_axis_1_label)

            secondary_ax = ax.secondary_yaxis('right', functions = (lambda x: (values_y_axis_2_max - values_y_axis_2_min)/(values_y_axis_1_max - values_y_axis_1_min) * (x - values_y_axis_1_min) + values_y_axis_2_min, lambda x: (values_y_axis_1_max - values_y_axis_1_min)/(values_y_axis_2_max - values_y_axis_2_min) * (x - values_y_axis_2_min) + values_y_axis_1_min))
            secondary_ax.set_ylabel(y_axis_2_label)

        else:
            ax.set_ylabel(y_axis_label)

        fig.colorbar(image, label = z_axis_label, ax = ax)

        base_path, plot_path = pathname_to_save(plot_base_path,
                                                sample_name,
                                                dataset.run_timestamp(),
                                                dataset.exp_name,
                                                f"{'_'.join(name_current_parameter.split('_')[:-1:])}_2D_sweep",
                                                filetype,
                                                flat_file_hirarchy=flat_file_hirarchy)
        if enable_latex_code: save_latex_code(plot_path, sample_name, experiment_name, figsize_imshow)
        save_metadata(sample_config, experiment_metadata, base_path)
        fig.savefig(plot_path)
        plt.close(fig)