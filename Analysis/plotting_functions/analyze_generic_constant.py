import numpy as np
import qcodes as qc
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import scipy
import scipy.interpolate
import scipy.misc
import scipy.optimize
import json
import os
import traceback
from plotting_helpers import *

from plotting_functions.analyze_generic_1D_sweep import *

def analyze_generic_constant(dataset, plot_base_path, filetype = '.png', plot_title=True, enable_latex_code=False, flat_file_hirarchy=False):
    """
    Plot timetrace.

    Plot Lockin_R and keithley_curr vs time (If available), otherwise send it to analyze_generic_1D_sweep()
    """
    sample_config = json.loads(dataset.get_metadata('sample_config'))
    experiment_metadata = json.loads(dataset.get_metadata('experiment_metadata'))
    data = dataset.get_parameter_data()

    # Catch whether timestamps were provided:
    if 'clock' not in data[list(data.keys())[0]].keys():
        analyze_generic_1D_sweep(dataset, plot_base_path, filetype, plot_title, enable_latex_code)
        return

    # Extract metadata
    sample_name = sample_config['sample_name']
    gate_mapping = sample_config['gate_mapping']
    decadac_mapping = sample_config['DECADAC_mapping']
    lockin_mapping = sample_config['Lockin_mapping']
    keithley_mapping = sample_config['Keithley_mapping']

    experiment_name = dataset.exp_name.replace('_', ' ')

    names_current_parameters = []
    for key in data.keys():
        if 'lockin' in key and '_R' in key:
            names_current_parameters.append(key)
        elif '_curr' in key:
            names_current_parameters.append(key)

    assert names_current_parameters != [], "Nothing to plot!"

    for name_current_parameter in names_current_parameters:
        current_parameter = data[name_current_parameter]

        x_axis_values = current_parameter['clock']
        y_axis_values = current_parameter[name_current_parameter]

        y_axis_scaling, y_axis_prefix = si_prefix(np.max(np.abs(y_axis_values)))

        x_axis_label = 'Time (s)'
        
        y_axis_label = ''
        if 'lockin' in name_current_parameter:
            y_axis_label = gate_mapping[lockin_mapping[name_current_parameter.split('_R')[0]]['in']] + f' R ({y_axis_prefix}A)'
        elif 'keithley' in name_current_parameter:
            y_axis_label = gate_mapping[keithley_mapping[name_current_parameter.split('_curr')[0]]] + f' I ({y_axis_prefix}A)'

        fig = plt.figure()
        ax = fig.add_subplot()

        ax.plot(x_axis_values, y_axis_values * y_axis_scaling)

        if plot_title: ax.set_title(f'{sample_name}\n{experiment_name}', wrap = True)
        ax.set_xlabel(x_axis_label)
        ax.set_ylabel(y_axis_label)

        ax.grid(which = 'major', zorder = -1)
        ax.grid(which = 'minor', zorder = -1, alpha = 0.3)
        ax.minorticks_on()

        base_path, plot_path = pathname_to_save(plot_base_path,
                                                sample_name,
                                                dataset.run_timestamp(),
                                                dataset.exp_name,
                                                f"{'_'.join(name_current_parameter.split('_')[:-1:])}_constant",
                                                filetype,
                                                flat_file_hirarchy=flat_file_hirarchy)
        if enable_latex_code: save_latex_code(plot_path, sample_name, experiment_name, matplotlib.rcParams.get('figure.figsize'))
        save_metadata(sample_config, experiment_metadata, base_path)
        fig.savefig(plot_path)
        plt.close(fig)
