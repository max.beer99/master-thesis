"""This script loads the sample_config of run {dataset_id_to_overload}
and overloads the entries specified in {overload}. It then saves the
new sample config to sample_configs_to_overload.json, which is then
used to actually overload the sample_config in
analyze_SET_measurment_interactive_4K.py ."""

#  -------------------------------------------------
#  --------- START OF USER DEFINED CONFIG ----------
#  -------------------------------------------------

database_path = "../../20211101_Lawrance_plotting.db"
path_to_sample_configs_to_overload_json = "../../sample_configs_to_overload.json"

run_ids_to_overload = [366]
overload = {
        "gate_mapping": {
        "1": "TRP",
        "2": "RB1", #TGR
        "3": "NC",
        "4": "RB2", #RB2
        "5": "RP",
        "6": "TRB2",
        "7": "S4",
        "8": "TGR", #RB1
        "9": "ORT",
        "10": "ScrTL",
        "11": "OLT",
        "12": "TLB2",
        "13": "TLP",
        "14": "TGL", #TGL
        "15": "LB1",
        "16": "LP",
        "17": "LB2", # LB2
        "18": "S1",
        "19": "S3",
        "20": "OLB",
        "21": "ORB",
        "22": "TRB1",
        "23": "TLB1",
        "24": "S2"
    }
    # "gate_mapping": {
    #     "1": "TRP",
    #     "2": "TGR",
    #     "3": "NC",
    #     "4": "RB2",
    #     "5": "RP",
    #     "6": "TRB2",
    #     "7": "S4",
    #     "8": "RB1",
    #     "9": "ORT",
    #     "10": "ScrTL",
    #     "11": "OLT",
    #     "12": "TLB2",
    #     "13": "TLP",
    #     "14": "TGL",
    #     "15": "LB1",
    #     "16": "LP",
    #     "17": "LB2",
    #     "18": "S1",
    #     "19": "S3",
    #     "20": "OLB",
    #     "21": "ORB",
    #     "22": "TRB1",
    #     "23": "TLB1",
    #     "24": "S2"
    # }
}

#  -----------------------------------------------
#  --------- END OF USER DEFINED CONFIG ----------
#  -----------------------------------------------


from cProfile import run
import json
from textwrap import indent
import qcodes as qc
import traceback

if not type(run_ids_to_overload) == list:
    run_ids_to_overload = [run_ids_to_overload]

for run_id_to_overload in run_ids_to_overload:
    # load sample_config
    qc.initialise_or_create_database_at(database_path)
    dataset = qc.load_by_id(run_id_to_overload)
    sample_config = json.loads(dataset.get_metadata('sample_config'))

    # overload sample_config
    sample_config.update(overload)

    # load sample_configs_to_overload.json
    sample_configs_to_overload = {}
    try:
        with open(path_to_sample_configs_to_overload_json, 'x') as file:
            json.dump({}, file, indent=4)
    except BaseException:
        pass

    with open(path_to_sample_configs_to_overload_json, 'r') as file:
        sample_configs_to_overload = json.load(file)

    # add entry to sample_configs_to_overload
    # The structure of sample_configs_to_overload is:
    # sample_configs_to_overload = {
    #                               DATABASE_PATH: {
    #                                               RUN_ID: SAMPLE_CONFIG
    #                                               RUN_ID: SAMPLE_CONFIG
    #                                               ...
    #                                               }
    #                               DATABASE_PATH: {
    #                                               RUN_ID: SAMPLE_CONFIG
    #                                               RUN_ID: SAMPLE_CONFIG
    #                                               ...
    #                                               }
    #                               }
    sample_configs_to_overload_database_path = {}
    try:
        sample_configs_to_overload_database_path = sample_configs_to_overload[database_path]
    except KeyError:
        pass
    sample_configs_to_overload_database_path.update({run_id_to_overload: sample_config})
    sample_configs_to_overload[database_path] = sample_configs_to_overload_database_path

    # save sample_configs_to_overload
    with open(path_to_sample_configs_to_overload_json, 'w+') as file:
        json.dump(sample_configs_to_overload, file, indent=4)
