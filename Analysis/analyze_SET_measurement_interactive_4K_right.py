"""Analyze and Plot results from SET_measurement_interactive_4K_right."""


# %% Setup

#  -------------------------------------------------
#  --------- START OF USER DEFINED CONFIG ----------
#  -------------------------------------------------

plot_base_path = "../../plots/"

database_path = "../../20210310_QT477_plotting.db"
# check_datasets_with_ids = list(range(1, 475 + 1))
check_datasets_with_ids = list(range(135, 145 + 1)) # TLM

# database_path = "../../20211101_Lawrance_plotting.db"
# check_datasets_with_ids = list(range(1, 1294 + 1))

filetype = '.png'
plot_title = False
enable_latex_code = True
flat_file_hirarchy = True # Save plots in a flat file hirarchy for easier overleaf upload
font_paths = ["../../fonts/"]
plot_dpi = 600

# filetype = '.png'
# plot_title = True
# enable_latex_code = False
# flat_file_hirarchy = False # Save plots in a flat file hirarchy for easier overleaf upload
# font_paths = ["../../fonts/"]
# plot_dpi = 150

path_to_sample_configs_to_overload_json = "../../sample_configs_to_overload.json"

#  -----------------------------------------------
#  --------- END OF USER DEFINED CONFIG ----------
#  -----------------------------------------------


from random import sample
from importlib_metadata import metadata
import numpy as np
import qcodes as qc
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.font_manager as font_manager
import scipy
import scipy.interpolate
import scipy.misc
import scipy.optimize
import json
import os
import traceback
from plotting_helpers import *
from plotting_functions.analyze_find_SET_operating_points import *
from plotting_functions.analyze_tlm_measurement import *
from plotting_functions.analyze_generic_constant import *
from plotting_functions.analyze_channel_sequence_1D_sweep import *
from plotting_functions.analyze_generic_sequence_1D_sweep import *
from plotting_functions.analyze_generic_1D_sweep import *
from plotting_functions.analyze_generic_2D_sweep import *

# Load fonts
for font in font_manager.findSystemFonts(font_paths):
    font_manager.fontManager.addfont(font)

# Matplotlib rcParams
figsize_imshow = (5, 4)
myRcParams = {
    'savefig.transparent': False,
    'savefig.facecolor': "white",
    'savefig.dpi': plot_dpi,
    'figure.figsize': (5.5, 3.5),
    'font.family': 'Latin Modern Roman',
    'mathtext.fontset': 'custom',
    'mathtext.bf': 'Latin Modern Roman',
    'mathtext.cal': 'Latin Modern Roman',
    'mathtext.it': 'Latin Modern Roman',
    'mathtext.rm': 'Latin Modern Roman',
    'mathtext.sf': 'Latin Modern Roman',
    'mathtext.tt': 'monospace',
    'font.size': 11,
    'axes.titlesize': 'medium',
    'axes.labelsize': 'medium',
    'figure.autolayout': False,
    'figure.constrained_layout.use': True,
    'figure.constrained_layout.h_pad': 0.2,
    'image.aspect': 'auto'
}
# Update rcParams
matplotlib.rcParams.update(matplotlib.rcParamsDefault)
for key in myRcParams.keys():
    matplotlib.rcParams[key] = myRcParams[key]

# Load path_to_sample_configs_to_overload.json to potentially overload
# sample_config
sample_configs_to_overload = {database_path: {}}
try:
    with open(path_to_sample_configs_to_overload_json, 'r') as file:
        sample_configs_to_overload = json.load(file)
    print('Sample config successfully overloaded!')
except BaseException:
    print('No sample config to overload!')
    print('continuing...')
# If sample_configs_to_overload exists but no entries for this specific db
# exist, add dummy key
if database_path not in sample_configs_to_overload.keys():
    sample_configs_to_overload[database_path] = {}

# Import dataset
qc.initialise_or_create_database_at(database_path)


def load_and_plot_star_datasets():
    # Load datasets marked with star
    check_datasets_with_ids.sort()

    all_datasets = []
    for run_id in check_datasets_with_ids:
        curr_dataset = qc.load_by_id(run_id)
        try:
            if curr_dataset.metadata['inspectr_tag'] == 'star':
                all_datasets.append(curr_dataset)
        except KeyError:
            continue

    # Sort datasets by experiment
    datasets_find_SET_operating_points = []
    datasets_tlm_measurement = []
    datasets_generic_constant = []
    datasets_channel_sequence_1D_sweep = []
    datasets_generic_sequence_1D_sweep = []
    datasets_generic_1D_sweep = []
    datasets_generic_2D_sweep = []
    for dataset in all_datasets:
        # Check whether sample_config needs to be overloaded
        if str(dataset.run_id) in sample_configs_to_overload[database_path].keys():
            dataset.add_metadata(tag = 'sample_config',
                                 metadata = json.dumps(sample_configs_to_overload[database_path][str(dataset.run_id)]))

        exp_name = dataset.exp_name.lower()
        if 'tune_set_linear' in exp_name:
            datasets_find_SET_operating_points.append(dataset)
        elif 'tlm' in exp_name:
            datasets_tlm_measurement.append(dataset)
        elif 'constant' in exp_name:
            datasets_generic_constant.append(dataset)
        elif 'hysteresis' in exp_name or 'repeated' in exp_name or 'sequence' in exp_name:
            if 'channel' in exp_name:
                datasets_channel_sequence_1D_sweep.append(dataset)
            else:
                datasets_generic_sequence_1D_sweep.append(dataset)
        elif '1d_sweep' in exp_name:
            datasets_generic_1D_sweep.append(dataset)
        elif '2d_sweep' in exp_name:
            datasets_generic_2D_sweep.append(dataset)
        else:
            print(f'Experiment {exp_name} can not be plotted!')

    # Assemble multi measurement experiment lists
    # New sequence are identified by discontinuities in run_ids or via the sample_config['first_in_sequence'] flag
    datasets_multi_tlm_measurement = []
    datasets_multi_channel_sequence_1D_sweep = []
    datasets_multi_generic_sequence_1D_sweep = []

    for i in range(len(datasets_tlm_measurement)):
        dataset = datasets_tlm_measurement[i]
        sample_config = json.loads(dataset.get_metadata('sample_config'))

        flag_first_in_sequence = False
        try:
            flag_first_in_sequence = sample_config['first_in_sequence']
        except KeyError:
            pass

        if i == 0:
            datasets_multi_tlm_measurement.append([dataset])
            continue
        if dataset.run_id == datasets_multi_tlm_measurement[-1][-1].run_id + 1 and not flag_first_in_sequence:
            datasets_multi_tlm_measurement[-1].append(dataset)
        else:
            datasets_multi_tlm_measurement.append([dataset])

    for i in range(len(datasets_channel_sequence_1D_sweep)):
        dataset = datasets_channel_sequence_1D_sweep[i]
        sample_config = json.loads(dataset.get_metadata('sample_config'))
        
        flag_first_in_sequence = False
        try:
            flag_first_in_sequence = sample_config['first_in_sequence']
        except KeyError:
            pass

        if i == 0:
            datasets_multi_channel_sequence_1D_sweep.append([dataset])
            continue
        if dataset.run_id == datasets_multi_channel_sequence_1D_sweep[-1][-1].run_id + 1 and not flag_first_in_sequence:
            datasets_multi_channel_sequence_1D_sweep[-1].append(dataset)
        else:
            datasets_multi_channel_sequence_1D_sweep.append([dataset])

    for i in range(len(datasets_generic_sequence_1D_sweep)):
        dataset = datasets_generic_sequence_1D_sweep[i]
        sample_config = json.loads(dataset.get_metadata('sample_config'))
        
        flag_first_in_sequence = False
        try:
            flag_first_in_sequence = sample_config['first_in_sequence']
        except KeyError:
            pass

        if i == 0:
            datasets_multi_generic_sequence_1D_sweep.append([dataset])
            continue
        if dataset.run_id == datasets_multi_generic_sequence_1D_sweep[-1][-1].run_id + 1 and not flag_first_in_sequence:
            datasets_multi_generic_sequence_1D_sweep[-1].append(dataset)
        else:
            datasets_multi_generic_sequence_1D_sweep.append([dataset])

    # Plot datasets
    for dataset in datasets_find_SET_operating_points:
        print(20*'-')
        try:
            print(f'Plotting run with run_id {dataset.run_id}...')
            analyze_find_SET_operating_points(dataset, plot_base_path=plot_base_path, filetype=filetype, plot_title=plot_title, enable_latex_code=enable_latex_code, flat_file_hirarchy=flat_file_hirarchy)
        except BaseException as err:
            print(f'An Error has occured during plotting!')
            traceback.print_exc()

    for dataset_list in datasets_multi_tlm_measurement:
        print(20*'-')
        try:
            print(f"Plotting run with run_id {', '.join([str(dataset.run_id) for dataset in dataset_list])}...")
            analyze_tlm_measurement(dataset_list, plot_base_path=plot_base_path, filetype=filetype, plot_title=plot_title, enable_latex_code=enable_latex_code, flat_file_hirarchy=flat_file_hirarchy)
        except BaseException as err:
            print(f'An Error has occured during plotting!')
            traceback.print_exc()
    
    for dataset in datasets_generic_constant:
        print(20*'-')
        try:
            print(f'Plotting run with run_id {dataset.run_id}...')
            analyze_generic_constant(dataset, plot_base_path=plot_base_path, filetype=filetype, plot_title=plot_title, enable_latex_code=enable_latex_code, flat_file_hirarchy=flat_file_hirarchy)
        except BaseException as err:
            print(f'An Error has occured during plotting!')
            traceback.print_exc()

    for dataset_list in datasets_multi_channel_sequence_1D_sweep:
        print(20*'-')
        try:
            print(f"Plotting run with run_id {', '.join([str(dataset.run_id) for dataset in dataset_list])}...")
            analyze_channel_sequence_1D_sweep(dataset_list, plot_base_path=plot_base_path, filetype=filetype, plot_title=plot_title, enable_latex_code=enable_latex_code, flat_file_hirarchy=flat_file_hirarchy)
        except BaseException as err:
            print(f'An Error has occured during plotting!')
            traceback.print_exc()

    for dataset_list in datasets_multi_generic_sequence_1D_sweep:
        print(20*'-')
        try:
            print(f"Plotting run with run_id {', '.join([str(dataset.run_id) for dataset in dataset_list])}...")
            analyze_generic_sequence_1D_sweep(dataset_list, plot_base_path=plot_base_path, filetype=filetype, plot_title=plot_title, enable_latex_code=enable_latex_code, flat_file_hirarchy=flat_file_hirarchy)
        except BaseException as err:
            print(f'An Error has occured during plotting!')
            traceback.print_exc()

    for dataset in datasets_generic_1D_sweep:
        print(20*'-')
        try:
            print(f'Plotting run with run_id {dataset.run_id}...')
            analyze_generic_1D_sweep(dataset, plot_base_path=plot_base_path, filetype=filetype, plot_title=plot_title, enable_latex_code=enable_latex_code, flat_file_hirarchy=flat_file_hirarchy)
        except BaseException as err:
            print(f'An Error has occured during plotting!')
            traceback.print_exc()

    for dataset in datasets_generic_2D_sweep:
        print(20*'-')
        try:
            print(f'Plotting run with run_id {dataset.run_id}...')
            analyze_generic_2D_sweep(dataset, plot_base_path=plot_base_path, filetype=filetype, plot_title=plot_title, enable_latex_code=enable_latex_code, flat_file_hirarchy=flat_file_hirarchy, figsize_imshow=figsize_imshow)
        except BaseException as err:
            print(f'An Error has occured during plotting!')
            traceback.print_exc()

load_and_plot_star_datasets()
