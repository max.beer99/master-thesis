depth(2.0)
height(2.0)
delta(5 * dbu)

oxide_thicknesses = [0.005, 0.010, 0.010]

mesa = layer("4/0")
ohmics = layer("5/0")
L1_oxide = layer("9/0")
L1 = layer("6/0")
L1_fine = layer("10/0")

L2_oxide = layer("11/0")
L2 = layer("7/0")
L2_fine = layer("12/0")

L3_oxide = layer("13/0")
L3 = layer("8/0")
L3_fine = layer("14/0")

Si_bulk = bulk
mask(mesa.inverted).etch(0.070, 0, :into => Si_bulk)
m_ohmics = mask(ohmics).grow(0.155, 0)

oxide_L1 = mask(L1_oxide).grow(oxide_thicknesses[0], oxide_thicknesses[0], :mode => :round)
m_L1 = mask(L1).grow(0.020, 0)
m_L1_fine = mask(L1_fine).grow(0.020, 0)

oxide_L2 = mask(L2_oxide).grow(oxide_thicknesses[1], oxide_thicknesses[1], :mode => :round)
m_L2 = mask(L2).grow(0.027, 0)
m_L2_fine = mask(L2_fine).grow(0.027, 0)

oxide_L3 = mask(L3_oxide).grow(oxide_thicknesses[2], oxide_thicknesses[2], :mode => :round)
m_L3 = mask(L3).grow(0.034, 0)
m_L3_fine = mask(L3_fine).grow(0.034, 0)

output("4/0", Si_bulk)
output("5/0", m_ohmics)
output("9/0", oxide_L1)
output("6/0", m_L1)
output("10/0", m_L1_fine)
output("11/0", oxide_L2)
output("7/0", m_L2)
output("12/0", m_L2_fine)
output("13/0", oxide_L3)
output("8/0", m_L3)
output("14/0", m_L3_fine)