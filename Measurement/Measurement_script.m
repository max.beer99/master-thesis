%% Folder creation
%Documantation: This block creates a folder for your group and the sample
%you are currently measuring.

%----------IMPORTANT----------
%This is the !only! block where you are allowed to change the code!
%Please enter:
%-Your group name
%-The sample you are measuring

formatOut   = 'yyyy_mm_dd_HHMM';

sample      = 'Lawrance_Batch_1_1_Chip_2_C4_Right_SET';
rootfolder = 'C:\Users\lab2\Documents\Beer\Data\sample_';

try
    save([rootfolder sample '\try.m'], 'group');
    delete([rootfolder sample '\try.m']);
catch
    mkdir([rootfolder sample ]); 
end
sminitdisp();
smget(1:13);


%% Transport current versus TG, B1, B2, P -> Inducing
smset('AUXMAX',1.5);
smset('AUXMIN',-0.5);
disp('-------------------------------------------------');
disp('What bias voltage do you want to apply (V)?');
prompt0='>V_Bias> ';
VBias =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What Voltage range do you want to apply (in [0V,2V])?');
prompt0='>Min V_Topgate> ';
V1 =   str2num(input(prompt0,'s'));
prompt0='>Max V_Topgate> ';
V2 =   str2num(input(prompt0,'s'));

while (V2 > 1.5 | V1 < 0)
    display('Input out of voltage range! Please enter a value between 0 and 1.5V.')
    disp('What Voltage range do you want to apply (in [0V,1.5V])?');
    prompt0='>Min V> ';
    V1 =   str2num(input(prompt0,'s'));
    prompt0='>Max V> ';
    V2 =   str2num(input(prompt0,'s'));
    
end
disp('-------------------------------------------------');
disp('How many data points do you want to record?');
prompt0='>npoints> ';
npoints =  str2num(input(prompt0,'s'));

ramptime=0.01;

%--------------------Loop setup----------------------------------
clear scanGateTestinit
scanGateTestinit.loops(1).npoints = npoints;
scanGateTestinit.loops(1).rng = [V1 V2];
scanGateTestinit.loops(1).setchan = {'AUX1', 'AUX2', 'AUX3', 'AUX4'};
scanGateTestinit.loops(1).getchan = {'R' 'amp' 'offset' 'Theta' 'freq' 'AUX1' 'AUX2' 'AUX3' 'AUX4'}; %
scanGateTestinit.loops(1).ramptime = ramptime;

%--------------------Figure setup--------------------------------
scanGateTestinit.figure = 13;

%-----Signal-----
scanGateTestinit.disp(1).loop = 1;
scanGateTestinit.disp(1).channel = 1;
scanGateTestinit.disp(1).dim = 1;
%-----Phase------
scanGateTestinit.disp(2).loop = 1;
scanGateTestinit.disp(2).channel = 4;
scanGateTestinit.disp(2).dim = 1;

disp('-------------------------------------------------');
disp('Is the top gate connected to Auxiliary Output Port 1? Are the barrier gates connected to Auxiliary Output Port 2 and 3? Is the plunger gate connected to Auxiliary Output Port 4?');
prompt0='>y/n> ';
check =  input(prompt0,'s');

if (check == 'y')
    %--------------------Loop run----------------------------------
    disp('------------Bias voltage is ramped------------');
    smset('amp',VBias,0.1);
    disp('------------Start values are applied------------');
    smset('AUX1',V1,0.1);
    smset('AUX2',V1,0.1);
    smset('AUX3',V1,0.1);
    smset('AUX4',V1,0.1);
    disp('------------Sweep start------------');
    smrun(scanGateTestinit,[rootfolder sample '\' datestr(now,formatOut) '_AccumulationSweep_Range-'  num2str(V1) 'Vto' num2str(V2) 'V_BiasVoltage-' num2str(VBias) '.mat']);

    disp('------------Sweep end------------');
else
    disp('Check connected cables and rerun the block.')
end
smset('AUXMAX',1.5);
smset('AUXMIN',-0.5);


%% Transport current versus barrier voltages (fixed topgate, plunger voltage) -> Pinchoff 
smset('AUXMAX',1.5);
smset('AUXMIN',-0.5);
disp('-------------------------------------------------');
disp('What bias voltage do you want to apply (V)?');
prompt0='>V_Bias> ';
VBias =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What topgate voltage do you want to apply (in [0V,1.5V])?');
prompt0='>V_Topgate> ';
VTop =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What plunger voltages do you want to apply (in [-0.5V,1.5V])?');
prompt0='>V_Plunger> ';
VPlunger =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What barrier voltage range do you want to apply (in [0V,1.5V])?');
prompt0='>Min V_Barriers> ';
V1 =  str2num(input(prompt0,'s'));
prompt0='>Max V_Barriers> ';
V2 =  str2num(input(prompt0,'s'));
while (V2 > 1.5 | V1 < 0)
    display('Input out of voltage range! Please enter a value between 0 and 1.5V.')
    disp('What barrier voltage range do you want to apply (in [0V,1.5V])?');
    prompt0='>Min V_Barriers> ';
    V1 =  str2num(input(prompt0,'s'));
    prompt0='>Max V_Barriers> ';
    V2 =  str2num(input(prompt0,'s'));
end
disp('-------------------------------------------------');
disp('What barrier voltage do you want to apply to the OPEN barrier (in [0V,1.5V])?');
prompt0='>V_BarOpen> ';
V_BarOpen =  str2num(input(prompt0,'s'));
while (V_BarOpen > 1.5 | V_BarOpen < 0)
    display('Input out of voltage range! Please enter a value between 0 and 1.5V.')
    disp('What barrier voltage do you want to apply to the OPEN barrier (in [0V,1.5V])?');
    prompt0='>V_BarOpen> ';
    V_BarOpen =  str2num(input(prompt0,'s'));
end
disp('-------------------------------------------------');
disp('How many data points do you want to record?');
prompt0='>npoints> ';
npoints =  str2num(input(prompt0,'s'));

ramptime=0.01;

%--------------------Barrier 1----------------------------------
%--------------------Loop setup----------------------------------
clear scanGateTestinit
scanGateTestinit.loops(1).npoints = npoints;
scanGateTestinit.loops(1).rng = [V2 V1];
scanGateTestinit.loops(1).setchan = {'AUX2'};
scanGateTestinit.loops(1).getchan = {'R' 'amp' 'offset' 'Theta' 'freq' 'AUX1' 'AUX2' 'AUX3' 'AUX4'}; %
scanGateTestinit.loops(1).ramptime = ramptime;

%--------------------Figure setup--------------------------------
scanGateTestinit.figure = 14;

%-----Signal-----
scanGateTestinit.disp(1).loop = 1;
scanGateTestinit.disp(1).channel = 1;
scanGateTestinit.disp(1).dim = 1;
%-----Phase------
scanGateTestinit.disp(2).loop = 1;
scanGateTestinit.disp(2).channel = 4;
scanGateTestinit.disp(2).dim = 1;

disp('-------------------------------------------------');
disp('Is the top gate connected to Auxiliary Output Port 1? Are the barrier gates connected to Auxiliary Output Port 2 and 3? Is the plunger gate connected to Auxiliary Output Port 4?');
prompt0='>y/n> ';
check =  input(prompt0,'s');

if (check == 'y')
    %--------------------Loop run----------------------------------
    disp('------------Bias voltage is ramped------------');
    smset('amp',VBias,0.1);
    disp('------------Topgate voltage is ramped------------');
    smset('AUX1',VTop,0.1);
    disp('------------Plunger voltage is ramped------------');
    smset('AUX4',VPlunger,0.1);
    disp('------------Open barrier voltage is ramped------------');
    smset('AUX3',V_BarOpen,0.1);
    disp('------------Start values are applied------------');
    smset('AUX2',V2,0.1);
    disp('------------Sweep 1/2 start------------');
    smrun(scanGateTestinit,[rootfolder sample '\' datestr(now,formatOut) '_Barrier1Sweep_Range-'  num2str(V1) 'Vto' num2str(V2) 'V_BiasVoltage-' num2str(VBias) 'V_TopgateVoltage-' num2str(VTop) 'V_PlungerVoltage-' num2str(VPlunger) 'V_BarrierOpenVoltage-' num2str(V_BarOpen) '.mat']);
    smset('AUX2',V1,0.1);
    disp('------------Sweep 1/2 end------------');
else
    disp('Check connected cables and rerun the block.')
end

%--------------------Barrier 2----------------------------------
%--------------------Loop setup----------------------------------
clear scanGateTestinit
scanGateTestinit.loops(1).npoints = npoints;
scanGateTestinit.loops(1).rng = [V2 V1];
scanGateTestinit.loops(1).setchan = {'AUX3'};
scanGateTestinit.loops(1).getchan = {'R' 'amp' 'offset' 'Theta' 'freq' 'AUX1' 'AUX2' 'AUX3' 'AUX4'}; %
scanGateTestinit.loops(1).ramptime = ramptime;

%--------------------Figure setup--------------------------------
scanGateTestinit.figure = 15;

%-----Signal-----
scanGateTestinit.disp(1).loop = 1;
scanGateTestinit.disp(1).channel = 1;
scanGateTestinit.disp(1).dim = 1;
%-----Phase------
scanGateTestinit.disp(2).loop = 1;
scanGateTestinit.disp(2).channel = 4;
scanGateTestinit.disp(2).dim = 1;

disp('-------------------------------------------------');
disp('Is the top gate connected to Auxiliary Output Port 1? Are the barrier gates connected to Auxiliary Output Port 2 and 3? Is the plunger gate connected to Auxiliary Output Port 4?');
prompt0='>y/n> ';
check =  input(prompt0,'s');

if (check == 'y')
    %--------------------Loop run----------------------------------
    disp('------------Bias voltage is ramped------------');
    smset('amp',VBias,0.1);
    disp('------------Topgate voltage is ramped------------');
    smset('AUX1',VTop,0.1);
    disp('------------Plunger voltage is ramped------------');
    smset('AUX4',VPlunger,0.1);
    disp('------------Open barrier voltage is ramped------------');
    smset('AUX2',V_BarOpen,0.1);
    disp('------------Start values are applied------------');
    smset('AUX3',V2,0.1);
    disp('------------Sweep 2/2 start------------');
    smrun(scanGateTestinit,[rootfolder sample '\' datestr(now,formatOut) '_Barrier2Sweep_Range-'  num2str(V1) 'Vto' num2str(V2) 'V_BiasVoltage-' num2str(VBias) 'V_TopgateVoltage-' num2str(VTop) 'V_PlungerVoltage-' num2str(VPlunger) 'V_BarrierOpenVoltage-' num2str(V_BarOpen) '.mat']);
    smset('AUX3',V1,0.1);
    disp('------------Sweep 2/2 end------------');
else
    disp('Check connected cables and rerun the block.')
end

smset('AUXMAX',1.5);
smset('AUXMIN',-0.5);


%% 2D Pinchoff sweep (barrier 1 vs barrier 2)
smset('AUXMAX',1.5);
smset('AUXMIN',-0.5);
disp('-------------------------------------------------');
disp('What bias voltage do you want to apply (V)?');
prompt0='>V_Bias> ';
VBias =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What topgate voltages do you want to apply (in [0V,1.5V])?');
prompt0='>V_Topgate> ';
VTop =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What plunger voltages do you want to apply (in [-0.5V,1.5V])?');
prompt0='>V_Plunger> ';
VPlunger =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What barrier 1 voltage range do you want to apply (in [0V,1.5V])?');
prompt0='>Min V_Barriers> ';
VBarrier1_1 =  str2num(input(prompt0,'s'));
prompt0='>Max V_Barriers> ';
VBarrier1_2 =  str2num(input(prompt0,'s'));
while (VBarrier1_2 > 1.5 | VBarrier1_1 < 0)
    display('Input out of voltage range! Please enter a value between 0 and 1.5V.')
    disp('What barrier 1 voltage range do you want to apply (in [0V,1.5V])?');
    prompt0='>Min V_Barriers> ';
    VBarrier1_1 =  str2num(input(prompt0,'s'));
    prompt0='>Max V_Barriers> ';
    VBarrier1_2 =  str2num(input(prompt0,'s'));
end
disp('-------------------------------------------------');
disp('What barrier 2 voltage range do you want to apply (in [0V,1.5V])?');
prompt0='>Min V_Barriers> ';
VBarrier2_1 =  str2num(input(prompt0,'s'));
prompt0='>Max V_Barriers> ';
VBarrier2_2 =  str2num(input(prompt0,'s'));
while (VBarrier2_2 > 1.5 | VBarrier2_1 < 0)
    display('Input out of voltage range! Please enter a value between 0 and 1.5V.')
    disp('What barrier 2 voltage range do you want to apply (in [0V,1.5V])?');
    prompt0='>Min V_Barriers> ';
    VBarrier2_1 =  str2num(input(prompt0,'s'));
    prompt0='>Max V_Barriers> ';
    VBarrier2_2 =  str2num(input(prompt0,'s'));
end
disp('-------------------------------------------------');
disp('How many data points do you want to record?');
prompt0='>X npoints> ';
npoints_X =  str2num(input(prompt0,'s'));
prompt0='>Y npoints> ';
npoints_Y =  str2num(input(prompt0,'s'));

ramptime=0.01;


%--------------------Loop setup----------------------------------
clear scanGateTest
scanGateTest.loops(1).npoints = npoints_X;
scanGateTest.loops(1).rng = [VBarrier1_2 VBarrier1_1];
scanGateTest.loops(1).setchan = {'AUX2'}; % Barrier 1
scanGateTest.loops(1).getchan = {'R' 'amp' 'offset' 'Theta' 'freq' 'AUX1' 'AUX2' 'AUX3' 'AUX4'}; %
scanGateTest.loops(1).ramptime = ramptime;

scanGateTest.loops(2).npoints = npoints_Y;
scanGateTest.loops(2).rng = [VBarrier2_2 VBarrier2_1];
scanGateTest.loops(2).setchan = {'AUX3'}; % Barrier 2
scanGateTest.loops(2).getchan = {'R' 'amp' 'offset' 'Theta' 'freq' 'AUX1' 'AUX2' 'AUX3' 'AUX4'}; %
scanGateTest.loops(2).ramptime = ramptime;
%--------------------Figure setup--------------------------------
scanGateTest.figure = 41;
scanGateTest.fighold = 1;
scanGateTest.disp(1).loop = 1;
scanGateTest.disp(1).channel = 1;
scanGateTest.disp(1).dim = 1;

scanGateTest.disp(2).loop = 2;
scanGateTest.disp(2).channel = 1;
scanGateTest.disp(2).dim = 2;

disp('-------------------------------------------------');
disp('Is the top gate connected to Auxiliary Output Port 1? Are the barrier gates connected to Auxiliary Output Port 2 and 3? Is the plunger gate connected to Auxiliary Output Port 4?');
prompt0='>y/n> ';
check =  input(prompt0,'s');

if (check == 'y')
    %--------------------Loop run----------------------------------
    disp('------------Bias voltage is ramped------------');
    smset('amp',VBias,0.1);
    disp('------------Topgate voltage is ramped------------');
    smset('AUX1',VTop,0.1);
    disp('------------Plunger voltage is ramped------------');
    smset('AUX4',VPlunger,0.1);
    disp('------------Start values are applied------------');
    smset('AUX2',VBarrier1_2,0.1);
    smset('AUX3',VBarrier2_2,0.1);
    disp('------------Sweep start------------');
    smrun(scanGateTest,[rootfolder sample '\' datestr(now,formatOut) '_2DPinchoff_Barrier1Sweep_Range-' num2str(VBarrier1_1) 'Vto' num2str(VBarrier1_2) 'V' '_Barrier2Sweep_Range-'  num2str(VBarrier2_1) 'Vto' num2str(VBarrier2_2) 'V_BiasVoltage-' num2str(VBias) 'V_TopgateVoltage-' num2str(VTop) 'V_PlungerVoltage-' num2str(VPlunger) '.mat']);

    disp('------------Sweep end------------');
else
    disp('Check connected cables and rerun the block.')
end
smset('AUXMAX',1.5);
smset('AUXMIN',-0.5);


%% 1D Coulomb blockade sweep (transport current versus barrier 1/barrier2)
smset('AUXMAX',1.5);
smset('AUXMIN',-0.5);
disp('-------------------------------------------------');
disp('What bias voltage do you want to apply (V)?');
prompt0='>V_Bias> ';
VBias =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What topgate voltages do you want to apply (in [0V,1.5V])?');
prompt0='>V_Topgate> ';
VTop =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What plunger voltages do you want to apply (in [-0.5V,1.5V])?');
prompt0='>V_Plunger> ';
VPlunger =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What barrier 1 voltage range do you want to apply (in [0V,1.5V])? -> Diagonal linecut');
prompt0='>Min V_B1> ';
VB1_1 =  str2num(input(prompt0,'s'));
prompt0='>Max V_B1> ';
VB1_2 =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What barrier 2 voltage range do you want to apply (in [0V,1.5V])? -> Diagonal linecut');
prompt0='>Min V_B2> ';
VB2_1 =  str2num(input(prompt0,'s'));
prompt0='>Max V_B2> ';
VB2_2 =  str2num(input(prompt0,'s'));
while (VB1_2 > 1.5 | VB1_1 < 0)
    display('Input out of voltage range! Please enter a value between 0 and 1.5V.')
    disp('What barrier 1 voltage range do you want to apply (in [0V,1.5V])? -> Diagonal linecut');
    prompt0='>Min V_B1> ';
    VB1_1 =  str2num(input(prompt0,'s'));
    prompt0='>Max V_B1> ';
    VB1_2 =  str2num(input(prompt0,'s'));
end
while (VB2_2 > 1.5 | VB2_1 < 0)
    display('Input out of voltage range! Please enter a value between 0 and 1.5V.')
    disp('What barrier 2 voltage range do you want to apply (in [0V,1.5V])? -> Diagonal linecut');
    prompt0='>Min V_B2> ';
    VB2_1 =  str2num(input(prompt0,'s'));
    prompt0='>Max V_B2> ';
    VB2_2 =  str2num(input(prompt0,'s'));
end

disp('-------------------------------------------------');
disp('How many data points do you want to record?');
prompt0='>npoints> ';
npoints =  str2num(input(prompt0,'s'));

ramptime=0.01;


%--------------------Loop setup----------------------------------
clear scanGateTestinit
m1 = (VB1_2 - VB1_1)/npoints;
b1 = VB1_1;
m2 = (VB2_2 - VB2_1)/npoints;
b2 = VB2_1;
scanGateTestinit.loops(1).npoints = npoints;
scanGateTestinit.loops(1).rng = [VB1_1 VB1_2];
scanGateTestinit.loops(1).setchan = {'AUX2' 'AUX3'};
scanGateTestinit.loops(1).trafofn(2).fn = @(x,y) (x(1) - b1)/m1*m2+b2;
scanGateTestinit.loops(1).trafofn(2).args = {};
scanGateTestinit.loops(1).getchan = {'R' 'amp' 'offset' 'Theta' 'freq' 'AUX1' 'AUX2' 'AUX3' 'AUX4'}; %
scanGateTestinit.loops(1).ramptime = ramptime;

%--------------------Figure setup--------------------------------
scanGateTestinit.figure = 17;

%-----Signal-----
scanGateTestinit.disp(1).loop = 1;
scanGateTestinit.disp(1).channel = 1;
scanGateTestinit.disp(1).dim = 1;
%-----Phase------
scanGateTestinit.disp(2).loop = 1;
scanGateTestinit.disp(2).channel = 4;
scanGateTestinit.disp(2).dim = 1;

disp('-------------------------------------------------');
disp('Is the top gate connected to Auxiliary Output Port 1? Are the barrier gates connected to Auxiliary Output Port 2 and 3? Is the plunger gate connected to Auxiliary Output Port 4?');
prompt0='>y/n> ';
check =  input(prompt0,'s');

if (check == 'y')
    %--------------------Loop run----------------------------------
    disp('------------Bias voltage is ramped------------');
    smset('amp',VBias,0.1);
    disp('------------Topgate voltages is ramped------------');
    smset('AUX1',VTop,0.1);
    disp('------------Plunger voltage is ramped------------');
    smset('AUX4',VPlunger,0.1);
    disp('------------Start values are applied------------');
    smset('AUX2',VB1_1,0.1);
    smset('AUX3',VB2_1,0.1);
    disp('------------Sweep start------------');
    smrun(scanGateTestinit,[rootfolder sample '\' datestr(now,formatOut) '_Barrier1Sweep_Range-' num2str(VB1_1) 'Vto' num2str(VB1_2) 'V' '_Barrier2Sweep_Range-'  num2str(VB2_1) 'Vto' num2str(VB2_2) 'V_BiasVoltage-' num2str(VBias) 'V_TopgateVoltage-' num2str(VTop) 'V_PlungerVoltage-' num2str(VPlunger) '.mat']);
    disp('------------Sweep end------------');
else
    disp('Check connected cables and rerun the block.')
end


smset('AUXMAX',1.5);
smset('AUXMIN',-0.5);


%% LOOP 1D Coulomb blockade sweep (transport current versus barrier 1/barrier2) Saving is fucked up
smset('AUXMAX',1.5);
smset('AUXMIN',-0.5);
disp('-------------------------------------------------');
disp('What bias voltage do you want to apply (V)?');
prompt0='>V_Bias> ';
VBias =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What topgate voltages do you want to apply (in [0V,1.5V])?');
prompt0='>V_Topgate> ';
VTop =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What plunger voltages do you want to apply (in [-0.5V,1.5V])?');
prompt0='>V_Plunger> ';
VPlunger =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What barrier 1 voltage range do you want to apply (in [0V,1.5V])? -> Diagonal linecut');
prompt0='>Min V_B1> ';
VB1_1 =  str2num(input(prompt0,'s'));
prompt0='>Max V_B1> ';
VB1_2 =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What barrier 2 voltage range do you want to apply (in [0V,1.5V])? -> Diagonal linecut');
prompt0='>Min V_B2> ';
VB2_1 =  str2num(input(prompt0,'s'));
prompt0='>Max V_B2> ';
VB2_2 =  str2num(input(prompt0,'s'));
while (VB1_2 > 1.5 | VB1_1 < 0)
    display('Input out of voltage range! Please enter a value between 0 and 1.5V.')
    disp('What barrier 1 voltage range do you want to apply (in [0V,1.5V])? -> Diagonal linecut');
    prompt0='>Min V_B1> ';
    VB1_1 =  str2num(input(prompt0,'s'));
    prompt0='>Max V_B1> ';
    VB1_2 =  str2num(input(prompt0,'s'));
end
while (VB2_2 > 1.5 | VB2_1 < 0)
    display('Input out of voltage range! Please enter a value between 0 and 1.5V.')
    disp('What barrier 2 voltage range do you want to apply (in [0V,1.5V])? -> Diagonal linecut');
    prompt0='>Min V_B2> ';
    VB2_1 =  str2num(input(prompt0,'s'));
    prompt0='>Max V_B2> ';
    VB2_2 =  str2num(input(prompt0,'s'));
end

disp('-------------------------------------------------');
disp('How many data points do you want to record?');
prompt0='>npoints> ';
npoints =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('How many loops do you want?');
prompt0='>Loops> ';
n_loops =  str2num(input(prompt0,'s'));

ramptime=0.01;

for nn=1:n_loops
    %--------------------Loop setup----------------------------------
    clear scanGateTestinit
    m1 = (VB1_2 - VB1_1)/npoints;
    b1 = VB1_1;
    m2 = (VB2_2 - VB2_1)/npoints;
    b2 = VB2_1;
    scanGateTestinit.loops(1).npoints = npoints;
    scanGateTestinit.loops(1).rng = [VB1_1 VB1_2];
    scanGateTestinit.loops(1).setchan = {'AUX2' 'AUX3'};
    scanGateTestinit.loops(1).trafofn(2).fn = @(x,y) (x(1) - b1)/m1*m2+b2;
    scanGateTestinit.loops(1).trafofn(2).args = {};
    scanGateTestinit.loops(1).getchan = {'R' 'amp' 'offset' 'Theta' 'freq' 'AUX1' 'AUX2' 'AUX3' 'AUX4'}; %
    scanGateTestinit.loops(1).ramptime = ramptime;

    %--------------------Figure setup--------------------------------
    scanGateTestinit.figure = 17;

    %-----Signal-----
    scanGateTestinit.disp(1).loop = 1;
    scanGateTestinit.disp(1).channel = 1;
    scanGateTestinit.disp(1).dim = 1;
    %-----Phase------
    scanGateTestinit.disp(2).loop = 1;
    scanGateTestinit.disp(2).channel = 4;
    scanGateTestinit.disp(2).dim = 1;

    %--------------------Loop run----------------------------------
    disp('------------Bias voltage is ramped------------');
    smset('amp',VBias,0.1);
    disp('------------Topgate voltages is ramped------------');
    smset('AUX1',VTop,0.1);
    disp('------------Plunger voltage is ramped------------');
    smset('AUX4',VPlunger,0.1);
    disp('------------Start values are applied------------');
    smset('AUX2',VB1_1,0.1);
    smset('AUX3',VB2_1,0.1);
    disp('------------Sweep start------------');
    smrun(scanGateTestinit,[rootfolder sample '\' datestr(now,formatOut) '_Barrier1Sweep_Range-' num2str(VB1_1) 'Vto' num2str(VB1_2) 'V' '_Barrier2Sweep_Range-'  num2str(VB2_1) 'Vto' num2str(VB2_2) 'V_BiasVoltage-' num2str(VBias) 'V_TopgateVoltage-' num2str(VTop) 'V_PlungerVoltage-' num2str(VPlunger) '.mat']);
    disp('------------Sweep end------------');

end

smset('AUXMAX',1.5);
smset('AUXMIN',-0.5);


%% 2D Pinchoff sweep (barrier 1,2 vs plunger)
smset('AUXMAX',2);
smset('AUXMIN',-0.5);
disp('-------------------------------------------------');
disp('What bias voltage do you want to apply (V)?');
prompt0='>V_Bias> ';
VBias =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What topgate voltages do you want to apply (in [0V,1.5V])?');
prompt0='>V_Topgate> ';
VTop =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What barrier 1 voltage range do you want to apply (in [0V,1.5V])?');
prompt0='>Min V_Barriers> ';
VBarrier1_1 =  str2num(input(prompt0,'s'));
prompt0='>Max V_Barriers> ';
VBarrier1_2 =  str2num(input(prompt0,'s'));
while (VBarrier1_2 > 1.5 | VBarrier1_1 < 0)
    display('Input out of voltage range! Please enter a value between 0 and 1.5V.')
    disp('What barrier 1 voltage range do you want to apply (in [0V,1.5V])?');
    prompt0='>Min V_Barriers> ';
    VBarrier1_1 =  str2num(input(prompt0,'s'));
    prompt0='>Max V_Barriers> ';
    VBarrier1_2 =  str2num(input(prompt0,'s'));
end
disp('-------------------------------------------------');
disp('What barrier 2 voltage range do you want to apply (in [0V,1.5V])?');
prompt0='>Min V_Barriers> ';
VBarrier2_1 =  str2num(input(prompt0,'s'));
prompt0='>Max V_Barriers> ';
VBarrier2_2 =  str2num(input(prompt0,'s'));
while (VBarrier2_2 > 1.5 | VBarrier2_1 < 0)
    display('Input out of voltage range! Please enter a value between 0 and 1.5V.')
    disp('What barrier 2 voltage range do you want to apply (in [0V,1.5V])?');
    prompt0='>Min V_Barriers> ';
    VBarrier2_1 =  str2num(input(prompt0,'s'));
    prompt0='>Max V_Barriers> ';
    VBarrier2_2 =  str2num(input(prompt0,'s'));
end
disp('-------------------------------------------------');
disp('What Plunger voltage range do you want to apply (in [-0.5V,1.5V])?');
prompt0='>Min V_Plunger> ';
VPlunger_1 =  str2num(input(prompt0,'s'));
prompt0='>Max V_Plunger> ';
VPlunger_2 =  str2num(input(prompt0,'s'));
while (VPlunger_2 > 1.5 | VPlunger_1 < -0.5)
    display('Input out of voltage range! Please enter a value between -0.5 and 1.5V.')
    disp('What Plunger voltage range do you want to apply (in [-0.5V,1.5V])?');
    prompt0='>Min V_Plunger> ';
    VPlunger_1 =  str2num(input(prompt0,'s'));
    prompt0='>Max V_Plunger> ';
    VPlunger_2 =  str2num(input(prompt0,'s'));
end
disp('-------------------------------------------------');
disp('How many data points do you want to record?');
prompt0='>X npoints> ';
npoints_X =  str2num(input(prompt0,'s'));
prompt0='>Y npoints> ';
npoints_Y =  str2num(input(prompt0,'s'));

ramptime=0.01;


%--------------------Loop setup----------------------------------
clear scanGateTest
m1 = (VBarrier1_1 - VBarrier1_2)/npoints_X;
b1 = VBarrier1_2;
m2 = (VBarrier2_1 - VBarrier2_2)/npoints_X;
b2 = VBarrier2_2;
scanGateTest.loops(1).npoints = npoints_X;
scanGateTest.loops(1).rng = [VBarrier1_2 VBarrier1_1];
scanGateTest.loops(1).setchan = {'AUX2' 'AUX3'}; % Barrier 1,2
scanGateTest.loops(1).trafofn(2).fn = @(x,y) (x(1) - b1)/m1*m2+b2;
scanGateTest.loops(1).getchan = {'R' 'amp' 'offset' 'Theta' 'freq' 'AUX1' 'AUX2' 'AUX3' 'AUX4'}; %
scanGateTest.loops(1).ramptime = ramptime;

scanGateTest.loops(2).npoints = npoints_Y;
scanGateTest.loops(2).rng = [VPlunger_2 VPlunger_1];
scanGateTest.loops(2).setchan = {'AUX4'}; % Barrier 2
scanGateTest.loops(2).getchan = {'R' 'amp' 'offset' 'Theta' 'freq' 'AUX1' 'AUX2' 'AUX3' 'AUX4'}; %
scanGateTest.loops(2).ramptime = ramptime;
%--------------------Figure setup--------------------------------
scanGateTest.figure = 41;
scanGateTest.fighold = 1;
scanGateTest.disp(1).loop = 1;
scanGateTest.disp(1).channel = 1;
scanGateTest.disp(1).dim = 1;

scanGateTest.disp(2).loop = 2;
scanGateTest.disp(2).channel = 1;
scanGateTest.disp(2).dim = 2;

disp('-------------------------------------------------');
disp('Is the top gate connected to Auxiliary Output Port 1? Are the barrier gates connected to Auxiliary Output Port 2 and 3? Is the plunger gate connected to Auxiliary Output Port 4?');
prompt0='>y/n> ';
check =  input(prompt0,'s');

if (check == 'y')
    %--------------------Loop run----------------------------------
    disp('------------Bias voltage is ramped------------');
    smset('amp',VBias,0.1);
    disp('------------Topgate voltage is ramped------------');
    smset('AUX1',VTop,0.1);
    disp('------------Start values are applied------------');
    smset('AUX2',VBarrier1_2,0.1);
    smset('AUX3',VBarrier2_2,0.1);
    smset('AUX4',VPlunger_2,0.1);
    disp('------------Sweep start------------');
    smrun(scanGateTest,[rootfolder sample '\' datestr(now,formatOut) '_2DPinchoff_Barrier1Sweep_Range-' num2str(VBarrier1_1) 'Vto' num2str(VBarrier1_2) 'V' '_Barrier2Sweep_Range-'  num2str(VBarrier2_1) 'Vto' num2str(VBarrier2_2) 'V_PlungerSweep_Range-'  num2str(VPlunger_1) 'Vto' num2str(VPlunger_2) 'V_BiasVoltage-' num2str(VBias) 'V_TopgateVoltage-' num2str(VTop) '.mat']);

    disp('------------Sweep end------------');
else
    disp('Check connected cables and rerun the block.')
end
smset('AUXMAX',1.5);
smset('AUXMIN',-0.5);


%% 1D Coulomb blockade sweep (transport current versus barrier 1,2 and plunger)
smset('AUXMAX',1.5);
smset('AUXMIN',-0.5);
disp('-------------------------------------------------');
disp('What bias voltage do you want to apply (V)?');
prompt0='>V_Bias> ';
VBias =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What topgate voltages do you want to apply (in [0V,1.5V])?');
prompt0='>V_Topgate> ';
VTop =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What barrier 1 voltage range do you want to apply (in [0V,1.5V])? -> Diagonal linecut');
prompt0='>Min V_B1> ';
VB1_1 =  str2num(input(prompt0,'s'));
prompt0='>Max V_B1> ';
VB1_2 =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What barrier 2 voltage range do you want to apply (in [0V,1.5V])? -> Diagonal linecut');
prompt0='>Min V_B2> ';
VB2_1 =  str2num(input(prompt0,'s'));
prompt0='>Max V_B2> ';
VB2_2 =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What plunger voltage range do you want to apply (in [-0.5V,1.5V])? -> Diagonal linecut');
prompt0='>Min V_P> ';
VP_1 =  str2num(input(prompt0,'s'));
prompt0='>Max V_P> ';
VP_2 =  str2num(input(prompt0,'s'));
while (VB1_2 > 1.5 | VB1_1 < 0)
    display('Input out of voltage range! Please enter a value between 0 and 1.5V.')
    disp('What barrier 1 voltage range do you want to apply (in [0V,1.5V])? -> Diagonal linecut');
    prompt0='>Min V_B1> ';
    VB1_1 =  str2num(input(prompt0,'s'));
    prompt0='>Max V_B1> ';
    VB1_2 =  str2num(input(prompt0,'s'));
end
while (VB2_2 > 1.5 | VB2_1 < 0)
    display('Input out of voltage range! Please enter a value between 0 and 1.5V.')
    disp('What barrier 2 voltage range do you want to apply (in [0V,1.5V])? -> Diagonal linecut');
    prompt0='>Min V_B2> ';
    VB2_1 =  str2num(input(prompt0,'s'));
    prompt0='>Max V_B2> ';
    VB2_2 =  str2num(input(prompt0,'s'));
end
while (VP_2 > 1.5 | VP_1 < -0.5)
    display('Input out of voltage range! Please enter a value between -0.5 and 1.5V.')
    disp('What plunger voltage range do you want to apply (in [-0.5V,1.5V])? -> Diagonal linecut');
    prompt0='>Min V_P> ';
    VP_1 =  str2num(input(prompt0,'s'));
    prompt0='>Max V_P> ';
    VP_2 =  str2num(input(prompt0,'s'));
end

disp('-------------------------------------------------');
disp('How many data points do you want to record?');
prompt0='>npoints> ';
npoints = str2num(input(prompt0,'s'));

ramptime=0.01;


%--------------------Loop setup----------------------------------
clear scanGateTestinit
m1 = (VB1_2 - VB1_1)/npoints;
b1 = VB1_1;
m2 = (VB2_2 - VB2_1)/npoints;
b2 = VB2_1;
mp = (VP_2 - VP_1)/npoints;
bp = VP_1;
scanGateTestinit.loops(1).npoints = npoints;
scanGateTestinit.loops(1).rng = [VB1_1 VB1_2];
scanGateTestinit.loops(1).setchan = {'AUX2' 'AUX3' 'AUX4'};
scanGateTestinit.loops(1).trafofn(2).fn = @(x,y) (x(1) - b1)/m1*m2+b2;
scanGateTestinit.loops(1).trafofn(2).args = {};
scanGateTestinit.loops(1).trafofn(3).fn = @(x,y) (x(1) - b1)/m1*mp+bp;
scanGateTestinit.loops(1).trafofn(3).args = {};
scanGateTestinit.loops(1).getchan = {'R' 'amp' 'offset' 'Theta' 'freq' 'AUX1' 'AUX2' 'AUX3' 'AUX4'}; %
scanGateTestinit.loops(1).ramptime = ramptime;

%--------------------Figure setup--------------------------------
scanGateTestinit.figure = 17;

%-----Signal-----
scanGateTestinit.disp(1).loop = 1;
scanGateTestinit.disp(1).channel = 1;
scanGateTestinit.disp(1).dim = 1;
%-----Phase------
scanGateTestinit.disp(2).loop = 1;
scanGateTestinit.disp(2).channel = 4;
scanGateTestinit.disp(2).dim = 1;

disp('-------------------------------------------------');
disp('Is the top gate connected to Auxiliary Output Port 1? Are the barrier gates connected to Auxiliary Output Port 2 and 3? Is the plunger gate connected to Auxiliary Output Port 4?');
prompt0='>y/n> ';
check =  input(prompt0,'s');

if (check == 'y')
    %--------------------Loop run----------------------------------
    disp('------------Bias voltage is ramped------------');
    smset('amp',VBias,0.1);
    disp('------------Topgate voltages is ramped------------');
    smset('AUX1',VTop,0.1);
    disp('------------Start values are applied------------');
    smset('AUX2',VB1_1,0.1);
    smset('AUX3',VB2_1,0.1);
    smset('AUX4',VP_1,0.1);
    disp('------------Sweep start------------');
    smrun(scanGateTestinit,[rootfolder sample '\' datestr(now,formatOut) '_Barrier1Sweep_Range-'  num2str(VB1_1) 'Vto' num2str(VB1_2) '_Barrier2Sweep_Range-'  num2str(VB2_1) 'Vto' num2str(VB2_2) '_PlungerSweep_Range-'  num2str(VP_1) 'Vto' num2str(VP_2) 'V_BiasVoltage-' num2str(VBias) 'V_TopgateVoltage-' num2str(VTop) '.mat']);
    disp('------------Sweep end------------');
else
    disp('Check connected cables and rerun the block.')
end


smset('AUXMAX',1.5);
smset('AUXMIN',-0.5);


%% LOOP 1D Coulomb blockade sweep (transport current versus barrier 1,2 and plunger) Saving is fucked up
smset('AUXMAX',1.5);
smset('AUXMIN',-0.5);
disp('-------------------------------------------------');
disp('What bias voltage do you want to apply (V)?');
prompt0='>V_Bias> ';
VBias =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What topgate voltages do you want to apply (in [0V,1.5V])?');
prompt0='>V_Topgate> ';
VTop =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What barrier 1 voltage range do you want to apply (in [0V,1.5V])? -> Diagonal linecut');
prompt0='>Min V_B1> ';
VB1_1 =  str2num(input(prompt0,'s'));
prompt0='>Max V_B1> ';
VB1_2 =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What barrier 2 voltage range do you want to apply (in [0V,1.5V])? -> Diagonal linecut');
prompt0='>Min V_B2> ';
VB2_1 =  str2num(input(prompt0,'s'));
prompt0='>Max V_B2> ';
VB2_2 =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What plunger voltage range do you want to apply (in [-0.5V,1.5V])? -> Diagonal linecut');
prompt0='>Min V_P> ';
VP_1 =  str2num(input(prompt0,'s'));
prompt0='>Max V_P> ';
VP_2 =  str2num(input(prompt0,'s'));
while (VB1_2 > 1.5 | VB1_1 < 0)
    display('Input out of voltage range! Please enter a value between 0 and 1.5V.')
    disp('What barrier 1 voltage range do you want to apply (in [0V,1.5V])? -> Diagonal linecut');
    prompt0='>Min V_B1> ';
    VB1_1 =  str2num(input(prompt0,'s'));
    prompt0='>Max V_B1> ';
    VB1_2 =  str2num(input(prompt0,'s'));
end
while (VB2_2 > 1.5 | VB2_1 < 0)
    display('Input out of voltage range! Please enter a value between 0 and 1.5V.')
    disp('What barrier 2 voltage range do you want to apply (in [0V,1.5V])? -> Diagonal linecut');
    prompt0='>Min V_B2> ';
    VB2_1 =  str2num(input(prompt0,'s'));
    prompt0='>Max V_B2> ';
    VB2_2 =  str2num(input(prompt0,'s'));
end
while (VP_2 > 1.5 | VP_1 < -0.5)
    display('Input out of voltage range! Please enter a value between -0.5 and 1.5V.')
    disp('What plunger voltage range do you want to apply (in [-0.5V,1.5V])? -> Diagonal linecut');
    prompt0='>Min V_P> ';
    VP_1 =  str2num(input(prompt0,'s'));
    prompt0='>Max V_P> ';
    VP_2 =  str2num(input(prompt0,'s'));
end

disp('-------------------------------------------------');
disp('How many data points do you want to record?');
prompt0='>npoints> ';
npoints =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('How many loops do you want?');
prompt0='>Loops> ';
n_loops =  str2num(input(prompt0,'s'));

ramptime=0.01;

for nn=1:n_loops
    %--------------------Loop setup----------------------------------
    clear scanGateTestinit
    m1 = (VB1_2 - VB1_1)/npoints;
    b1 = VB1_1;
    m2 = (VB2_2 - VB2_1)/npoints;
    b2 = VB2_1;
    mp = (VP_2 - VP_1)/npoints;
    bp = VP_1;
    scanGateTestinit.loops(1).npoints = npoints;
    scanGateTestinit.loops(1).rng = [VB1_1 VB1_2];
    scanGateTestinit.loops(1).setchan = {'AUX2' 'AUX3', 'AUX4'};
    scanGateTestinit.loops(1).trafofn(2).fn = @(x,y) (x(1) - b1)/m1*m2+b2;
    scanGateTestinit.loops(1).trafofn(2).args = {};
    scanGateTestinit.loops(1).trafofn(3).fn = @(x,y) (x(1) - b1)/m1*mp+bp;
    scanGateTestinit.loops(1).trafofn(3).args = {};
    scanGateTestinit.loops(1).getchan = {'R' 'amp' 'offset' 'Theta' 'freq' 'AUX1' 'AUX2' 'AUX3' 'AUX4'}; %
    scanGateTestinit.loops(1).ramptime = ramptime;

    %--------------------Figure setup--------------------------------
    scanGateTestinit.figure = 17;

    %-----Signal-----
    scanGateTestinit.disp(1).loop = 1;
    scanGateTestinit.disp(1).channel = 1;
    scanGateTestinit.disp(1).dim = 1;
    %-----Phase------
    scanGateTestinit.disp(2).loop = 1;
    scanGateTestinit.disp(2).channel = 4;
    scanGateTestinit.disp(2).dim = 1;

    %--------------------Loop run----------------------------------
    disp('------------Bias voltage is ramped------------');
    smset('amp',VBias,0.1);
    disp('------------Topgate voltages is ramped------------');
    smset('AUX1',VTop,0.1);
    disp('------------Start values are applied------------');
    smset('AUX2',VB1_1,0.1);
    smset('AUX3',VB2_1,0.1);
    smset('AUX4',VP_1,0.1);
    disp('------------Sweep start------------');
    smrun(scanGateTestinit,[rootfolder sample '\' datestr(now,formatOut) '_Barrier1Sweep_Range-'  num2str(VB1_1) 'Vto' num2str(VB1_2) '_Barrier2Sweep_Range-'  num2str(VB2_1) 'Vto' num2str(VB2_2) '_PlungerSweep_Range-'  num2str(VP_1) 'Vto' num2str(VP_2) 'V_BiasVoltage-' num2str(VBias) 'V_TopgateVoltage-' num2str(VTop) '.mat']);
    disp('------------Sweep end------------');

end

smset('AUXMAX',1.5);
smset('AUXMIN',-0.5);


%% 1D Coulomb blockade sweep (transport current versus plunger)
smset('AUXMAX',2.0);
smset('AUXMIN',-0.5);
disp('-------------------------------------------------');
disp('What bias voltage do you want to apply (V)?');
prompt0='>V_Bias> ';
VBias =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What topgate voltages do you want to apply (in [0V,1.5V])?');
prompt0='>V_Topgate> ';
VTop =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What barrier 1 voltage do you want to apply (in [0V,1.5V])?');
prompt0='>V_B1> ';
VB1 =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What barrier 2 voltage do you want to apply (in [0V,1.5V])?');
prompt0='>V_B2> ';
VB2 =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What plunger voltage range do you want to apply (in [-0.5V,1.5V])? -> Diagonal linecut');
prompt0='>Min V_P> ';
VP_1 =  str2num(input(prompt0,'s'));
prompt0='>Max V_P> ';
VP_2 =  str2num(input(prompt0,'s'));
while (VP_2 > 1.5 | VP_1 < -0.5)
    display('Input out of voltage range! Please enter a value between -0.5 and 1.5V.')
    disp('What plunger voltage range do you want to apply (in [-0.5V,1.5V])? -> Diagonal linecut');
    prompt0='>Min V_P> ';
    VP_1 =  str2num(input(prompt0,'s'));
    prompt0='>Max V_P> ';
    VP_2 =  str2num(input(prompt0,'s'));
end

disp('-------------------------------------------------');
disp('How many data points do you want to record?');
prompt0='>npoints> ';
npoints = str2num(input(prompt0,'s'));

ramptime=0.01;


%--------------------Loop setup----------------------------------
clear scanGateTestinit
scanGateTestinit.loops(1).npoints = npoints;
scanGateTestinit.loops(1).rng = [VP_1 VP_2];
scanGateTestinit.loops(1).setchan = {'AUX4'};
scanGateTestinit.loops(1).getchan = {'R' 'amp' 'offset' 'Theta' 'freq' 'AUX1' 'AUX2' 'AUX3' 'AUX4'}; %
scanGateTestinit.loops(1).ramptime = ramptime;

%--------------------Figure setup--------------------------------
scanGateTestinit.figure = 17;

%-----Signal-----
scanGateTestinit.disp(1).loop = 1;
scanGateTestinit.disp(1).channel = 1;
scanGateTestinit.disp(1).dim = 1;
%-----Phase------
scanGateTestinit.disp(2).loop = 1;
scanGateTestinit.disp(2).channel = 4;
scanGateTestinit.disp(2).dim = 1;

disp('-------------------------------------------------');
disp('Is the top gate connected to Auxiliary Output Port 1? Are the barrier gates connected to Auxiliary Output Port 2 and 3? Is the plunger gate connected to Auxiliary Output Port 4?');
prompt0='>y/n> ';
check =  input(prompt0,'s');

if (check == 'y')
    %--------------------Loop run----------------------------------
    disp('------------Bias voltage is ramped------------');
    smset('amp',VBias,0.1);
    disp('------------Topgate an Barrier voltages is ramped------------');
    smset('AUX1',VTop,0.1);
    smset('AUX2',VB1,0.1);
    smset('AUX3',VB2,0.1);
    disp('------------Start values are applied------------');
    smset('AUX4',VP_1,0.1);
    disp('------------Sweep start------------');
    smrun(scanGateTestinit,[rootfolder sample '\' datestr(now,formatOut) '_Barrier1-'  num2str(VB1) 'V_Barrier2' num2str(VB2) 'V_PlungerSweep_Range-'  num2str(VP_1) 'Vto' num2str(VP_2) 'V_BiasVoltage-' num2str(VBias) 'V_TopgateVoltage-' num2str(VTop) '.mat']);
    disp('------------Sweep end------------');
else
    disp('Check connected cables and rerun the block.')
end


smset('AUXMAX',2.0);
smset('AUXMIN',-0.5);


%% 2D Coulomb blockade diamond sweep (barrier 1/barrier 2 vs bias voltage)
smset('AUXMAX',1.5);
smset('AUXMIN',-0.5);
disp('-------------------------------------------------');
disp('What topgate voltage do you want to apply (in [0V,1.5V])?');
prompt0='>V_Topgate> ';
VTop =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What plunger voltages do you want to apply (in [-0.5V,1.5V])?');
prompt0='>V_Plunger> ';
VPlunger =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What barrier 1 voltage range do you want to apply (in [0V,1.5V])? -> Diagonal linecut');
prompt0='>Min V_B1> ';
VB1_1 =  str2num(input(prompt0,'s'));
prompt0='>Max V_B1> ';
VB1_2 =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What barrier 2 voltage range do you want to apply (in [0V,1.5V])? -> Diagonal linecut');
prompt0='>Min V_B2> ';
VB2_1 =  str2num(input(prompt0,'s'));
prompt0='>Max V_B2> ';
VB2_2 =  str2num(input(prompt0,'s'));
while (VB1_2 > 1.5 | VB1_1 < 0)
    display('Input out of voltage range! Please enter a value between 0 and 1.5V.')
    disp('What barrier 1 voltage range do you want to apply (in [0V,1.5V])? -> Diagonal linecut');
    prompt0='>Min V_B1> ';
    VB1_1 =  str2num(input(prompt0,'s'));
    prompt0='>Max V_B1> ';
    VB1_2 =  str2num(input(prompt0,'s'));
end
while (VB2_2 > 1.5 | VB2_1 < 0)
    display('Input out of voltage range! Please enter a value between 0 and 1.5V.')
    disp('What barrier 2 voltage range do you want to apply (in [0V,1.5V])? -> Diagonal linecut');
    prompt0='>Min V_B2> ';
    VB2_1 =  str2num(input(prompt0,'s'));
    prompt0='>Max V_B2> ';
    VB2_2 =  str2num(input(prompt0,'s'));
end
disp('-------------------------------------------------');
disp('What bias voltage range do you want to apply (V pre voltage divider)?');
prompt0='>Min V_Bias> ';
VBias1 =  str2num(input(prompt0,'s'));
prompt0='>Max V_Bias> ';
VBias2 =  str2num(input(prompt0,'s'));


disp('-------------------------------------------------');
disp('How many data points do you want to record?');
prompt0='>X npoints> ';
npoints_X =  str2num(input(prompt0,'s'));
prompt0='>Y npoints> ';
npoints_Y =  str2num(input(prompt0,'s'));

ramptime=0.01;


%--------------------Loop setup----------------------------------
clear scanGateTest
m1 = (VB1_2 - VB1_1)/npoints_X;
b1 = VB1_1;
m2 = (VB2_2 - VB2_1)/npoints_X;
b2 = VB2_1;
scanGateTest.loops(1).npoints = npoints_X;
scanGateTest.loops(1).rng = [VB1_1 VB1_2];
scanGateTest.loops(1).setchan = {'AUX2' 'AUX3'};
scanGateTest.loops(1).trafofn(2).fn = @(x,y) (x(1) - b1)/m1*m2+b2;
scanGateTest.loops(1).trafofn(2).args = {};
scanGateTest.loops(1).getchan = {'R' 'amp' 'offset' 'Theta' 'freq' 'AUX1' 'AUX2' 'AUX3' 'AUX4'}; %
scanGateTest.loops(1).ramptime = ramptime;

scanGateTest.loops(2).npoints = npoints_Y;
scanGateTest.loops(2).rng = [VBias1 VBias2];
scanGateTest.loops(2).setchan = {'offset'}; % Bias
scanGateTest.loops(2).getchan = {'R' 'amp' 'offset' 'Theta' 'freq' 'AUX1' 'AUX2' 'AUX3' 'AUX4'}; %
scanGateTest.loops(2).ramptime = ramptime;
%--------------------Figure setup--------------------------------
scanGateTest.figure = 42;
scanGateTest.fighold = 1;
scanGateTest.disp(1).loop = 1;
scanGateTest.disp(1).channel = 1;
scanGateTest.disp(1).dim = 1;

scanGateTest.disp(2).loop = 2;
scanGateTest.disp(2).channel = 1;
scanGateTest.disp(2).dim = 2;

disp('-------------------------------------------------');
disp('Is the top gate connected to Auxiliary Output Port 1? Are the barrier gates connected to Auxiliary Output Port 2 and 3? Is the plunger gate connected to Auxiliary Output Port 4?');
prompt0='>y/n> ';
check =  input(prompt0,'s');

if (check == 'y')
    %--------------------Loop run----------------------------------   
    %disp('------------Amplitude is ramped to 0.1V pre voltage divider------------');
    %smset('amp',0.1,0.1);
    disp('------------Frequency is set to zero------------');
    save = smget('freq');
    save = save{1}
    smset('freq',0,5);
    disp('------------Barrier voltages are ramped------------');
    smset('AUX1',VTop,0.1);
    disp('------------Plunger voltage is ramped------------');
    smset('AUX4',VPlunger,0.1);
    disp('------------Start values are applied------------');
    smset('AUX2',VB1_1,0.1);
    smset('AUX3',VB2_1,0.1);
    smset('amp',0,0.1);
    smset('offset',VBias1,0.1);
    disp('------------Sweep start------------');
    smrun(scanGateTest,[rootfolder sample '\' datestr(now,formatOut) '_2DCoulombDiamonds_Barrier1Sweep_Range-' num2str(VB1_1) 'Vto' num2str(VB1_2) 'V' '_Barrier2Sweep_Range-'  num2str(VB2_1) 'Vto' num2str(VB2_2) 'V_BiasVoltage-' num2str(VBias1) 'Vto' num2str(VBias2) 'V_TopgateVoltage-' num2str(VTop) 'V_PlungerVoltage-' num2str(VPlunger) '.mat'])
    smset('freq',save,5);
    smset('offset',0,0.1);
    disp('------------Sweep end------------');
else
    disp('Check connected cables and rerun the block.')
end
smset('AUXMAX',1.5);
smset('AUXMIN',-0.5);


%% 2D Coulomb blockade diamond sweep (plunger vs bias voltage)
smset('AUXMAX',2);
smset('AUXMIN',-0.5);
disp('-------------------------------------------------');
disp('What topgate voltage do you want to apply (in [0V,1.5V])?');
prompt0='>V_Topgate> ';
VTop =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What barrier 1 voltage do you want to apply (in [0V,1.5V])?');
prompt0='>V_Barrier> ';
VB1 =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What barrier 2 voltage do you want to apply (in [0V,1.5V])?');
prompt0='>V_Barrier> ';
VB2 =  str2num(input(prompt0,'s'));
disp('-------------------------------------------------');
disp('What plunger voltage range do you want to apply (in [-0.5V,1.5V])?');
prompt0='>Min V_Plunger> ';
VPlunger_1 =  str2num(input(prompt0,'s'));
prompt0='>Max V_Plunger> ';
VPlunger_2 =  str2num(input(prompt0,'s'));
while (VPlunger_2 > 1.5 | VPlunger_1 < -0.5)
    display('Input out of voltage range! Please enter a value between -0.5 and 1.5V.')
    disp('What plunger voltage range do you want to apply (in [-0.5V,1.5V])?');
    prompt0='>Min V_Plunger> ';
    VPlunger_1 =  str2num(input(prompt0,'s'));
    prompt0='>Max V_Plunger> ';
    VPlunger_2 =  str2num(input(prompt0,'s'));
end
disp('-------------------------------------------------');
disp('What bias voltage range do you want to apply (V pre voltage divider)?');
prompt0='>Min V_Bias> ';
VBias1 =  str2num(input(prompt0,'s'));
prompt0='>Max V_Bias> ';
VBias2 =  str2num(input(prompt0,'s'));


disp('-------------------------------------------------');
disp('How many data points do you want to record?');
prompt0='>X npoints> ';
npoints_X =  str2num(input(prompt0,'s'));
prompt0='>Y npoints> ';
npoints_Y =  str2num(input(prompt0,'s'));

ramptime=0.01;


%--------------------Loop setup----------------------------------
clear scanGateTest
scanGateTest.loops(1).npoints = npoints_X;
scanGateTest.loops(1).rng = [VPlunger_1 VPlunger_2];
scanGateTest.loops(1).setchan = {'AUX4'};
scanGateTest.loops(1).getchan = {'R' 'amp' 'offset' 'Theta' 'freq' 'AUX1' 'AUX2' 'AUX3' 'AUX4'}; %
scanGateTest.loops(1).ramptime = ramptime;

scanGateTest.loops(2).npoints = npoints_Y;
scanGateTest.loops(2).rng = [VBias1 VBias2];
scanGateTest.loops(2).setchan = {'offset'}; % Bias
scanGateTest.loops(2).getchan = {'R' 'amp' 'offset' 'Theta' 'freq' 'AUX1' 'AUX2' 'AUX3' 'AUX4'}; %
scanGateTest.loops(2).ramptime = ramptime;
%--------------------Figure setup--------------------------------
scanGateTest.figure = 42;
scanGateTest.fighold = 1;
scanGateTest.disp(1).loop = 1;
scanGateTest.disp(1).channel = 1;
scanGateTest.disp(1).dim = 1;

scanGateTest.disp(2).loop = 2;
scanGateTest.disp(2).channel = 1;
scanGateTest.disp(2).dim = 2;

disp('-------------------------------------------------');
disp('Is the top gate connected to Auxiliary Output Port 1? Are the barrier gates connected to Auxiliary Output Port 2 and 3? Is the plunger gate connected to Auxiliary Output Port 4?');
prompt0='>y/n> ';
check =  input(prompt0,'s');

if (check == 'y')
    %--------------------Loop run----------------------------------   
    %disp('------------Amplitude is ramped to 0.1V pre voltage divider------------');
    %smset('amp',0.1,0.1);
    disp('------------Frequency is set to zero------------');
    save = smget('freq');
    save = save{1}
    smset('freq',0,5);
    disp('------------Topgate voltages are ramped------------');
    smset('AUX1',VTop,0.1);
    disp('------------Barrier voltage is ramped------------');
    smset('AUX2',VB1,0.1);
    smset('AUX3',VB2,0.1);
    disp('------------Start values are applied------------');
    smset('AUX4',VPlunger_1,0.1);
    smset('amp',0,0.1);
    smset('offset',VBias1,0.1);
    disp('------------Sweep start------------');
    smrun(scanGateTest,[rootfolder sample '\' datestr(now,formatOut) '_2DCoulombDiamonds_PlungerSweep_Range-' num2str(VPlunger_1) 'Vto' num2str(VPlunger_2) 'V_BiasVoltage-' num2str(VBias1) 'Vto' num2str(VBias2) 'V_TopgateVoltage-' num2str(VTop) 'V_Barrier1Voltage-' num2str(VB1) 'V_Barrier2Voltage-' num2str(VB2) '.mat'])
    smset('freq',save,5);
    smset('offset',0,0.1);
    disp('------------Sweep end------------');
else
    disp('Check connected cables and rerun the block.')
end
smset('AUXMAX',2);
smset('AUXMIN',-0.5);


%% Ramp down to zero
smset({'AUX1','AUX2','AUX3','AUX4','amp','offset'},0,0.1);

