%% Setup Script for Bertha Setup 

% Completely resets special-measure and all instruments!
% 
% navigate to special-measure/instruments/
% in order for the instruments to load correctly or add this folder to your
% path. Moving to a different directory allows for loading custom
% instrument configurations (sminst_* files)


% Load empty smdata
if util.yes_no_input('Reset special-measure and all instruments?', 5, 'yes')
	instrreset
	clearvars -global smdata
	load C:\Users\inst2c\Documents\Matlab\special-measure\instruments\smdata_empty	
	global smdata	
	fprintf('Reset complete\n');
    sminitdisp 
    
else
	fprintf('Reset aborted\n');
    
end


%% Setup Script for 4K Setup 
% Completely resets special-measure and all instruments!
% 
% navigate to special-measure/instruments/
% in order for the instruments to load correctly or add this folder to your
% path. Moving to a different directory allows for loading custom
% instrument configurations (sminst_* files)

% Load empty smdata
if util.yes_no_input('Reset special-measure and all instruments?', 5, 'yes')
	instrreset
	clearvars -global smdata
	load C:\Users\lab2\Documents\Matlab\special-measure\instruments\smdata_empty	
	global smdata	
    
else
	fprintf('Reset aborted\n');
    
end

%% Setup Script for Selene (Paul)
% Completely resets special-measure and all instruments!
% 
% navigate to special-measure/instruments/
% in order for the instruments to load correctly or add this folder to your
% path. Moving to a different directory allows for loading custom
% instrument configurations (sminst_* files)

% Load empty smdata
if util.yes_no_input('Reset special-measure and all instruments?', 5, 'yes')
	instrreset
	clearvars -global smdata
	load C:\Users\lablocal\Documents\Matlab\special-measure\instruments\smdata_empty	
	global smdata	
    
else
	fprintf('Reset aborted\n');
    
end

run('C:\Users\lablocal\Documents\MATLAB\ziDriver\ziAddPath.m');


%% Setup Script for Selene (Thomas)
% Completely resets special-measure and all instruments!
% 
% navigate to special-measure/instruments/
% in order for the instruments to load correctly or add this folder to your
% path. Moving to a different directory allows for loading custom
% instrument configurations (sminst_* files)

% Load empty smdata
if util.yes_no_input('Reset special-measure and all instruments?', 5, 'yes')
	instrreset
	clearvars -global smdata
	load C:\Users\lab2\Documents\Matlab\special-measure\instruments\smdata_empty	
	global smdata	
    
else
	fprintf('Reset aborted\n');
    
end

run('C:\Users\lab2\Documents\MATLAB\ziDriver\ziAddPath.m');
 

%% DecaDAC
% Load DecaDAC
% 
% --- Old DecaDAC3 (Silver, 16 channels) ------------------------------------
% - Black optical cable = TX
% - Red optical cable = RX
% - Baud rate: 57600 (new DecaDACS 9600)
% - Troubleshot old DecaDAC: Try inverting optical connections (Black = RX)
% 
% After inserting USB make sure drivers are installed. If drivers fail to
% install go to "http://www.ftdichip.com/Drivers/VCP.htm" and download the
% setup executable for Windows (the first one, executable file on the right
% of web page)% When drivers are installed go to device manager to see
% which COM port the device has an insert the correct port below.
% -------------------------------------------------------------------------
% 
% --- Set DECADAC parameters  ---------------------------------------------
% decaDACRange     Range of the physical switch on the DAC. This MUST match in
%                  order to avoid unpredictable behaviour!
% decaDACLimit     sm internal channel limit. Set this to add an additinal layer
%                  of protection. Can be equal to DACrange
% decaDACRampRate  This sets the sm internal maximum ramp rate for the DAC!
%                  (default=0.07)
% decaDACFactor    Sets the factor in rangeramp for use with voltage dividers.
%                  (default=1)
% nDecaDACChannels Number of DecaDAC channels
% -------------------------------------------------------------------------
decaDACRange = [-10 10]; 
decaDACLimit = [-5 5];
decaDACRampRate = 0.07;
decaDACFactor = 1;
nDecaDACChannels = 16; % 

% Load default instrument and assign correct serial object. CHECK COM PORT!
ii = smloadinst('DecaDAC');
%obj=serial('COM10','BaudRate',57600);
obj=serial('COM10','BaudRate',9600);

fopen(obj);
smdata.inst(ii).data.inst=obj;

% Set the range
smdata.inst(ii).data.rng = repmat(decaDACRange, nDecaDACChannels, 1);

% init all RAMP channels as CH1 - CH16 (This can be changed to match
% channel numbers on the DAC)
chans = 2:2:2*nDecaDACChannels;
for j = 1:nDecaDACChannels
	smaddchannel(ii, chans(j), ['CH',num2str(j)], [decaDACLimit decaDACRampRate decaDACFactor]);
end

% Initialize DAC
smadacinit(ii);

% Clear variables
clearvars chans decaDACFactor decaDACLimit decaDACRampRate decaDACRange ii j nDecaDACChannels obj 

%% DecaDAC: Add
% 
% --- New DecaDAC (Black, 20 channels) ------------------------------------
% - Black optical cable = TX
% - Red optical cable = RX
% - Baud rate: 9600 (old DecaDACs 57600)
% - Troubleshot old DecaDAC: Try inverting optical connections (Black = RX)
%
% After inserting USB make sure drivers are installed. If drivers fail to
% install go to "http://www.ftdichip.com/Drivers/VCP.htm" and download the
% setup executable for Windows (the first one, executable file on the right
% of web page)% When drivers are installed go to device manager to see
% which COM port the device has an insert the correct port below.
% -------------------------------------------------------------------------

% --- Set DecaDAC parameters  ---------------------------------------------
% decaDACRange     Range of the physical switch on the DAC. This MUST match in
%                  order to avoid unpredictable behaviour!
% decaDACLimit     sm internal channel limit. Set this to add an additinal layer
%                  of protection. Can be equal to DACrange
% decaDACRampRate  This sets the sm internal maximum ramp rate for the DAC!
%                  (default=0.07)
% decaDACFactor    Sets the factor in rangeramp for use with voltage dividers.
%                  (default=1)
% nDecaDACChannels Number of DecaDAC channels
% -------------------------------------------------------------------------
decaDACRange = [-10 10]; 
decaDACLimit = [-10 10];
decaDACRampRate = 1; %0.07;
decaDACFactor = 1;
nDecaDACChannels = 20;

% Load default instrument and assign correct serial object
ii = smloadinst('DecaDAC20', [], 'COM11', 'BaudRate', 9600);
smdata.inst(ii).name = 'DecaDAC1';
ii = sminstlookup('DecaDAC1');

% Set the range
smdata.inst(ii).data.rng = repmat(decaDACRange, nDecaDACChannels, 1);

% Initialize all RAMP channels as CH1 - CH16
chans = 2:2:2*nDecaDACChannels;
for j = 1:nDecaDACChannels
	smaddchannel(ii, chans(j), ['CH',num2str(j)], [decaDACLimit decaDACRampRate decaDACFactor]);
end

% Initialize and open DAC
smopen(ii);
smadacinit(ii);

% Clear variables
clearvars chans decaDACFactor decaDACLimit decaDACRampRate decaDACRange ii j nDecaDACChannels

%% Lock In SR830 1st
% Load Lock In SR830 _ FIRST 
%
% --- Stanford Research 830 Lock In Amp -----------------------------------
% 
% Please Set the desired GPIB port on the device. Default at the moment is
% 12 on the Bertha Rack
% -------------------------------------------------------------------------

% intrument GPIB address
GPIBport=12;

% Load default instrument and assign correct VISA-GPIB object
ii = smloadinst('SR830');
vg = visa('agilent',['GPIB0::' num2str(GPIBport) '::INSTR']);
fopen(vg);
smdata.inst(ii).data.inst=vg;

% set name
smdata.inst(ii).name='LockIn1';

% 1: X, 2: Y, 3: R, 4: Theta, 5: freq, 6: ref amplitude
% 7:10: AUX input 1-4, 11:14: Aux output 1:4
% 15,16: stored data, length determined by datadim
% 17: sensitivity
% 18: time constant
% 19: sync filter on/off
smaddchannel(ii,3,'LIC');
smaddchannel(ii,4,'LIAtheta');
smaddchannel(ii,5,'LIAfreq');
smaddchannel(ii,6,'LIAamp');
smaddchannel(ii,17,'LIAsens');
smaddchannel(ii,18,'LIAtc');
smaddchannel(ii,15,'LIAbuf1');
% smaddchannel(ii,16,'LIAbuf2');

clearvars ii GPIBport vg

%% Lock In SR830 2nd 
% Load Lock In SR830 _ SECOND
%
% --- Stanford Research 830 Lock In Amp -----------------------------------
% 
% Please Set the desired GPIB port on the device. Default at the moment is
% 12 on the Bertha Rack.
% -------------------------------------------------------------------------

% intrument GPIB address
GPIBport=18;

% Load default instrument and assign correct VISA-GPIB object
ii = smloadinst('SR830');
vg = visa('agilent',['GPIB0::' num2str(GPIBport) '::INSTR']);
fopen(vg);
smdata.inst(ii).data.inst=vg;

% set name
smdata.inst(ii).name='LockIn2';

% 1: X, 2: Y, 3: R, 4: Theta, 5: freq, 6: ref amplitude
% 7:10: AUX input 1-4, 11:14: Aux output 1:4
% 15,16: stored data, length determined by datadim
% 17: sensitivity
% 18: time constant
% 19: sync filter on/off
smaddchannel(ii,3,'LIC2');
smaddchannel(ii,4,'LIAtheta2');
smaddchannel(ii,5,'LIAfreq2');
smaddchannel(ii,6,'LIAamp2');
smaddchannel(ii,17,'LIAsens2');
smaddchannel(ii,18,'LIAtc2');
smaddchannel(ii,15,'LIAbuf12');
% smaddchannel(ii,16,'LIAbuf2');

clearvars ii GPIBport vg

%% Keithley2400 GPIB 

% %%
% % Load Keithley2400
% %
% % --- Keithley 2400 Source Meter Unit -------------------------------------
% %
% % Please Set the desired GPIB port on the device. Default at the moment is
% % 24 on the Bertha Rack.
% % -------------------------------------------------------------------------
% 
% intrument GPIB address

GPIBport=26;


% Load default instrument and assign correct VISA-GPIB object
ii = smloadinst('Keithley2400');
vg2 = visa('agilent',['GPIB0::' num2str(GPIBport) '::INSTR']);
fopen(vg2);
smdata.inst(ii).data.inst=vg2;

% set name
smdata.inst(ii).name='Keithley_up';

% 1 =VOLT, 2 =CURRENT, 3 =COMPLIANCE, 4 =ISSOURCEVOLT, 5 =OUTPUTON
smaddchannel(ii,1,'SMUV');
smaddchannel(ii,2,'SMUI');
smaddchannel(ii,3,'SMUcomp');
smaddchannel(ii,4,'SMUisv');
smaddchannel(ii,5,'SMUoutputon');

clearvars ii GPIBport vg2

%% Keithley2400 USB

% %%
% % Load Keithley2400
% %
% % --- Keithley 2400 Source Meter Unit -------------------------------------
% %
% % Please Set the desired GPIB port on the device. Default at the moment is
% % 24 on the Bertha Rack.
% % -------------------------------------------------------------------------
% 
% intrument GPIB address
GPIBport=25;


% Load default instrument and assign correct VISA-GPIB object
ii = smloadinst('Keithley2400');
% vg2 = visa('agilent',['GPIB0::' num2str(GPIBport) '::INSTR']);
vg2 = visa('agilent',['USB0::0x05E6::0x2450::04400596::INSTR']);
fopen(vg2);
smdata.inst(ii).data.inst=vg2;

% set name
smdata.inst(ii).name='SMU1';

% 1 =VOLT, 2 =CURRENT, 3 =COMPLIANCE, 4 =ISSOURCEVOLT, 5 =OUTPUTON
smaddchannel(ii,1,'SMUV2');
smaddchannel(ii,2,'SMUI2');
smaddchannel(ii,3,'SMUcomp2');
smaddchannel(ii,4,'SMUisv2');
smaddchannel(ii,5,'SMUoutputon2');

clearvars ii GPIBport vg2

%% Keithley2450 GPIB

% %%
% % Load Keithley2450
% %
% % --- Keithley 2450 Source Meter Unit -------------------------------------
% %
% % Please Set the desired GPIB port on the device. Default at the moment is
% % 24 on the Bertha Rack.
% % -------------------------------------------------------------------------
% 
% intrument GPIB address
GPIBport=22;


% Load default instrument and assign correct VISA-GPIB object
ii = smloadinst('Keithley2450');
% vg2 = visa('agilent',['GPIB0::' num2str(GPIBport) '::INSTR']);
vg2 = visa('agilent',['GPIB0::' num2str(GPIBport) '::INSTR']);
fopen(vg2);
smdata.inst(ii).data.inst=vg2;

% set name
smdata.inst(ii).name='Keitley2450_up';

% 1 =VOLT, 2 =CURRENT, 3 =COMPLIANCE, 4 =ISSOURCEVOLT, 5 =OUTPUTON, 6
smaddchannel(ii,1,'SMUV');
smaddchannel(ii,2,'SMUI');
smaddchannel(ii,3,'SMUcomp');
smaddchannel(ii,4,'SMUisv');
smaddchannel(ii,5,'SMUoutputon');
smaddchannel(ii,6,'SMU4ptmeas');

clearvars ii GPIBport vg2

%% SMS120C Magnet Power Supply
% Load SMS120C
%
% --- SMS120C Magnet Power Supply -----------------------------------------
% - Baud rate: 9600 
%
% Setup for SMS120C Magnet Power Supply. Diver version 4 for use without a
% switch heater! Please make sure to check the COM port before usage! 
% -------------------------------------------------------------------------

% Load default instrument and assign correct serial object. CHECK COM PORT!
ii = smloadinst('SMS120C');
smdata.inst(ii).cntrlfn=@smcSMS120Cv4;
mg=serial('COM3','BaudRate',9600);
fopen(mg);
smdata.inst(ii).data.inst=mg;

% 1: magnetic field
smaddchannel(ii,1,'Bz');
clearvars ii mg

%% Zurich Instruments MFLI Lock-In Amplifier: Add
% execute ziAddPath.m
% can be found under Matlab/ziDrivers


% driver under C:\Users\lab2\Documents\MATLAB\special-measure\sm\channels

%
% --- Zurich Instruments MFLI Lock-In Amplifier ---------------------------
% - device_name = dev3338
%
% The device initialization relies on the device with matching name being
% connected to the local network. Operation via a direct USB connection
% might need for a change in the driver!
% -------------------------------------------------------------------------

ii = smloadinst('MFLI');
smdata.inst.cntrlfn = @smcMFLI_VMN_AUXOUTS;
smdata.inst.datadim(13:18,1) = 0;
smdata.inst.type(13:18,1) = 0;
smdata.inst.channels(13,1:13) = 'AUX1         ';
smdata.inst.channels(14,1:13) = 'AUX2         ';
smdata.inst.channels(15,1:13) = 'AUX3         ';
smdata.inst.channels(16,1:13) = 'AUX4         ';
smdata.inst.channels(17,1:13) = 'AUXMAX       ';
smdata.inst.channels(18,1:13) = 'AUXMIN       ';
smdata.inst(ii).name = 'LockIn1';
smdata.inst(ii).data.device_name='dev5247'; %%% adjust name according to Device number
ii = sminstlookup('LockIn1');

smaddchannel(ii, 1, 'X');
smaddchannel(ii, 2, 'Y');
smaddchannel(ii, 3, 'R');
smaddchannel(ii, 4, 'Theta');
smaddchannel(ii, 5, 'freq');
smaddchannel(ii, 6, 'amp');

smaddchannel(ii, 12, 'offset');
smaddchannel(ii, 13, 'AUX1');
smaddchannel(ii, 14, 'AUX2');
smaddchannel(ii, 15, 'AUX3');
smaddchannel(ii, 16, 'AUX4');
smaddchannel(ii, 17, 'AUXMAX');
smaddchannel(ii, 18, 'AUXMIN');

% 1: X, 2: Y, 3: R, 4: Theta, 5: freq, 6: ref amplitude
% 7: Buffered Read out R
% 8: buffered Read Out phase
%10: Buffered Read Out x
%11: Time constant
%12: Offset on Vref
%13: AUXOUT0, 14: AUXOUT1, 15: AUXOUT2, 16: AUXOUT3
%17: AUXOUTMAX, 18: AUXOUTMIN,

% Open LockIn
smopen(ii);

%smdata.inst(2).datadim=ones(8,0);

clearvars ii

%% DecaDAC20: Add
% 
% --- New DecaDAC (Black, 20 channels) ------------------------------------
% - Black optical cable = TX
% - Red optical cable = RX
% - Baud rate: 9600 (old DecaDACs 57600)
% - Troubleshot old DecaDAC: Try inverting optical connections (Black = RX)
%
% After inserting USB make sure drivers are installed. If drivers fail to
% install go to "http://www.ftdichip.com/Drivers/VCP.htm" and download the
% setup executable for Windows (the first one, executable file on the right
% of web page). When drivers are installed go to device manager to see
% which COM port the device has an insert the correct port below.
% -------------------------------------------------------------------------

% --- Set DecaDAC parameters  ---------------------------------------------
% decaDACRange     Range of the physical switch on the DAC. This MUST match in
%                  order to avoid unpredictable behaviour!
% decaDACLimit     sm internal channel limit. Set this to add an additinal layer
%                  of protection. Can be equal to DACrange
% decaDACRampRate  This sets the sm internal maximum ramp rate for the DAC!
%                  (default=0.07)
% decaDACFactor    Sets the factor in rangeramp for use with voltage dividers.
%                  (default=1)
% nDecaDACChannels Number of DecaDAC channels
% -------------------------------------------------------------------------
decaDACRange = [-10 10]; 
decaDACLimit = [-5 5];
decaDACRampRate = 0.07; %0.07;
decaDACFactor = 1;
nDecaDACChannels = 20;

% Load default instrument and assign correct serial object
ii = smloadinst('DecaDAC20', [], 'COM4', 'BaudRate', 9600);
smdata.inst(ii).name = 'DecaDAC1';
ii = sminstlookup('DecaDAC1');

% Set the range
smdata.inst(ii).data.rng = repmat(decaDACRange, nDecaDACChannels, 1);

% Initialize all RAMP channels as CH1 - CH16
chans = 2:2:2*nDecaDACChannels;
for j = 1:nDecaDACChannels
	smaddchannel(ii, chans(j), ['CH',num2str(j)], [decaDACLimit decaDACRampRate decaDACFactor]);
end

% Initialize and open DAC
smopen(ii);
smadacinit(ii);

% Clear variables
clearvars chans decaDACFactor decaDACLimit decaDACRampRate decaDACRange ii j nDecaDACChannels

%% Lakeshore335: Temperatur Control

% %%
% % Load LakeShore Temperatur Control
% %
% % --- LakeShore Temperatur Control Unit -------------------------------------
% %
% % Please Set the desired GPIB port on the device. Default at the moment is
% % 12 on the Bertha Rack.
% % -------------------------------------------------------------------------
% 
% intrument GPIB address
GPIBport=17;


% Load default instrument and assign correct VISA-GPIB object
ii = smloadinst('Lakeshore335');
vg2 = visa('agilent',['GPIB0::' num2str(GPIBport) '::INSTR']);
fopen(vg2);
smdata.inst(ii).data.inst=vg2;

% set name
smdata.inst(ii).name='Lakeshore';

smaddchannel(ii, 1, 'Temp');
smaddchannel(ii, 2, 'SetA');
clearvars ii GPIBport vg2

%%
smdata.configch=[1:22]
