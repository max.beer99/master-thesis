"""Interactive measuring script for SET characterization @ 4K, right setup."""

# %% Setup variables:

#  -------------------------------------------------
#  --------- START OF USER DEFINED CONFIG ----------
#  -------------------------------------------------

# Usefull: print(a.replace(', ', ',\n').replace('{', '{\n').replace('}', '\n}'))

# Connections:
dac_connection = 'ASRL6::INSTR'
lockin_up_connection = 'GPIB1::12::INSTR'
lockin_down_connection = 'GPIB1::14::INSTR'
keithley_right_connection = 'GPIB1::27::INSTR'
keithley_left_connection = 'GPIB1::23::INSTR'

# Storage options
database_path = "~/Documents/DATA/SiGe/Beer/default_db/20211101_Lawrance.db"

#  Change this path if you want to load a sample config:
#  THIS CONFIG IS THE ONE GOING TO BE USED IN THE CODE
sample_config_path_load = "./../../../../../DATA/SiGe/Beer/sample_configs/20211101_Lawrance_Batch_3_Chip_14_C3.json"
#  Change this path if you want to update a sample config:
#  THIS CONFIG IS GOING TO BE OVERWRITTEN BY THE CODE BELOW
sample_config_path_save = "./../../../../../DATA/SiGe/Beer/sample_configs/20211101_Lawrance_Batch_3_Chip_14_C3.json"

# Sample Config
sample_config = dict()
#  Sample name
sample_config['sample_name'] = '20211101_Lawrance_Batch_3_Chip_14_C3'
#  Mapping Brakeoutbox : Gates
sample_config['gate_mapping'] = {
    '1': 'TRP',
    '2': 'TGR',
    '3': 'NC',
    '4': 'RB2',
    '5': 'RP',
    '6': 'TRB2',
    '7': 'S4',
    '8': 'RB1',
    '9': 'ORT',
    '10': 'ScrTL',
    '11': 'OLT',
    '12': 'TLB2',
    '13': 'TLP',
    '14': 'TGL',
    '15': 'LB1',
    '16': 'LP',
    '17': 'LB2',
    '18': 'S1',
    '19': 'S3',
    '20': 'OLB',
    '21': 'ORB',
    '22': 'TRB1',
    '23': 'TLB1',
    '24': 'S2'
}
#  Mapping DECADAC channels : Breakoutbox
sample_config['DECADAC_mapping'] = {
    '0': '1',
    '1': '4',
    '2': '5',
    '3': '6',
    '4': '7',
    '5': '8',
    '6': '10',
    '7': '12',
    '8': '13',
    '9': '15',
    '10': '16',
    '11': '17',
    '12': '18',
    '13': '19',
    '14': '22',
    '15': '23',
    '16': '24',
    '17': '',
    '18': '',
    '19': ''
}
#  Mapping Keithley : Breakoutbox
sample_config['Keithley_mapping'] = {
    'keithley_left': '14',
    'keithley_right': '2'
}
#  Mapping Lockin : Breakoutbox
sample_config['Lockin_mapping'] = {
    'lockin_up': {
        'out': '11',
        'in': '20'
        },
    'lockin_down': {
        'out': '9',
        'in': '21'
        }
}
#  Lockin voltage divider scaling
sample_config['Lockin_voltage_scaling'] = 1e-04
#  Lockin amplitude
sample_config['lockin_amplitude'] = {
    'lockin_amplitude_up': 1,
    'lockin_amplitude_down': 1    
}
#  Define gate sets
sample_config['gate_lists'] = {
    'gates_set_left': ['LB1', 'LB2', 'LP'],
    'gates_set_right': ['RB1', 'RB2', 'RP'],
    'gates_claviature': ['S1', 'S2', 'S3', 'S4'],
    'gates_channel': ['TRB1', 'TRB2', 'TRP', 'TLB1', 'TLB2', 'TLP', 'S1', 'S2', 'S3', 'S4'],
    'gates_channel_left': ['TLB1', 'TLB2', 'TLP'],
    'gates_channel_right': ['TRB1', 'TRB2', 'TRP'],
    'gates_ohmics_left': ['OLB', 'OLT'],
    'gates_ohmics_right': ['ORB', 'ORT'],
    'gates_top_left': ['TGL'],
    'gates_top_right': ['TGR'],
}

sample_config['comments'] = '''lockin_up: 1e-04, lockin_down: 1e-04, Measured on 29.07.2022;'''

#  -----------------------------------------------
#  --------- END OF USER DEFINED CONFIG ----------
#  -----------------------------------------------

#  Save and load config
import json
#  Save config on top
#   First read save path to check for differences, then ask for override
_temp_config = {}
try:
    with open(sample_config_path_save) as json_file:
        _temp_config = json.load(json_file)
except:
    pass

_override_flag = True
if _temp_config != sample_config and not _temp_config == {}:
    _override_flag = False
    _input = input('Differences between local sample_config and sample_config '
                   f'at {sample_config_path_save} detected! Override saved '
                   'config with local config? (y/n) ')
    if _input == 'y':
        _override_flag = True

if _override_flag:
    print(f'Saving sample_config to {sample_config_path_save}.')
    with open(sample_config_path_save, "w") as outfile:
        json.dump(sample_config, outfile)
else:
    print('Skipping sample_config saving.')

#   Load sample_config
print(f'Loading sample_config from {sample_config_path_load}.')
with open(sample_config_path_load) as json_file:
    sample_config = json.load(json_file)

# %% Import modules and initialize setup

import time
import os
import numpy as np
from tqdm import tqdm
import qcodes as qc
import pyvisa
import functools

from qcodes.instrument_drivers.Harvard.Decadac import Decadac
from qcodes.instrument_drivers.stanford_research.SR830 import SR830
from qcodes.instrument_drivers.tektronix.Keithley_2450 import Keithley2450
from qcodes.instrument_drivers.tektronix.Keithley_2400 import Keithley_2400
from qcodes.instrument_drivers.tektronix.Keithley_2401 import Keithley_2401
import qcodes.instrument_drivers.Lakeshore.Model_325 as ls  # Temperature sensor

from scipy.signal import savgol_filter
from measurement_routines import *

#  Gate sets for convinience
gates_set_left = sample_config['gate_lists']['gates_set_left']
gates_set_right = sample_config['gate_lists']['gates_set_right']
gates_claviature = sample_config['gate_lists']['gates_claviature']
gates_channel = sample_config['gate_lists']['gates_channel']
gates_channel_left = sample_config['gate_lists']['gates_channel_left']
gates_channel_right = sample_config['gate_lists']['gates_channel_right']
gate_ohmic_left = sample_config['gate_lists']['gates_ohmics_left'][0]
gate_ohmic_right = sample_config['gate_lists']['gates_ohmics_right'][0]
gate_top_left = sample_config['gate_lists']['gates_top_left'][0]
gate_top_right = sample_config['gate_lists']['gates_top_right'][0]


#  Check if station already exists, if so skip initialization
try:
    assert 'station' not in locals().keys()

except AssertionError:
    print('Setup already initialized, skipping initialization')

else:
    # Initialize Instruments
    qc.Instrument.close_all()
    # Clock
    clock = Clock('clock')

    # DECADAC
    dac = Decadac(
                    'dac',
                    dac_connection,
                    min_val=-10,
                    max_val=10,
                    terminator='\n'
                    )
    for channel in dac.channels:

        channel.update_period.set(50)
        channel.ramp(0, 0.3)

    # Lockins
    lockin_up = SR830("lockin_up", lockin_up_connection)
    lockin_down = SR830("lockin_down", lockin_down_connection)

    # Keithleys
    keithley_right = Keithley_2401('keithley_right', keithley_right_connection)
    keithley_left = Keithley_2401('keithley_left', keithley_left_connection)

    time.sleep(5)

    keithley_right.output.set(1)
    keithley_left.output.set(1)

    # Aquire voltage once as it seems to stabelize the connection
    _ = keithley_left.volt()
    _ = keithley_right.volt()

    keithley_right.rangev(5)
    keithley_right.rangei(1e-06)
    keithley_right.compliancei.set(1e-06)
    keithley_left.rangev(5)
    keithley_left.rangei(1e-06)
    keithley_left.compliancei.set(1e-06)

    # Aquire voltage once as it seems to stabelize the connection
    _ = keithley_left.volt()
    _ = keithley_right.volt()

    # Add to Station
    station = qc.Station(
                            clock,
                            dac,
                            lockin_up,
                            lockin_down,
                            keithley_right,
                            keithley_left
                            )

# Initialize database
qc.initialise_or_create_database_at(database_path)


# %% Useful functions
def reset_lockins():
    """Reset (Set to 0V) Lockins."""
    print('Resetting Lockins...', end='')
    lockin_up.amplitude.set(0.004)
    lockin_down.amplitude.set(0.004)
    print('Done!')


def reset_decadac():
    """Reset (Set to 0V) All Decadac channels."""
    print('Resetting DECADAC...', end='')
    for channel in dac.channels:
        if channel.volt.get() != 0:
            channel.ramp(0, 0.3)
        else:
            continue
    print('Done!')


def reset_keithleys():
    """Reset (Set to 0V) All Keithley channels."""
    print('Resetting Keithleys...', end='')
    ramp_keithley(keithley_left, 0)
    ramp_keithley(keithley_right, 0)
    print('Done!')


def reset_all():
    """Reset (Set to 0V) all Instruments."""
    reset_lockins()
    reset_decadac()
    reset_keithleys()
    print('All Instruments reset!')


def prepare_lockins(lockin_amplitude_up = sample_config['lockin_amplitude']['lockin_amplitude_up'], lockin_amplitude_down = sample_config['lockin_amplitude']['lockin_amplitude_down']):
    """Prepare Lockins for measurement (Set Frequency and Amplitude)."""
    print('Preparing Lockins...', end='')
    lockin_up.amplitude.set(lockin_amplitude_up)
    lockin_up.frequency(77)
    lockin_up.sensitivity(10e-09)
    lockin_up.input_config('I 1M')
    lockin_up.input_coupling('AC')
    lockin_up.input_shield('float')
    lockin_down.amplitude.set(lockin_amplitude_down)
    lockin_down.frequency(111)
    lockin_down.sensitivity(10e-09)
    lockin_down.input_config('I 1M')
    lockin_down.input_coupling('AC')
    lockin_down.input_shield('float')
    print('Done!')

V_th_right = 0
V_th_left = 0
def save_current_OP():
    """Save currently applied voltages to gates."""
    _current_OP = get_current_voltage_configuration()
    sample_config['current_OP'] = _current_OP.copy()
    
    global V_th_right
    global V_th_left
    V_th_right = sample_config['current_OP']['voltages_set_right'][gate_top_right]
    V_th_left = sample_config['current_OP']['voltages_set_left'][gate_top_left]


def get_current_voltage_configuration():
    """Get current applied voltages."""
    _current_voltage = {}
    _current_voltage['voltages_set_left'] = {}
    for gate in gates_set_left:
        _current_voltage['voltages_set_left'][gate] = get_decadac_channel_list([gate])[0].volt()
    _current_voltage['voltages_set_left'][gate_top_left] = get_keithley(gate_top_left).volt()

    _current_voltage['voltages_set_right'] = {}
    for gate in gates_set_right:
        _current_voltage['voltages_set_right'][gate] = get_decadac_channel_list([gate])[0].volt()
    _current_voltage['voltages_set_right'][gate_top_right] = get_keithley(gate_top_right).volt()

    _current_voltage['voltages_channel'] = {}
    for gate in gates_channel:
        _current_voltage['voltages_channel'][gate] = get_decadac_channel_list([gate])[0].volt()
    
    return _current_voltage.copy()

def reset_to_OP():
    """Reset gates to saved OP."""
    assert 'current_OP' in sample_config.keys(), "No OP saved!"
    _current_OP = sample_config['current_OP']

    ramp_keithley(get_keithley(gate_top_left), _current_OP['voltages_set_left'][gate_top_left])
    for gate in gates_set_left:
        get_decadac_channel_list([gate])[0].ramp(_current_OP['voltages_set_left'][gate], 0.3)

    ramp_keithley(get_keithley(gate_top_right), _current_OP['voltages_set_right'][gate_top_right])
    for gate in gates_set_right:
        get_decadac_channel_list([gate])[0].ramp(_current_OP['voltages_set_right'][gate], 0.3)

    for gate in gates_channel:
        get_decadac_channel_list([gate])[0].ramp(_current_OP['voltages_channel'][gate], 0.3)


def tune_gate(tuning_function):
    """Wraps a tuning function such that if only one voltage is provided, all
    gates are ramped."""
    
    @functools.wraps(tuning_function)
    def tune(*args, save_OP=True):
        if len(args) == 1:
            tuning_function(*(tuning_function.__code__.co_argcount-1)*[args[0]], save_OP=save_OP)
        else:
            tuning_function(args, save_OP=save_OP)
    
    return tune

@tune_gate
def tune_left_SET(voltage_TGL, voltage_LB1, voltage_LB2, voltage_LP, save_OP = True):
    """Tune left SET.
        
    Parameters
    ----------
    voltage_TGL, voltage_LB1, voltage_LB2, voltage_LP, save_OP = True
    """
    get_decadac_channel_list(['LB1'])[0].ramp(voltage_LB1, 0.3)
    get_decadac_channel_list(['LB2'])[0].ramp(voltage_LB2, 0.3)
    get_decadac_channel_list(['LP'])[0].ramp(voltage_LP, 0.3)
    ramp_keithley(get_keithley('TGL'), voltage_TGL)

    if save_OP:
        save_current_OP()

@tune_gate
def tune_right_SET(voltage_TGR, voltage_RB1, voltage_RB2, voltage_RP, save_OP = True):
    """Tune right SET.
        
    Parameters
    ----------
    voltage_TGR, voltage_RB1, voltage_RB2, voltage_RP, save_OP = True
    """
    get_decadac_channel_list(['RB1'])[0].ramp(voltage_RB1, 0.3)
    get_decadac_channel_list(['RB2'])[0].ramp(voltage_RB2, 0.3)
    get_decadac_channel_list(['RP'])[0].ramp(voltage_RP, 0.3)
    ramp_keithley(get_keithley('TGR'), voltage_TGR)

    if save_OP:
        save_current_OP()

@tune_gate
def tune_channel(voltage_TLB1, voltage_TLP, voltage_TLB2, voltage_S1, voltage_S2, voltage_S3, voltage_S4, voltage_TRB2, voltage_TRP, voltage_TRB1, save_OP = True):
    """Tune channel.
    
    Parameters
    ----------
    voltage_TLB1, voltage_TLP, voltage_TLB2, voltage_S1, voltage_S2, voltage_S3, voltage_S4, voltage_TRB2, voltage_TRP, voltage_TRB1, save_OP = True
    """
    get_decadac_channel_list(['TLB1'])[0].ramp(voltage_TLB1, 0.3)
    get_decadac_channel_list(['TLP'])[0].ramp(voltage_TLP, 0.3)
    get_decadac_channel_list(['TLB2'])[0].ramp(voltage_TLB2, 0.3)
    get_decadac_channel_list(['S1'])[0].ramp(voltage_S1, 0.3)
    get_decadac_channel_list(['S2'])[0].ramp(voltage_S2, 0.3)
    get_decadac_channel_list(['S3'])[0].ramp(voltage_S3, 0.3)
    get_decadac_channel_list(['S4'])[0].ramp(voltage_S4, 0.3)
    get_decadac_channel_list(['TRB2'])[0].ramp(voltage_TRB2, 0.3)
    get_decadac_channel_list(['TRP'])[0].ramp(voltage_TRP, 0.3)
    get_decadac_channel_list(['TRB1'])[0].ramp(voltage_TRB1, 0.3)

    if save_OP:
        save_current_OP()

def get_port(label: str):
    """Get the port from label."""
    assert label in sample_config['gate_mapping'].values(), f'{label} not mapped!'

    return int({v: k for k,v in sample_config['gate_mapping'].items()}[label])


def get_decadac_index(label: str):
    """Get the decadac channel index from label."""
    _port = str(get_port(label))
    assert _port in sample_config['DECADAC_mapping'].values(), f'{label} not mapped to DECADAC!'

    return int({v: k for k,v in sample_config['DECADAC_mapping'].items()}[_port])


def get_decadac_multiindex(labels: list):
    """Get a list of decadac channel indices from a list of labels."""
    _decadac_indices = []
    for label in labels:
        _decadac_indices.append(get_decadac_index(label))

    return _decadac_indices


def get_decadac_channel_list(labels: list):
    """Get a list of decadac channels from a list of labels."""
    _decadac_channels = []

    for label in labels:
        _decadac_channels.append(dac.channels[get_decadac_index(label)])

    return _decadac_channels


def get_keithley(label: str):
    """Get Keithley variable from label."""
    _port = str(get_port(label))
    assert _port in sample_config['Keithley_mapping'].values(), f'{label} not mapped to Keithley!'

    return eval({v: k for k,v in sample_config['Keithley_mapping'].items()}[_port])


def get_lockin(label: str):
    """Get Lockin variable from label."""
    _lockin_mappings = {k: list(sample_config['Lockin_mapping'][k].values()) for k in sample_config['Lockin_mapping'].keys()}
    _lockin_ports = []
    for _ports in list(_lockin_mappings.values()):
        _lockin_ports += [_ports[0], _ports[1]]

    _port = str(get_port(label))
    assert _port in _lockin_ports, f'{label} not mapped to Lockin!'

    _index_found = None
    _lockin_mappings_values = list(_lockin_mappings.values())
    for _i in range(len(_lockin_mappings_values)):
        if _port in _lockin_mappings_values[_i]:
            _index_found = _i
            break

    return eval(list(_lockin_mappings.keys())[_index_found])


# %% Find SET operating points
lockin_threshold = 1e-09
stability_measurement_length = 150

pinch_off_analysis_right = {}
pinch_off_analysis_left = {}

reset_all()
prepare_lockins()

try:
    pinch_off_analysis_right = set_find_operating_point(station, sample_config, topgates = [get_keithley(gate_top_right)], decadac_gates = get_decadac_channel_list(gates_set_right), ohmic = get_lockin(gate_ohmic_right), gate_volt_range = [0, 1.5], gate_volt_step = 0.005, delay = 0.5, lockin_threshold = lockin_threshold, clock = clock, waittime_stability_measurement = .5, number_stability_measurement = stability_measurement_length*2, experiment_name_prefix = 'Right_')
except BaseException as err:
    print('An error has occured while tuning right SET!')
    print('Resetting right SET...')
    ramp_keithley(get_keithley(gate_top_right), 0)
    for channel in get_decadac_channel_list(gates_set_right):
        channel.ramp(0, 0.3)

    print(err)

time.sleep(5)

try:
    pinch_off_analysis_left = set_find_operating_point(station, sample_config, topgates = [get_keithley(gate_top_left)], decadac_gates = get_decadac_channel_list(gates_set_left), ohmic = get_lockin(gate_ohmic_left), gate_volt_range = [0, 1.5], gate_volt_step = 0.005, delay = 0.5, lockin_threshold = lockin_threshold, clock = clock, waittime_stability_measurement = .5, number_stability_measurement = stability_measurement_length*2,  experiment_name_prefix = 'Left_')
except BaseException as err:
    print('An error has occured while tuning left SET!')
    print('Resetting left SET...')
    ramp_keithley(get_keithley(gate_top_left), 0)
    for channel in get_decadac_channel_list(gates_set_left):
        channel.ramp(0, 0.3)

    print(err)

time.sleep(5)

print("Saving operating point...")
save_current_OP()

print(f'Right tuning result: {pinch_off_analysis_right}')
print(f'Left tuning result: {pinch_off_analysis_left}')


# %% Check pinchoff hysteresis for SETs
lockin_threshold = 1e-09

_number_sweep_pairs = 1

reset_all()
prepare_lockins()

try:
    for i in range(_number_sweep_pairs):
        print(f'\nStarting sweep pair {i+1}...')
        _ = linear_1d_sweep_gates_measure_ohmics(station, sample_config, topgates = [get_keithley(gate_top_right)], decadac_gates = get_decadac_channel_list(gates_set_right),  ohmics = [get_lockin(gate_ohmic_right)], gate_volt_range = [0, 1.5], gate_volt_step =  0.005, lockin_threshold = lockin_threshold, experiment_name_prefix = 'Right_SET_Hysteresis_Up_', delay = 0.5)
        _ = linear_1d_sweep_gates_measure_ohmics(station, sample_config, topgates = [get_keithley(gate_top_right)], decadac_gates = get_decadac_channel_list(gates_set_right),  ohmics = [get_lockin(gate_ohmic_right)], gate_volt_range = [1.5, 0], gate_volt_step =  -0.005, lockin_threshold = lockin_threshold, experiment_name_prefix = 'Right_SET_Hysteresis_Down_', delay = 0.5)

except BaseException as err:
    print('An error has occured while checking hysteresis of right SET')
    print(err)

try:
    for i in range(_number_sweep_pairs):
        print(f'\nStarting sweep pair {i+1}...')
        _ = linear_1d_sweep_gates_measure_ohmics(station, sample_config, topgates = [get_keithley(gate_top_left)], decadac_gates = get_decadac_channel_list(gates_set_left),  ohmics = [get_lockin(gate_ohmic_left)], gate_volt_range = [0, 1.5], gate_volt_step =  0.005, lockin_threshold = lockin_threshold, experiment_name_prefix = 'Left_SET_Hysteresis_Up_', delay = 0.5)
        _ = linear_1d_sweep_gates_measure_ohmics(station, sample_config, topgates = [get_keithley(gate_top_left)], decadac_gates = get_decadac_channel_list(gates_set_left),  ohmics = [get_lockin(gate_ohmic_left)], gate_volt_range = [1.5, 0], gate_volt_step =  -0.005, lockin_threshold = lockin_threshold, experiment_name_prefix = 'Left_SET_Hysteresis_Up_', delay = 0.5)

except BaseException as err:
    print('An error has occured while checking hysteresis of left SET')
    print(err)


# %% SET 2D Barrier-Barrier sweep
lockin_threshold = 1e-09

gate_volt_range_RB1 = [V_th_right, V_th_right]
gate_volt_range_RB2 = [V_th_right, V_th_right]
number_steps_RB1 = 1
number_steps_RB2 = 1

gate_volt_range_LB1 = [0.650, 0.700]
gate_volt_range_LB2 = [0.580, 0.660]
number_steps_LB1 = 10
number_steps_LB2 = 10


print("Resetting to operating point...")
reset_to_OP()
prepare_lockins()

_experiment_metadata = {}

print('Performing 2D scans...')

try:
    _dataset = linear_2d_sweep_gates_measure_ohmics_independend_ranges(station, sample_config.copy(), decadac_gates_1 = get_decadac_channel_list(['RB1']), decadac_gates_2 = get_decadac_channel_list(['RB2']), ohmics = [get_lockin(gate_ohmic_right)], gate_volt_ranges_1 = [gate_volt_range_RB1], number_gate_steps_1 = number_steps_RB1, gate_volt_ranges_2 = [gate_volt_range_RB2], number_gate_steps_2 = number_steps_RB2, lockin_threshold = lockin_threshold, experiment_name_prefix = 'Right_B_vs_B_', experiment_metadata = _experiment_metadata.copy())
except BaseException as err:
    print('An error has occured while analyzing right SET')
    print(err)

try:
    _dataset = linear_2d_sweep_gates_measure_ohmics_independend_ranges(station, sample_config.copy(), decadac_gates_1 = get_decadac_channel_list(['LB1']), decadac_gates_2 = get_decadac_channel_list(['LB2']), ohmics = [get_lockin(gate_ohmic_left)], gate_volt_ranges_1 = [gate_volt_range_LB1], number_gate_steps_1 = number_steps_LB1, gate_volt_ranges_2 = [gate_volt_range_LB2], number_gate_steps_2 = number_steps_LB2, lockin_threshold = lockin_threshold, experiment_name_prefix = 'Left_B_vs_B_', experiment_metadata = _experiment_metadata.copy())
except BaseException as err:
    print('An error has occured while analyzing right SET')
    print(err)

print("Resetting to operating point...")
reset_to_OP()


# %% SET 2D Barrier-Barrier vs Plunger sweep
lockin_threshold = 2e-09

gate_volt_range_RB1 = [V_th_right, V_th_right]
gate_volt_range_RB2 = [V_th_right, V_th_right]
gate_volt_range_RP = [V_th_right, V_th_right]
number_steps_RB12 = 1
number_steps_RP = 1

gate_volt_range_LB1 = 2*[0.650 + 0.030]
gate_volt_range_LB2 = 2*[0.580 + 0.048]
gate_volt_range_LP = [0.480, 0.630]
number_steps_LB12 = 1
number_steps_LP = 100


_experiment_metadata = {'current_voltage_config': get_current_voltage_configuration()}

print("Resetting to operating point...")
reset_to_OP()
prepare_lockins()

print('Performing 2D scans...')

try:
    _dataset = linear_2d_sweep_gates_measure_ohmics_independend_ranges(station, sample_config.copy(), decadac_gates_1 = get_decadac_channel_list(['RB1', 'RB2']), decadac_gates_2 = get_decadac_channel_list(['RP']), ohmics = [get_lockin(gate_ohmic_right)], gate_volt_ranges_1 = [gate_volt_range_RB1, gate_volt_range_RB2], number_gate_steps_1 = number_steps_RB12, gate_volt_ranges_2 = [gate_volt_range_RP], number_gate_steps_2 = number_steps_RP, lockin_threshold = lockin_threshold, experiment_name_prefix = 'Right_BB_vs_Plunger_', experiment_metadata = _experiment_metadata.copy())
except BaseException as err:
    print('An error has occured while analyzing right SET')
    print(err)

try:
    _dataset = linear_2d_sweep_gates_measure_ohmics_independend_ranges(station, sample_config.copy(), decadac_gates_1 = get_decadac_channel_list(['LB1', 'LB2']), decadac_gates_2 = get_decadac_channel_list(['LP']), ohmics = [get_lockin(gate_ohmic_left)], gate_volt_ranges_1 = [gate_volt_range_LB1, gate_volt_range_LB2], number_gate_steps_1 = number_steps_LB12, gate_volt_ranges_2 = [gate_volt_range_LP], number_gate_steps_2 = number_steps_LP, lockin_threshold = lockin_threshold, experiment_name_prefix = 'Left_BB_vs_Plunger_', experiment_metadata = _experiment_metadata.copy())
except BaseException as err:
    print('An error has occured while analyzing right SET')
    print(err)

print("Resetting to operating point...")
reset_to_OP()


# %% Sweep channel
lockin_threshold = 10e-09

gate_volt_range = [0, 1.5]
gate_volt_step = 0.05

_experiment_metadata = {}

print("Resetting to operating point...")
reset_to_OP()
prepare_lockins()

# Set lockins so that they can measure each other
print("Preparing lockins for virtual cross connection...")
get_lockin(gate_ohmic_right).frequency(111)
get_lockin(gate_ohmic_left).frequency(111)
get_lockin(gate_ohmic_left).amplitude(0.004)

print('Sweeping the channel...')

_dataset = linear_1d_sweep_gates_measure_ohmics(station, sample_config.copy(), topgates = [], decadac_gates = get_decadac_channel_list(gates_channel), ohmics = [get_lockin(gate_ohmic_left), get_lockin(gate_ohmic_right)], gate_volt_range = gate_volt_range, gate_volt_step = gate_volt_step, delay = 0.5, lockin_threshold = lockin_threshold, experiment_name_prefix = 'Channel_', experiment_metadata = _experiment_metadata.copy())


# %% individual pinchoff channel
lockin_threshold = 5e-09

V_th_channel = np.mean(list(sample_config['current_OP']['voltages_channel'].values()))

gate_volt_range = [0, 1.000]
gate_volt_step = 0.025


_experiment_metadata = {'Voltages': get_current_voltage_configuration()}
_experiment_metadata['V_th_channel'] = V_th_channel

print("Resetting to operating point...")
reset_to_OP()
prepare_lockins()

# Set lockins so that they can measure each other
print("Preparing lockins for virtual cross connection...")
get_lockin(gate_ohmic_right).frequency(111)
get_lockin(gate_ohmic_left).frequency(111)
get_lockin(gate_ohmic_left).amplitude(0.004)

print('Checking individual pinchoff for channel gates...')

_dataset = sequence_linear_1d_sweep_gates_measure_ohmics(station, sample_config.copy(), decadac_gates_list = [[channel] for channel in get_decadac_channel_list(gates_channel)], ohmics = [get_lockin(gate_ohmic_left), get_lockin(gate_ohmic_right)], gate_volt_range = gate_volt_range, gate_volt_step = gate_volt_step, default_gate_voltage = V_th_channel, lockin_threshold = lockin_threshold, experiment_name_prefix = 'Channel_individual_pinchoff_', delay = 0.5, experiment_metadata = _experiment_metadata.copy())

print("Resetting to operating point...")
reset_to_OP()
prepare_lockins()


# %% Sweep channel cross-connected lockin_down
lockin_threshold = 1e-09

gate_volt_range = [0, 1]
gate_volt_step = 0.005

_experiment_metadata = {}

print("Resetting to operating point...")
reset_to_OP()
prepare_lockins()

print('Sweeping the channel...')

_dataset = linear_1d_sweep_gates_measure_ohmics(station, sample_config.copy(), topgates = [], decadac_gates = get_decadac_channel_list(gates_channel), ohmics = [lockin_down], gate_volt_range = gate_volt_range, gate_volt_step = gate_volt_step, delay = 0.5, lockin_threshold = lockin_threshold, experiment_name_prefix = 'Channel_', experiment_metadata = _experiment_metadata.copy())


# %% individual pinchoff channel (cross-connected lockin_down)
lockin_threshold = 1e-09

V_th_channel = V_th_right

gate_volt_range = [0, 1.5]
gate_volt_step = 0.005

_experiment_metadata = {}
_experiment_metadata['V_th_channel'] = V_th_channel

print("Resetting to operating point...")
reset_to_OP()
prepare_lockins()

print(f'Setting Channel to {V_th_channel}...')
for channel in get_decadac_channel_list(gates_channel):
    if channel.volt.get() != V_th_channel:
        channel.ramp(V_th_channel, 0.3)
    else:
        continue

print("Saving operating point...")
save_current_OP()

print('Checking individual pinchoff for channel gates...')

_dataset = sequence_linear_1d_sweep_gates_measure_ohmics(station, sample_config.copy(), decadac_gates_list = [[channel] for channel in get_decadac_channel_list(gates_channel)], ohmics = [lockin_down], gate_volt_range = gate_volt_range, gate_volt_step = gate_volt_step, default_gate_voltage = V_th_channel, lockin_threshold = lockin_threshold, experiment_name_prefix = 'Channel_individual_pinchoff_', delay = 0.5, experiment_metadata = _experiment_metadata.copy())


# %% Set, wait, Sweep SET pinchoff series
lockin_threshold = 1e-09

set_and_sense_voltages = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5, V_th_right, V_th_left]  # in V
set_and_sense_wait_time = 120  # in seconds

gate_volt_range = [0, 1.5]
gate_volt_step = 0.005

_experiment_metadata = {}

reset_all()
prepare_lockins()

for set_and_sense_voltage in set_and_sense_voltages:
    _experiment_metadata['V_set_and_sense'] = set_and_sense_voltage

    print(f'Setting to {set_and_sense_voltage} V...')

    ramp_keithley(get_keithley(gate_top_right), set_and_sense_voltage)
    ramp_keithley(get_keithley(gate_top_left), set_and_sense_voltage)
    for channel in get_decadac_channel_list(gates_set_left + gates_set_right):
        if channel.volt.get() != set_and_sense_voltage:
            channel.ramp(set_and_sense_voltage, 0.3)
        else:
            continue

    print(f'Waiting {set_and_sense_wait_time} s...')
    time.sleep(set_and_sense_wait_time)

    print('Running scans...')
    _dataset = linear_1d_sweep_gates_measure_ohmics(station, sample_config.copy(), topgates = [get_keithley(gate_top_right)], decadac_gates = get_decadac_channel_list(gates_set_right), ohmics = [get_lockin(gate_ohmic_right)], gate_volt_range = gate_volt_range, gate_volt_step = gate_volt_step, delay = 0.5, lockin_threshold = lockin_threshold, experiment_name_prefix = 'Right_set_wait_sweep_', experiment_metadata = _experiment_metadata.copy())
    _dataset = linear_1d_sweep_gates_measure_ohmics(station, sample_config.copy(), topgates = [get_keithley(gate_top_left)], decadac_gates = get_decadac_channel_list(gates_set_left), ohmics = [get_lockin(gate_ohmic_left)], gate_volt_range = gate_volt_range, gate_volt_step = gate_volt_step, delay = 0.5, lockin_threshold = lockin_threshold, experiment_name_prefix = 'Left_set_wait_sweep_', experiment_metadata = _experiment_metadata.copy())

    print('\n--------------------------------------\n')


# %% Manual commands

# %% 1D sweep
_topgates = [keithley_right]
_decadac_gates = []
_ohmics = [get_lockin('ORT')]
_gate_volt_range = [0, 1.5]
_gate_volt_step = 0.001
_keithley_threshold = 1e-06
_lockin_threshold = 1e-06
_experiment_name_prefix = 'Manual_Sweep_TGL_Hold_SET_at_0_'
_experiment_metadata = {'current_voltages': get_current_voltage_configuration()}

linear_1d_sweep_gates_measure_ohmics(station, sample_config.copy(), topgates = _topgates, decadac_gates = _decadac_gates, ohmics = _ohmics, gate_volt_range = _gate_volt_range, gate_volt_step = _gate_volt_step, delay = 0.5, lockin_threshold = _lockin_threshold, keithley_threshold = _keithley_threshold, experiment_name_prefix = _experiment_name_prefix, experiment_metadata = _experiment_metadata.copy())


# %% Repeated 1D sweep
n_repetitions = 2
bool_up_down = True

_topgates = [get_keithley('TGR')]
_decadac_gates = []
_ohmics = [get_lockin('ORT')]
_gate_volt_range = [0, 1.5]
_gate_volt_step = 0.005
_keithley_threshold = 1e-09
_lockin_threshold = 1e-09
_experiment_name_prefix = 'Hysteresis_TGR_to_RP_'
_experiment_metadata = {'current_voltages': get_current_voltage_configuration(), 'note': 'Connect keithley_right to gate in experiment name and decadac channel that was connected to the gate to TGR.'}


for i in range(n_repetitions):
    print('Setting to start point...')
    for keithley in _topgates:
        ramp_keithley(keithley, _gate_volt_range[0])
    for gate in _decadac_gates:
        gate.ramp(_gate_volt_range[0], 0.3)
    
    time.sleep(5)
        
    print(f'\nStarting sweep Up {i+1}...')
    linear_1d_sweep_gates_measure_ohmics(station, sample_config.copy(), topgates = _topgates, decadac_gates = _decadac_gates, ohmics = _ohmics, gate_volt_range = _gate_volt_range, gate_volt_step = _gate_volt_step, delay = 0.5, lockin_threshold = _lockin_threshold, keithley_threshold = _keithley_threshold, experiment_name_prefix = f'{_experiment_name_prefix}_Up_{i+1}_', experiment_metadata = _experiment_metadata.copy())
    if bool_up_down:
        print(f'\nStarting sweep Down {i+1}...')
        linear_1d_sweep_gates_measure_ohmics(station, sample_config.copy(), topgates = _topgates, decadac_gates = _decadac_gates, ohmics = _ohmics, gate_volt_range = _gate_volt_range[::-1], gate_volt_step = -_gate_volt_step, delay = 0.5, lockin_threshold = _lockin_threshold, keithley_threshold = _keithley_threshold, experiment_name_prefix = f'{_experiment_name_prefix}_Down_{i+1}_', experiment_metadata = _experiment_metadata.copy())


# %% Hold Constant
_topgates = [keithley_left]
_decadac_gates = []
_ohmics = [get_lockin('OLT'), get_lockin('ORT')]
_gate_voltage = V_th_left
_number_measurements = 600
_wait_between_measurements = 0.1
_keithley_threshold = 5e-09
_lockin_threshold = 5e-09
_experiment_name_prefix = 'Manual_Constant_Both_SETs_'
_experiment_metadata = {'current_voltages': get_current_voltage_configuration()}

constant_set_gates_measure_ohmics(station, sample_config.copy(), topgates = _topgates, decadac_gates = _decadac_gates, ohmics = _ohmics, gate_voltage = _gate_voltage, number_measurements = _number_measurements, wait_between_measurements = _wait_between_measurements, clock = clock, lockin_threshold = _lockin_threshold, keithley_threshold = _keithley_threshold, experiment_name_prefix = _experiment_name_prefix, experiment_metadata = _experiment_metadata.copy())


# %% Manual 2D sweep
_lockin_threshold = 1e-09
_delay = 0.5

_decadac_gates_1 = get_decadac_channel_list(['TLB1'])
_decadac_gates_2 = get_decadac_channel_list(['TLB2'])
_ohmics = [get_lockin('OLT'), get_lockin('ORT')]
_gate_volt_ranges_1 = [[0.560, 0.610]]
_gate_volt_ranges_2 = [[0.550, 0.650]]
_number_steps_1 = 10
_number_steps_2 = 10

_experiment_name_prefix = 'Manual_SET_TLB1_TLP_TLB2_'
_experiment_metadata = {'current_voltages': get_current_voltage_configuration()}

linear_2d_sweep_gates_measure_ohmics_independend_ranges(station, sample_config.copy(), decadac_gates_1 = _decadac_gates_1, decadac_gates_2 = _decadac_gates_2, ohmics = _ohmics, gate_volt_ranges_1 = _gate_volt_ranges_1, number_gate_steps_1 = _number_steps_1, gate_volt_ranges_2 = _gate_volt_ranges_2, number_gate_steps_2 = _number_steps_2, lockin_threshold = lockin_threshold, experiment_name_prefix = _experiment_name_prefix, experiment_metadata = _experiment_metadata.copy(), delay=_delay)


#%% Test structures

# %% Check pinchoff for test structures
pinch_off_analysis_top = {}
pinch_off_analysis_bottom = {}

reset_all()
prepare_lockins()

try:
    pinch_off_analysis_top = set_find_operating_point(station, sample_config, topgates = [get_keithley('TGT')], decadac_gates = [], ohmic = get_lockin('OLT'), gate_volt_range = [0, 1.5], gate_volt_step = 0.005, delay = 0.5, experiment_name_prefix = 'Teststructure_Top_', lockin_threshold=10e-09)
except BaseException as err:
    print('An error has occured while tuning top Teststructure')
    print(err)

try:
    pinch_off_analysis_bottom = set_find_operating_point(station, sample_config, topgates = [get_keithley('TGB')], decadac_gates = [], ohmic = get_lockin('OLB'), gate_volt_range = [0, 1.5], gate_volt_step = 0.005, delay = 0.5, experiment_name_prefix = 'Teststructure_Bottom_', lockin_threshold=10e-09)
except BaseException as err:
    print('An error has occured while tuning bottom Teststructure')
    print(err)

print(f'Top tuning result: {pinch_off_analysis_top}')
print(f'Bottom tuning result: {pinch_off_analysis_bottom}')

V_th_top = 0
V_th_bottom = 0
try:
    V_th_top = pinch_off_analysis_top['V_th']
except:
    pass
try:
    V_th_bottom = pinch_off_analysis_bottom['V_th']
except:
    pass

# %% Check pinchoff hysteresis for test_structures

_number_sweep_pairs = 10

reset_all()
prepare_lockins()

try:
    for i in range(_number_sweep_pairs):
        print(f'Starting sweep pair {i+1}...')
        _ = linear_1d_sweep_gates_measure_ohmics(station, sample_config, topgates = [get_keithley('TGT')], decadac_gates = [],  ohmics = [get_lockin('OLT')], gate_volt_range = [0, 2], gate_volt_step =  0.005, lockin_threshold = 10e-09, experiment_name_prefix = 'Hysteresis_Up_Teststructure_Top_', delay = 0.5)
        _ = linear_1d_sweep_gates_measure_ohmics(station, sample_config, topgates = [get_keithley('TGT')], decadac_gates = [],  ohmics = [get_lockin('OLT')], gate_volt_range = [2, 0], gate_volt_step =  -0.005, lockin_threshold = 10e-09, experiment_name_prefix = 'Hysteresis_Down_Teststructure_Top_', delay = 0.5)

except BaseException as err:
    print('An error has occured while checking hysteresis of top Teststructure')
    print(err)

try:
    for i in range(_number_sweep_pairs):
        print(f'Starting sweep pair {i+1}...')
        _ = linear_1d_sweep_gates_measure_ohmics(station, sample_config, topgates = [get_keithley('TGB')], decadac_gates = [],  ohmics = [get_lockin('OLB')], gate_volt_range = [0, 2], gate_volt_step =  0.005, lockin_threshold = 10e-09, experiment_name_prefix = 'Hysteresis_Up_Teststructure_Bottom_', delay = 0.5)
        _ = linear_1d_sweep_gates_measure_ohmics(station, sample_config, topgates = [get_keithley('TGB')], decadac_gates = [],  ohmics = [get_lockin('OLB')], gate_volt_range = [2, 0], gate_volt_step =  -0.005, lockin_threshold = 10e-09, experiment_name_prefix = 'Hysteresis_Down_Teststructure_Bottom_', delay = 0.5)

except BaseException as err:
    print('An error has occured while checking hysteresis of bottom Teststructure')
    print(err)

# %% Check TLMs

##############################################
######### START OF USER DEFINED CODE #########
##############################################

_TLM_side = 'Left'
TLM_range = [-200e-03, 200e-03]
TLM_step = (np.max(TLM_range) - np.min(TLM_range)) / 100

##############################################
######### END OF USER DEFINED CODE ###########
##############################################

reset_all()
keithley_left.rangev(2 * np.max(np.abs(TLM_range)))
keithley_left.rangei(2 * np.max(np.abs(TLM_range)) / 250)
keithley_left.compliancei.set(2 * np.max(np.abs(TLM_range)) / 250)

try:
    _input = input('Please Float all gates and confirm! (y/n) ')
    assert _input == 'y'

    _input = input('Connect TLM-1 to keithley_left and confirm! (y/n) ')
    assert _input == 'y'

    print('Starting baseline sweep...')
    _ = linear_1d_sweep_gates_measure_ohmics(station, sample_config, topgates = [keithley_left], decadac_gates = [],  ohmics = [], gate_volt_range = TLM_range, gate_volt_step = TLM_step, experiment_name_prefix = f'TLM_base_{_TLM_side}_', delay = 0.5, keithley_threshold = 2e-03)

    print('\n----------------\n')
    _input = input('Ground TLM-2 and confirm! (y/n) ')
    assert _input == 'y'

    print('Starting sweep TLM-1_2...')
    _ = linear_1d_sweep_gates_measure_ohmics(station, sample_config, topgates = [keithley_left], decadac_gates = [],  ohmics = [], gate_volt_range = TLM_range, gate_volt_step = TLM_step, experiment_name_prefix = f'TLM_1_2_{_TLM_side}_', delay = 0.5, keithley_threshold = 2e-03)

    print('\n----------------\n')
    _input = input('Float TLM-2, ground TLM-3 and confirm! (y/n) ')
    assert _input == 'y'

    print('Starting sweep TLM-1_3...')
    _ = linear_1d_sweep_gates_measure_ohmics(station, sample_config, topgates = [keithley_left], decadac_gates = [],  ohmics = [], gate_volt_range = TLM_range, gate_volt_step = TLM_step, experiment_name_prefix = f'TLM_1_3_{_TLM_side}_', delay = 0.5, keithley_threshold = 2e-03)

    print('\n----------------\n')
    _input = input('Float TLM-3, ground TLM-4 and confirm! (y/n) ')
    assert _input == 'y'

    print('Starting sweep TLM-1_4...')
    _ = linear_1d_sweep_gates_measure_ohmics(station, sample_config, topgates = [keithley_left], decadac_gates = [],  ohmics = [], gate_volt_range = TLM_range, gate_volt_step = TLM_step, experiment_name_prefix = f'TLM_1_4_{_TLM_side}_', delay = 0.5, keithley_threshold = 2e-03)

    print('\n----------------\n')
    _input = input('Float TLM-4, ground TLM-5 and confirm! (y/n) ')
    assert _input == 'y'

    print('Starting sweep TLM-1_5...')
    _ = linear_1d_sweep_gates_measure_ohmics(station, sample_config, topgates = [keithley_left], decadac_gates = [],  ohmics = [], gate_volt_range = TLM_range, gate_volt_step = TLM_step, experiment_name_prefix = f'TLM_1_5_{_TLM_side}_', delay = 0.5, keithley_threshold = 2e-03)


    _input = input('Please Float all gates and confirm! (y/n) ')
    assert _input == 'y'

    _input = input('Connect TLM-2 to keithley_left and confirm! (y/n) ')
    assert _input == 'y'

    print('\n----------------\n')
    _input = input('Ground TLM-3 and confirm! (y/n) ')
    assert _input == 'y'

    print('Starting sweep TLM-2_3...')
    _ = linear_1d_sweep_gates_measure_ohmics(station, sample_config, topgates = [keithley_left], decadac_gates = [],  ohmics = [], gate_volt_range = TLM_range, gate_volt_step = TLM_step, experiment_name_prefix = f'TLM_2_3_{_TLM_side}_', delay = 0.5, keithley_threshold = 2e-03)

    print('\n----------------\n')
    _input = input('Float TLM-3, ground TLM-4 and confirm! (y/n) ')
    assert _input == 'y'

    print('Starting sweep TLM-2_4...')
    _ = linear_1d_sweep_gates_measure_ohmics(station, sample_config, topgates = [keithley_left], decadac_gates = [],  ohmics = [], gate_volt_range = TLM_range, gate_volt_step = TLM_step, experiment_name_prefix = f'TLM_2_4_{_TLM_side}_', delay = 0.5, keithley_threshold = 2e-03)

    print('\n----------------\n')
    _input = input('Float TLM-4, ground TLM-5 and confirm! (y/n) ')
    assert _input == 'y'

    print('Starting sweep TLM-2_5...')
    _ = linear_1d_sweep_gates_measure_ohmics(station, sample_config, topgates = [keithley_left], decadac_gates = [],  ohmics = [], gate_volt_range = TLM_range, gate_volt_step = TLM_step, experiment_name_prefix = f'TLM_2_5_{_TLM_side}_', delay = 0.5, keithley_threshold = 2e-03)


    _input = input('Please Float all gates and confirm! (y/n) ')
    assert _input == 'y'

    _input = input('Connect TLM-3 to keithley_left and confirm! (y/n) ')
    assert _input == 'y'

    print('\n----------------\n')
    _input = input('Ground TLM-4 and confirm! (y/n) ')
    assert _input == 'y'

    print('Starting sweep TLM-3_4...')
    _ = linear_1d_sweep_gates_measure_ohmics(station, sample_config, topgates = [keithley_left], decadac_gates = [],  ohmics = [], gate_volt_range = TLM_range, gate_volt_step = TLM_step, experiment_name_prefix = f'TLM_3_4_{_TLM_side}_', delay = 0.5, keithley_threshold = 2e-03)

    print('\n----------------\n')
    _input = input('Float TLM-4, ground TLM-5 and confirm! (y/n) ')
    assert _input == 'y'

    print('Starting sweep TLM-3_5...')
    _ = linear_1d_sweep_gates_measure_ohmics(station, sample_config, topgates = [keithley_left], decadac_gates = [],  ohmics = [], gate_volt_range = TLM_range, gate_volt_step = TLM_step, experiment_name_prefix = f'TLM_3_5_{_TLM_side}_', delay = 0.5, keithley_threshold = 2e-03)


    _input = input('Please Float all gates and confirm! (y/n) ')
    assert _input == 'y'

    _input = input('Connect TLM-4 to keithley_left and confirm! (y/n) ')
    assert _input == 'y'

    print('\n----------------\n')
    _input = input('Ground TLM-5 and confirm! (y/n) ')
    assert _input == 'y'

    print('Starting sweep TLM-4_5...')
    _ = linear_1d_sweep_gates_measure_ohmics(station, sample_config, topgates = [keithley_left], decadac_gates = [],  ohmics = [], gate_volt_range = TLM_range, gate_volt_step = TLM_step, experiment_name_prefix = f'TLM_4_5_{_TLM_side}_', delay = 0.5, keithley_threshold = 2e-03)

except AssertionError:
    print('Aborting...')

keithley_left.rangev(5)
keithley_left.rangei(1e-06)
keithley_left.compliancei.set(1e-06)
reset_all()
