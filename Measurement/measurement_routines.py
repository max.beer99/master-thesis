"""Standard routines for measurements."""
import time
import os
import numpy as np
from tqdm import tqdm
import qcodes as qc
import pyvisa
import json
import functools

import scipy
import scipy.interpolate
import scipy.misc
import scipy.optimize
import scipy.signal

from qcodes.instrument_drivers.Harvard.Decadac import Decadac
from qcodes.instrument_drivers.stanford_research.SR830 import SR830
from qcodes.instrument_drivers.tektronix.Keithley_2450 import Keithley2450
from qcodes.instrument_drivers.tektronix.Keithley_2400 import Keithley_2400
from qcodes.instrument_drivers.tektronix.Keithley_2401 import Keithley_2401
import qcodes.instrument_drivers.Lakeshore.Model_325 as ls  # Temperature sensor
import qcodes.instrument.parameter


class Clock(qcodes.instrument.parameter.Parameter):
    """This class implements a simple clock parameter."""
    def __init__(self, name):
        super().__init__(name,
                       label='Time (s)',
                       docstring='This parameter is a simple clock keeping'
                                 'track of the time that has passed since'
                                 'initialization.')
        self._init_time = time.time()

    def get_raw(self):
        return time.time() - self._init_time


def ramp_keithley(device: 'keithley', value: float, step=0.1, wait=0.01):
    """Ramp keithley device to value."""
    if not device.output.get():
        print('Device output is off. Please enable device output first')
        return False
    if not device.mode.get() == 'VOLT':
        print(
            'Ramping only supported for devices in voltage-source mode '
            'Please set source_funtion to voltage'
        )
        return False

    try:
        if device.volt.get() == value:
            print('Target value already reached')
            return True
        if device.volt.get() > value:
            step *= -1

        set_value = device.volt.get()
        while (set_value < value - np.abs(step)
                or set_value > value + np.abs(step)):
            set_value += step
            device.volt.set(set_value)

            time.sleep(wait)

        device.volt.set(value)

        return True
    except BaseException:
        print('Something went wrong')

def constant_set_gates_measure_ohmics(
                                        station: 'station',
                                        sample_config: dict,
                                        topgates: 'keithley_list',
                                        decadac_gates: 'decadac_channel_list',
                                        ohmics: 'lockin_list',
                                        gate_voltage: float,
                                        number_measurements: float,
                                        wait_between_measurements: float,
                                        clock: Clock,
                                        lockin_threshold=1e-09,
                                        lockin_current_scaling=1,
                                        keithley_threshold=1e-09,
                                        experiment_name_prefix='',
                                        delay=1,
                                        experiment_metadata={}
                                        ):
    """
    Set gates to constant voltage, measure ohmics.

    Set top- and decadac-gates to gate_voltage and measure ohmics
    number_measurements times and wait for wait_betwenn_measurements seconds.
    Break when surpassing thresholds.
    """
    # Set experiment metadata
    experiment_metadata['constant_set_gates_measure_ohmics_params'] = {
            'gate_voltage': gate_voltage,
            'number_measurements': number_measurements,
            'wait_between_measurements': wait_between_measurements,
            'lockin_threshold': lockin_threshold,
            'lockin_current_scaling': lockin_current_scaling,
            'keithley_threshold': keithley_threshold,
            'experiment_name_prefix': experiment_name_prefix,
            'delay': delay
            }

    # Setup experiment and context
    _experiment = qc.load_or_create_experiment(
                                                experiment_name=experiment_name_prefix+"constant_set_gates_measure_ohmics",
                                                sample_name=sample_config['sample_name']
                                                )
    _context_meas = qc.Measurement(
                                    exp=_experiment,
                                    station=station,
                                    name=sample_config['sample_name']
                                    )

    # Register independend parameters
    _context_meas.register_parameter(clock)
    for keithley in topgates:
        _context_meas.register_parameter(keithley.volt)
    for decadac_channel in decadac_gates:
        _context_meas.register_parameter(decadac_channel.volt)
    # Register the dependent parameters
    for keithley in topgates:
        _context_meas.register_parameter(
                                        keithley.curr,
                                        setpoints=[keithley.volt for keithley in topgates]+[decadac_channel.volt for decadac_channel in decadac_gates]+[clock]
                                        )
    for lockin in ohmics:
        _context_meas.register_parameter(
                                        lockin.R,
                                        setpoints=[keithley.volt for keithley in topgates]+[decadac_channel.volt for decadac_channel in decadac_gates]+[clock]
                                        )
        _context_meas.register_parameter(
                                        lockin.P,
                                        setpoints=[keithley.volt for keithley in topgates]+[decadac_channel.volt for decadac_channel in decadac_gates]+[clock]
                                        )

    # Time for periodic background database writes
    _context_meas.write_period = 1

    # Run measurement
    with _context_meas.run() as datasaver:
        datasaver.dataset.add_metadata(tag='sample_config', metadata=json.dumps(sample_config))
        datasaver.dataset.add_metadata(tag='experiment_metadata', metadata=json.dumps(experiment_metadata))
        for decadac_channel in decadac_gates:
            decadac_channel.ramp(gate_voltage, 0.3)
        for keithley in topgates:
            ramp_keithley(keithley, gate_voltage)

        time.sleep(delay)

        for current_step in np.arange(number_measurements):
            time.sleep(wait_between_measurements)

            _break_flag = False
            for keithley in topgates:
                if keithley.curr() >= keithley_threshold:
                    _break_flag = True
                    print('Keithley threshold reached!')
            for lockin in ohmics:
                if lockin.R() >= lockin_threshold:
                    _break_flag = True
                    print('Lockin threshold reached!')
            if _break_flag:
                break

            _results = [(keithley.volt, keithley.volt()) for keithley in topgates] + [(decadac_channel.volt, decadac_channel.volt()) for decadac_channel in decadac_gates] + [(keithley.curr, keithley.curr()) for keithley in topgates] + [(lockin.R, lockin.R() * lockin_current_scaling) for lockin in ohmics] + [(lockin.P, lockin.P()) for lockin in ohmics] + [(clock, clock())]
            datasaver.add_result(*_results)

        return datasaver.dataset


def linear_1d_sweep_gates_measure_ohmics(
                                        station: 'station',
                                        sample_config: dict,
                                        topgates: 'keithley_list',
                                        decadac_gates: 'decadac_channel_list',
                                        ohmics: 'lockin_list',
                                        gate_volt_range: list,
                                        gate_volt_step: float,
                                        lockin_threshold=1e-09,
                                        lockin_current_scaling=1,
                                        keithley_threshold=1e-09,
                                        experiment_name_prefix='',
                                        delay=0.1,
                                        experiment_metadata={}
                                        ):
    """
    Linear sweep of gates, measure ohmics.

    Perform simultaneous linear sweep of top- and decadac-gates while
    measuring current with lockin until end of range or break thresholds.
    """
    # Set experiment metadata
    experiment_metadata['linear_1d_sweep_gates_measure_ohmics'] = {
            'gate_volt_range': gate_volt_range,
            'gate_volt_step': gate_volt_step,
            'lockin_threshold': lockin_threshold,
            'lockin_current_scaling': lockin_current_scaling,
            'keithley_threshold': keithley_threshold,
            'experiment_name_prefix': experiment_name_prefix,
            'delay': delay
            }

    # Setup experiment and context
    _experiment = qc.load_or_create_experiment(
                                                experiment_name=experiment_name_prefix+"linear_1d_sweep_gates_measure_ohmics",
                                                sample_name=sample_config['sample_name']
                                                )
    _context_meas = qc.Measurement(
                                    exp=_experiment,
                                    station=station,
                                    name=sample_config['sample_name']
                                    )

    # Register independend parameters
    for keithley in topgates:
        _context_meas.register_parameter(keithley.volt)
    for decadac_channel in decadac_gates:
        _context_meas.register_parameter(decadac_channel.volt)
    # Register the dependent parameters
    for keithley in topgates:
        _context_meas.register_parameter(
                                        keithley.curr,
                                        setpoints=[keithley.volt for keithley in topgates]+[decadac_channel.volt for decadac_channel in decadac_gates]
                                        )
    for lockin in ohmics:
        _context_meas.register_parameter(
                                        lockin.R,
                                        setpoints=[keithley.volt for keithley in topgates]+[decadac_channel.volt for decadac_channel in decadac_gates]
                                        )
        _context_meas.register_parameter(
                                        lockin.P,
                                        setpoints=[keithley.volt for keithley in topgates]+[decadac_channel.volt for decadac_channel in decadac_gates]
                                        )

    # Time for periodic background database writes
    _context_meas.write_period = 1

    # Run measurement
    with _context_meas.run() as datasaver:
        datasaver.dataset.add_metadata(tag='sample_config', metadata=json.dumps(sample_config))
        datasaver.dataset.add_metadata(tag='experiment_metadata', metadata=json.dumps(experiment_metadata))
        for set_v in np.arange(gate_volt_range[0], gate_volt_range[1], gate_volt_step):
            for decadac_channel in decadac_gates:
                decadac_channel.ramp(set_v, 0.3)
            for keithley in topgates:
                keithley.volt.set(set_v)

            time.sleep(delay)

            _break_flag = False
            for keithley in topgates:
                if keithley.curr() >= keithley_threshold:
                    _break_flag = True
                    print('Keithley threshold reached!')
            for lockin in ohmics:
                if lockin.R() >= lockin_threshold:
                    _break_flag = True
                    print('Lockin threshold reached!')
            if _break_flag:
                break

            _results = [(keithley.volt, keithley.volt()) for keithley in topgates] + [(decadac_channel.volt, decadac_channel.volt()) for decadac_channel in decadac_gates] + [(keithley.curr, keithley.curr()) for keithley in topgates] + [(lockin.R, lockin.R() * lockin_current_scaling) for lockin in ohmics] + [(lockin.P, lockin.P()) for lockin in ohmics]
            datasaver.add_result(*_results)

        return datasaver.dataset


def sequence_linear_1d_sweep_gates_measure_ohmics(
                                                    station: 'station',
                                                    sample_config: dict,
                                                    decadac_gates_list: 'decadac_channel_list_list',
                                                    ohmics: 'lockin_list',
                                                    gate_volt_range: list,
                                                    gate_volt_step: float,
                                                    default_gate_voltage: float,
                                                    lockin_threshold=1e-09,
                                                    lockin_current_scaling=1,
                                                    keithley_threshold=1e-09,
                                                    experiment_name_prefix='',
                                                    delay=0.1,
                                                    experiment_metadata={}
                                                    ):
    """
    A Sequence of linear sweep of decadac gates, measure ohmics.

    Perform sequence of decadac-gates while measuring current with lockin until
    end of range or break thresholds.

    decadac_gates_list is of the form
    [[channel1_1, channel1_2, ...], [channel2_1, channel2_2, ...], ...]
    """
    # Set experiment metadata
    experiment_metadata['sequence_linear_1d_sweep_gates_measure_ohmics'] = {
            'gate_volt_range': gate_volt_range,
            'gate_volt_step': gate_volt_step,
            'default_gate_voltage': default_gate_voltage,
            'lockin_threshold': lockin_threshold,
            'lockin_current_scaling': lockin_current_scaling,
            'keithley_threshold': keithley_threshold,
            'experiment_name_prefix': experiment_name_prefix,
            'delay': delay
            }

    # Mark experiment as sequence_linear_1d_sweep_gates_measure_ohmics
    experiment_name_prefix = experiment_name_prefix + 'sequence_'

    # Run sequence
    _datasets = []
    for decadac_gates_collection in decadac_gates_list:
        _current_dataset = linear_1d_sweep_gates_measure_ohmics(
                                                                station=station,
                                                                sample_config=sample_config,
                                                                topgates=[],
                                                                decadac_gates=decadac_gates_collection,
                                                                ohmics=ohmics,
                                                                gate_volt_range=gate_volt_range,
                                                                gate_volt_step=gate_volt_step,
                                                                lockin_threshold=lockin_threshold,
                                                                lockin_current_scaling=lockin_current_scaling,
                                                                keithley_threshold=keithley_threshold,
                                                                experiment_name_prefix=experiment_name_prefix,
                                                                delay=delay,
                                                                experiment_metadata=experiment_metadata.copy()
                                                                )
        _datasets.append(_current_dataset)

        # Reset to default gate voltage
        for _decadac_channel in decadac_gates_collection:
            _decadac_channel.ramp(default_gate_voltage, 0.3)
        time.sleep(2)

    return _datasets


def linear_1d_sweep_ohmic_measure_ohmic(
                                        station: 'station',
                                        sample_config: dict,
                                        ohmic: 'lockin',
                                        ohmic_volt_range: list,
                                        ohmic_volt_step: float,
                                        lockin_threshold=1e-09,
                                        experiment_name_prefix='',
                                        delay=0.1,
                                        experiment_metadata={}
                                        ):
    """
    Linear sweep ohmic, measure ohmic.

    Perform simultaneous linear sweep of ohmic while measuring current with
    lockin until end of range or break thresholds.
    """
    # Set experiment metadata
    experiment_metadata['linear_1d_sweep_ohmic_measure_ohmic'] = {
            'ohmic_volt_range': ohmic_volt_range,
            'ohmic_volt_step': ohmic_volt_step,
            'lockin_threshold': lockin_threshold,
            'experiment_name_prefix': experiment_name_prefix,
            'delay': delay
            }

    # Setup experiment and context
    _experiment = qc.load_or_create_experiment(
                                                experiment_name=experiment_name_prefix+"linear_1d_sweep_ohmic_measure_ohmic",
                                                sample_name=sample_config['sample_name']
                                                )
    _context_meas = qc.Measurement(
                                    exp=_experiment,
                                    station=station,
                                    name=sample_config['sample_name']
                                    )

    # Register independend parameters
    _context_meas.register_parameter(ohmic.amplitude)
    # Register the dependent parameters
    _context_meas.register_parameter(
                                    ohmic.R,
                                    setpoints=[ohmic.amplitude]
                                    )

    # Time for periodic background database writes
    _context_meas.write_period = 1

    # Run measurement
    with _context_meas.run() as datasaver:
        datasaver.dataset.add_metadata(tag='sample_config', metadata=json.dumps(sample_config))
        datasaver.dataset.add_metadata(tag='experiment_metadata', metadata=json.dumps(experiment_metadata))
        for set_v in np.arange(ohmic_volt_range[0], ohmic_volt_range[1], ohmic_volt_step):
            time.sleep(delay)

            ohmic.amplitude(set_v)

            _break_flag = False
            if ohmic.R() >= lockin_threshold:
                _break_flag = True
                print('Lockin threshold reached!')
            if _break_flag:
                break

            _results = [(ohmic.amplitude, ohmic.amplitude()), (ohmic.R, ohmic.R())]
            datasaver.add_result(*_results)

        return datasaver.dataset

def linear_2d_sweep_gates_measure_ohmics(
                                        station: 'station',
                                        sample_config: dict,
                                        topgates_1: 'keithley_list',
                                        topgates_2: 'keithley_list',
                                        decadac_gates_1: 'decadac_channel_list',
                                        decadac_gates_2: 'decadac_channel_list',
                                        ohmics: 'lockin_list',
                                        gate_volt_range_1: list,
                                        gate_volt_step_1: float,
                                        gate_volt_range_2: list,
                                        gate_volt_step_2: float,
                                        lockin_threshold=1e-09,
                                        lockin_current_scaling=1,
                                        keithley_threshold=1e-09,
                                        experiment_name_prefix='',
                                        decadac_gates_1_offset=[],
                                        decadac_gates_2_offset=[],
                                        delay=0.1,
                                        experiment_metadata={}
                                        ):
    """
    Linear 2D sweep of gate groups 1 and 2, measure ohmics.

    Perform simultaneous linear sweep of top- and decadac-gates within a group
    while measuring current with lockin until end of range or break thresholds.
    """
    # Set experiment metadata
    experiment_metadata['linear_2d_sweep_gates_measure_ohmics'] = {
            'gate_volt_range_1': gate_volt_range_1,
            'gate_volt_step_1': gate_volt_step_1,
            'gate_volt_range_2': gate_volt_range_2,
            'gate_volt_step_2': gate_volt_step_2,
            'lockin_threshold': lockin_threshold,
            'lockin_current_scaling': lockin_current_scaling,
            'keithley_threshold': keithley_threshold,
            'experiment_name_prefix': experiment_name_prefix,
            'decadac_gates_1_offset': decadac_gates_1_offset,
            'decadac_gates_2_offset': decadac_gates_1_offset,
            'delay': delay
            }

    assert len(decadac_gates_1_offset) == len(decadac_gates_1) or decadac_gates_1_offset == [], "incorrect shape of decadac_gates_1_offset!"
    assert len(decadac_gates_2_offset) == len(decadac_gates_2) or decadac_gates_2_offset == [], "incorrect shape of decadac_gates_2_offset!"

    if decadac_gates_1_offset == []:
        decadac_gates_1_offset = [0] * len(decadac_gates_1)
    if decadac_gates_2_offset == []:
        decadac_gates_2_offset = [0] * len(decadac_gates_2)

    # Setup experiment and context
    _experiment = qc.load_or_create_experiment(
                                                experiment_name=experiment_name_prefix+"linear_2d_sweep_gates_measure_ohmics",
                                                sample_name=sample_config['sample_name']
                                                )
    _context_meas = qc.Measurement(
                                    exp=_experiment,
                                    station=station,
                                    name=sample_config['sample_name']
                                    )

    # Register independend parameters
    for keithley in topgates_1:
        _context_meas.register_parameter(keithley.volt)
    for keithley in topgates_2:
        _context_meas.register_parameter(keithley.volt)
    for decadac_channel in decadac_gates_1:
        _context_meas.register_parameter(decadac_channel.volt)
    for decadac_channel in decadac_gates_2:
        _context_meas.register_parameter(decadac_channel.volt)
    # Register the dependent parameters
    for keithley_1 in topgates_1:
        _context_meas.register_parameter(
                                        keithley_1.curr,
                                        setpoints=[keithley_1.volt for keithley_1 in topgates_1]+[keithley_2.volt for keithley_2 in topgates_2]+[decadac_channel_1.volt for decadac_channel_1 in decadac_gates_1]+[decadac_channel_2.volt for decadac_channel_2 in decadac_gates_2]
                                        )
    for keithley_2 in topgates_2:
        _context_meas.register_parameter(
                                        keithley_2.curr,
                                        setpoints=[keithley_1.volt for keithley_1 in topgates_1]+[keithley_2.volt for keithley_2 in topgates_2]+[decadac_channel_1.volt for decadac_channel_1 in decadac_gates_1]+[decadac_channel_2.volt for decadac_channel_2 in decadac_gates_2]
                                        )
    for lockin in ohmics:
        _context_meas.register_parameter(
                                        lockin.R,
                                        setpoints=[keithley_1.volt for keithley_1 in topgates_1]+[keithley_2.volt for keithley_2 in topgates_2]+[decadac_channel_1.volt for decadac_channel_1 in decadac_gates_1]+[decadac_channel_2.volt for decadac_channel_2 in decadac_gates_2]
                                        )
        _context_meas.register_parameter(
                                        lockin.P,
                                        setpoints=[keithley_1.volt for keithley_1 in topgates_1]+[keithley_2.volt for keithley_2 in topgates_2]+[decadac_channel_1.volt for decadac_channel_1 in decadac_gates_1]+[decadac_channel_2.volt for decadac_channel_2 in decadac_gates_2]
                                        )

    # Time for periodic background database writes
    _context_meas.write_period = 1

    # Run measurement
    with _context_meas.run() as datasaver:
        datasaver.dataset.add_metadata(tag='sample_config', metadata=json.dumps(sample_config))
        datasaver.dataset.add_metadata(tag='experiment_metadata', metadata=json.dumps(experiment_metadata))
        for set_v1 in np.arange(gate_volt_range_1[0], gate_volt_range_1[1], gate_volt_step_1):
            for decadac_channel_1_i in range(len(decadac_gates_1)):
                decadac_gates_1[decadac_channel_1_i].ramp(set_v1 + decadac_gates_1_offset[decadac_channel_1_i], 0.3)
            for keithley_1 in topgates_1:
                keithley_1.volt.set(set_v1)

            for set_v2 in np.arange(gate_volt_range_2[0], gate_volt_range_2[1], gate_volt_step_2):
                for decadac_channel_2_i in range(len(decadac_gates_2)):
                    decadac_gates_2[decadac_channel_2_i].ramp(set_v2 + decadac_gates_2_offset[decadac_channel_2_i], 0.3)
                for keithley_2 in topgates_2:
                    keithley_2.volt.set(set_v2)

                time.sleep(delay)

                _break_flag = False
                for keithley_1 in topgates_1:
                    if keithley_1.curr() >= keithley_threshold:
                        _break_flag = True
                        print('Keithley threshold reached!')
                for keithley_2 in topgates_2:
                    if keithley_2.curr() >= keithley_threshold:
                        _break_flag = True
                        print('Keithley threshold reached!')
                for lockin in ohmics:
                    if lockin.R() >= lockin_threshold:
                        _break_flag = True
                        print('Lockin threshold reached!')
                if _break_flag:
                    break

                _results = [(keithley_1.volt, keithley_1.volt()) for keithley_1 in topgates_1] + [(keithley_2.volt, keithley_2.volt()) for keithley_2 in topgates_2] + [(decadac_channel_1.volt, decadac_channel_1.volt()) for decadac_channel_1 in decadac_gates_1] + [(decadac_channel_2.volt, decadac_channel_2.volt()) for decadac_channel_2 in decadac_gates_2] + [(keithley_1.curr, keithley_1.curr()) for keithley_1 in topgates_1] + [(keithley_2.curr, keithley_2.curr()) for keithley_2 in topgates_2] + [(lockin.R, lockin.R() * lockin_current_scaling) for lockin in ohmics] + [(lockin.P, lockin.P()) for lockin in ohmics]
                datasaver.add_result(*_results)

        return datasaver.dataset


def linear_2d_sweep_gates_measure_ohmics_independend_ranges(
                                        station: 'station',
                                        sample_config: dict,
                                        decadac_gates_1: 'decadac_channel_list',
                                        decadac_gates_2: 'decadac_channel_list',
                                        ohmics: 'lockin_list',
                                        gate_volt_ranges_1: list,
                                        number_gate_steps_1: float,
                                        gate_volt_ranges_2: list,
                                        number_gate_steps_2: float,
                                        lockin_threshold=1e-09,
                                        lockin_current_scaling=1,
                                        experiment_name_prefix='',
                                        delay=0.1,
                                        experiment_metadata={}
                                        ):
    """
    Linear 2D sweep of gate groups 1 and 2, measure ohmics.

    Perform simultaneous linear sweep of top- and decadac-gates within a group
    while measuring current with lockin until end of range or break thresholds.

    Each gate in the two sets has its own range. in order to keep the shape of
    the ranges consistent, the number of steps in each set is given and not
    the step size.

    This function also does not allow to sweep a keithley (Not implemented).

    Example:
    For decadac_gates_1 = get_decadac_channel_list(['RB1', 'RB2'])
    One sets gate_volt_ranges_1 = [[0, 1], [0.5, 1]] if one wants to sweep RB1
    from 0 to 1 and RB2 from 0.5 to 1. Therefore an offset parameter is not
    used.
    """
    # Set experiment metadata
    experiment_metadata['linear_2d_sweep_gates_measure_ohmics_independend_ranges'] = {
            'gate_volt_ranges_1': gate_volt_ranges_1,
            'number_gate_steps_1': number_gate_steps_1,
            'gate_volt_ranges_2': gate_volt_ranges_2,
            'number_gate_steps_2': number_gate_steps_2,
            'lockin_threshold': lockin_threshold,
            'lockin_current_scaling': lockin_current_scaling,
            'experiment_name_prefix': experiment_name_prefix,
            'delay': delay
            }

    assert len(gate_volt_ranges_1) == len(decadac_gates_1), "incorrect shape of gate_volt_ranges_1!"
    assert len(gate_volt_ranges_2) == len(decadac_gates_2), "incorrect shape of gate_volt_ranges_2!"

    assert isinstance(gate_volt_ranges_1[0], list), "Please use nested lists for gate_volt_ranges_1!"
    assert isinstance(gate_volt_ranges_2[0], list), "Please use nested lists for gate_volt_ranges_2!"

    # Setup experiment and context
    _experiment = qc.load_or_create_experiment(
                                                experiment_name=experiment_name_prefix+"linear_2d_sweep_gates_measure_ohmics",
                                                sample_name=sample_config['sample_name']
                                                )
    _context_meas = qc.Measurement(
                                    exp=_experiment,
                                    station=station,
                                    name=sample_config['sample_name']
                                    )

    # Register independend parameters
    for decadac_channel in decadac_gates_1:
        _context_meas.register_parameter(decadac_channel.volt)
    for decadac_channel in decadac_gates_2:
        _context_meas.register_parameter(decadac_channel.volt)
    # Register the dependent parameters
    for lockin in ohmics:
        _context_meas.register_parameter(
                                        lockin.R,
                                        setpoints=[decadac_channel_1.volt for decadac_channel_1 in decadac_gates_1]+[decadac_channel_2.volt for decadac_channel_2 in decadac_gates_2]
                                        )
        _context_meas.register_parameter(
                                        lockin.P,
                                        setpoints=[decadac_channel_1.volt for decadac_channel_1 in decadac_gates_1]+[decadac_channel_2.volt for decadac_channel_2 in decadac_gates_2]
                                        )

    # Time for periodic background database writes
    _context_meas.write_period = 1

    # Run measurement
    with _context_meas.run() as datasaver:
        datasaver.dataset.add_metadata(tag='sample_config', metadata=json.dumps(sample_config))
        datasaver.dataset.add_metadata(tag='experiment_metadata', metadata=json.dumps(experiment_metadata))

        voltages_1 = [np.linspace(volt_range[0], volt_range[1], number_gate_steps_1) for volt_range in gate_volt_ranges_1]
        voltages_2 = [np.linspace(volt_range[0], volt_range[1], number_gate_steps_2) for volt_range in gate_volt_ranges_2]

        for index_1 in range(number_gate_steps_1):
            for decadac_channel_1_i in range(len(decadac_gates_1)):
                decadac_gates_1[decadac_channel_1_i].ramp(voltages_1[decadac_channel_1_i][index_1], 0.3)

            for index_2 in range(number_gate_steps_2):
                for decadac_channel_2_i in range(len(decadac_gates_2)):
                    decadac_gates_2[decadac_channel_2_i].ramp(voltages_2[decadac_channel_2_i][index_2], 0.3)

                time.sleep(delay)

                _break_flag = False
                for lockin in ohmics:
                    if lockin.R() >= lockin_threshold:
                        _break_flag = True
                        print('Lockin threshold reached!')
                if _break_flag:
                    break

                _results = [(decadac_channel_1.volt, decadac_channel_1.volt()) for decadac_channel_1 in decadac_gates_1] + [(decadac_channel_2.volt, decadac_channel_2.volt()) for decadac_channel_2 in decadac_gates_2] + [(lockin.R, lockin.R() * lockin_current_scaling) for lockin in ohmics] + [(lockin.P, lockin.P()) for lockin in ohmics]
                datasaver.add_result(*_results)

        return datasaver.dataset


def set_find_operating_point(
                                station: 'station',
                                sample_config: dict,
                                topgates: 'keithley_list',
                                decadac_gates: 'decadac_channel_list',
                                ohmic: 'lockin',
                                gate_volt_range: list,
                                gate_volt_step: float,
                                clock: Clock,
                                lockin_threshold=1e-09,
                                lockin_current_scaling=1,
                                keithley_threshold=1e-09,
                                minimum_R=20e-12,
                                experiment_name_prefix='',
                                delay=0.1,
                                number_stability_measurement=100,
                                waittime_stability_measurement=0.01,
                                smoothing_n=5,
                                R_operating_point=200e-12,
                                experiment_metadata={}
                            ):
    """
        This function analyzes the pinchoff of the current through an SET
        (ohmic) and finds its operting point by sweeping the gates specified
        in topgates and decadac_gates.
    """
    # Set experiment metadata
    experiment_metadata['SET_find_operating_point'] = {
            'gate_volt_range': gate_volt_range,
            'gate_volt_step': gate_volt_step,
            'lockin_threshold': lockin_threshold,
            'lockin_current_scaling': lockin_current_scaling,
            'keithley_threshold': keithley_threshold,
            'minimum_R': minimum_R,
            'experiment_name_prefix': experiment_name_prefix,
            'delay': delay,
            'number_stability_measurement': number_stability_measurement,
            'waittime_stability_measurement': waittime_stability_measurement,
            'smoothing_n': smoothing_n,
            'R_operating_point': R_operating_point
            }

    pinch_off_analysis = {}

    # Check if any topgate can be controlled. This should be the case
    # as we want to measure pinchoff
    assert not topgates == []

    # Mark experiment as SET_find_operating_point
    experiment_name_prefix = experiment_name_prefix + 'tune_SET_'

    # Sweeping down all gates to protect them
    print('Preparing Instruments...')
    for keithley in topgates:
        ramp_keithley(keithley, gate_volt_range[0])
    for channel in decadac_gates:
        if channel.volt.get() != gate_volt_range[0]:
            channel.ramp(gate_volt_range[0], 0.3)
        else:
            continue
    time.sleep(2)
    print('Done.')

    # Run first pinchoff measurement
    print('Measuring pinchoff...')
    _dataset = linear_1d_sweep_gates_measure_ohmics(
                                                    station,
                                                    sample_config,
                                                    topgates,
                                                    decadac_gates,
                                                    [ohmic],
                                                    gate_volt_range,
                                                    gate_volt_step,
                                                    lockin_threshold=lockin_threshold,
                                                    lockin_current_scaling=lockin_current_scaling,
                                                    keithley_threshold=keithley_threshold,
                                                    experiment_name_prefix=experiment_name_prefix,
                                                    delay=delay,
                                                    experiment_metadata=experiment_metadata.copy()
                                                    )
    _data_first_pinchoff = _dataset.get_parameter_data()
    _lockin_R_first_pinchoff = _data_first_pinchoff[ohmic.name + '_R'][ohmic.name + '_R']
    _gate_voltage_first_pinchoff = _data_first_pinchoff[ohmic.name + '_R'][topgates[0].name + '_volt']
    _V_max = np.max(_gate_voltage_first_pinchoff)

    # Check if Accumulation was possible
    if np.max(_lockin_R_first_pinchoff) < minimum_R:
        print(f'Akkumulation not possible for {_V_max} V!')
        return pinch_off_analysis

    print(f'Accumulation possible for {_V_max} V.')

    # Find pinchoff point
    _V_pinchoff = np.min(_gate_voltage_first_pinchoff[np.where(_lockin_R_first_pinchoff > minimum_R)])
    pinch_off_analysis['V_pinchoff'] = _V_pinchoff

    print(f'Pinchoff occurs for {_V_pinchoff} V.')

    # Find inflection point (V_th, I_th) as operating point of SET
    #   Define usefull variables for sanity
    _R = _lockin_R_first_pinchoff
    _V = _gate_voltage_first_pinchoff

    print('Analyzing Pinchoff to determine operating point...')
    try:
        # smoothen and interpolate
        _interp_first_pinchoff = scipy.interpolate.interp1d(_V, scipy.signal.savgol_filter(_R, window_length = smoothing_n, polyorder = 3), kind = 'cubic')

        assert np.max(_interp_first_pinchoff(_V)) > R_operating_point, "Operating current not reached."

    except BaseException as err:
        print('Operating point could be not found!')
        print(err)
        return pinch_off_analysis

    _V_th = _V[np.where(np.abs(_interp_first_pinchoff(_V) - R_operating_point) == np.min(np.abs(_interp_first_pinchoff(_V) - R_operating_point)))][0]
    _R_th = float(_interp_first_pinchoff(_V_th))
    pinch_off_analysis['V_th'] = _V_th
    pinch_off_analysis['R_th'] = _R_th
    print(f'Operating Point found for {_V_th} V @ {_R_th} A.')

    time.sleep(2)

    print('Setting SET to operating point...')
    for keithley in topgates:
        ramp_keithley(keithley, _V_th)
    for channel in decadac_gates:
        if channel.volt.get() != _V_th:
            channel.ramp(_V_th, 0.3)
        else:
            continue
    time.sleep(2)
    print('Done.')

    # Checking if the maximum current was stable
    print('Checking for stability...')
    _dataset = constant_set_gates_measure_ohmics(
                                                    station,
                                                    sample_config,
                                                    topgates,
                                                    decadac_gates,
                                                    [ohmic],
                                                    _V_th,
                                                    number_stability_measurement,
                                                    waittime_stability_measurement,
                                                    clock,
                                                    lockin_threshold=lockin_threshold,
                                                    lockin_current_scaling=lockin_current_scaling,
                                                    keithley_threshold=keithley_threshold,
                                                    experiment_name_prefix=experiment_name_prefix,
                                                    delay=2,
                                                    experiment_metadata=experiment_metadata.copy()
                                                )
    _data_maximum_current_stability = _dataset.get_parameter_data()
    _lockin_R_maximum_current_stability = _data_maximum_current_stability[ohmic.name + '_R'][ohmic.name + '_R']
    _gate_voltage_maximum_current_stability = _data_maximum_current_stability[ohmic.name + '_R'][topgates[0].name + '_volt']

    # Check if Accumulation at threshold is stable
    if np.min(_lockin_R_maximum_current_stability) < minimum_R:
        print(f'Accumulation unstable for {_V_th} V!')
        return pinch_off_analysis

    print(f'Accumulation stable for {_V_th} V.')

    print('\nSET operating point found and set.')
    return pinch_off_analysis
